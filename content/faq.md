---
title: Frequently Asked Questions
desc: Questions that I am asked often.
---

These are some questions I've been asked often. Before asking me a question, please check if it's already answered on here.

## Contact

### Should I use dchen@dennisc.net or dchen@mathadvance.org?

I don't really care which, to be honest, so don't spend too much time worrying about it. However, if it's very obviously Math Advance related (MAT, MAST, etc) you probably should email the Math Advance email. 

### I have other contact information besides your email, can I use it?

If any of the following conditions are met:

- it is productivity related (can you teach me/my kid/volunteer for my program/etc) and **long-term**,
- you will want a record of this, or have reason to believe I will (the point above is a good reason to believe I will want a record),
- it's MAST related (besides "can you unlock/check a unit"). Tech issues should be opened up on GitLab if you know how and emailed otherwise,

then please use email.

## Puzzles

### How did you write your puzzles?

Short answer: I thought about puzzles a lot.

Long answer: Before constructing my first puzzle, I did a lot of puzzles from Cracking the Cryptic, noticed a set of ideas that seemed to interest me in particular, and once I'd had a lot of prototypical ideas about bounding, I thought, "now I have this weird bounding idea myself. I wonder if it'd work?" Well, I tried to turn it into a concrete puzzle, and as luck would have it, the idea did work. 
As I was constructing and testsolving my first puzzle, I realized "Hey there's another vibe (not even idea at this point) that seems interesting to explore". So I solidified it into an idea and kept pushing really hard, and when I was done I finished my second puzzle, which is my hardest to this date (Alphabet Soup). (That puzzle literally drained the sanity out of me, I worked on it for 4 hours a day for a whole week straight.)

Basically to get started I got a lot of good examples by solving good puzzles from CTC, and having a good mental model of how a specific idea (bounding) worked led me to daydream about it and helped me construct my own take on it. Then, the process of creating my first puzzle led to leftover ideas for the next, and I kept that ball rolling and now I have 50-something puzzles created.

## Math

### Can you look at or help with my contest?

Yes, but if and only if you are using [mapm](https://mapm.mathadvance.org) ([why mapm?](/writing/tech/why-mapm)). There are a ton of examples, tutorials, and documentation:

- [Zero To Mapm](https://git.sr.ht/~dennisc/zero-to-mapm), for total newbies to Rust (and to a lesser extent, LaTeX). You do not actually need to learn how to write Rust, just how to install Rust programs.
- [Official docs](https://mapm.mathadvance.org), use them once you've correctly setup your mapm install
- [mathadvance contests](https://gitlab.com/mathadvance/contest-releases), which can be used as real-world examples of mapm contests.

**I am willing to help you setup mapm, migrate to it, etc.** We've got decent adoption of mapm inside of Math Advance, I want to see if I can get a couple groups outside to use it.

If you're running a non-math contest and want assistance with mapm, my offer still stands. (Though of course I can't help from a non-technical standpoint, since I don't know anything besides math well enough to do so.)

### Can you teach me/my kid?

[Yes, if you apply and are low-mid AIME level.](/apply-2023.pdf)

### OK, I saw your Fall 2023 course. It's for low-mid level AIME people. I'm high-level AIME/low-level olympiad. Would it help?

Typically this question is also asked with some variation of, "I see a lot of your alums have gotten ridiculous awards. Would it help me do the same?" Well, maybe by associating with them. But my course was not really the direct reason for these insane performances.

We did have some olympiad content, but not nearly enough instruction at that level to come close to explaining these performances. My internal model is something like this: imagine people are rockets trying to leave Earth. MAST is like the thrusters that shoot them out of orbit (beginner-mid AIME level). The reason they continue to travel in space (get all these crazy awards) is because of their own velocity (motivation and studies). So maybe in some sense "they wouldn't be here" without my program. But it just provides an initial start, and my students were just really good at utilizing it.

Will it help? To a limited extent, I think largely with consistency. I think I have enough mathematical maturity in general to point out, "Hey your solutions look like you aren't really focusing enough on details"/"You could work on identifying the main hard steps vs when you actually can just leave smth as an exercise to the reader"/etc. If you don't know if you're beyond the level of this course, maybe try filling out the diagnostic and sending an application. If the answer is "yes", then you'd probably finish very fast anyway.

## Programming

### How did you make this website?

Mostly with pandoc. It's hosted with NGINX. You can also see my post on [how to make a static website](/writing/tech/static-website).

### Can I use X project that you made?

Yes; in fact, please do. I couldn't even stop you if I wanted to, that's the point of open source. (If I didn't want you to use it, I wouldn't have made it public, would I?)

I'm curious by disposition though, so if you're using one of my projects, I'd appreciate it if you sent an email letting me know. That way I know which of my projects has the most reach, which is one of the factors I consider when deciding what to work on.

### TeX Live or MiKTeX?

TeX Live hands down. Particularly if you are using Windows, which is probably the reason you'd ask such a question. The installation/setup process has a lot less hiccups, particularly with Asymptote.

Also, if you are using Windows, you might want to consider [not](https://web.evanchen.cc/faq-linux.html)?
