---
title: Advice for math contests
---

[What are math contests, and should you do them?](/math/advice/overview)

[For people just starting](/math/advice/starting)

[For borderline AIME qualifiers](/math/advice/aime)

[For olympiad hopefuls](/math/advice/usamo)
