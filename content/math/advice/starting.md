---
title: For people just starting math contests
---

So you've decided to do math competitions. Great! I'm going to try and give you a little bit of inside knowledge and tell you the things I wish I knew when I started out.

If you haven't done any problems, **start doing them now**. Here are a couple of things you can do:

- Mostly for Americans: print out some tests in the AMC series and do as many problems as you can. Do not bother to time yourself yet, working on speed can come after you're able to solve a good range of problems.
    - For middle schoolers: see the [AMC 8](https://artofproblemsolving.com/wiki/index.php/AMC_8_Problems_and_Solutions).
    - For underclassmen (9th and 10th grade), and more math-savvy middle schoolers: see the [AMC 10](https://artofproblemsolving.com/wiki/index.php/AMC_10_Problems_and_Solutions).
    - For upperclassmen (11th and 12th grade): see the [AMC 12](https://artofproblemsolving.com/wiki/index.php/AMC_12_Problems_and_Solutions).[^rough]
- Pick up a textbook. I prefer paper textbooks because it's much easier to carry around, not be distracted, etc. [AoPS Volume 1](https://artofproblemsolving.com/store/book/aops-vol1) is good. If you're near the top of your class, you can just skip to [AoPS Volume 2](https://artofproblemsolving.com/store/book/aops-vol2).
- Register for a class, join a club, etc. This is so you have a group of people to expose you to the fundamentals and give you a foothold on math contests, so that you develop the skills and knowledge to independently study in the future. Note that most school math clubs are jokes and are not good enough for this. **If you don't know who to study with, ask the nicest math person you know to help.**

[^rough]: If the most recent tests look unreasonably hard, it's because they are. If you're starting math contests this late, it can be an uphill battle! Try starting with the AMCs between 2010 and 2018.

Until you have started doing problems in some way, do not bother reading the rest of this page.

## Your intuition is king

Different advice applies to different people. Some advice is so common-sense that I will state it absolutely: for instance, don't spend 12 hours every day doing math contests, that is a really bad idea. But usually, if I am not sure about something or it doesn't apply to everyone, I will [add modifiers like "I think" to appropriately express my level of confidence.](/writing/essays/modifiers).

If your intuition contradicts my advice, you should trust your gut, especially when I add something like "I think."

On the flip side, use common sense. If you are spending 12 hours to finish the hardest problem in one chapter of a book instead of reading the next, and everyone is telling you to move on, you should probably move on.

## Contests worth taking

In the USA, there is only one math contest series people take seriously: the AMC 10/12. If you do well, you can qualify for the AIME (~5000 people), and if you do well on the AIME, you can qualify for the USA Math Olympiad (~500 people).[^jmo] If you want to know what happens after the USAMO, read the [USA IMO team selection process](https://web.evanchen.cc/faq-rules.html#CR-1).

[^jmo]: I am simplifying a little here: there is a USA Junior Math Olympiad, which you qualify for by taking the AMC 10 and AIME, and a USA Math Olympiad, which you qualify for by taking the AMC 12 and AIME. The USAMO is harder to qualify for and slightly more prestigious. The sum of USAJMO and USAMO qualifiers is roughly 500, not 500 each.

Anyway, you do not need to worry too much about the specifics. If you are serious about math contests, your goal should be to do well on the AMC 10/12 and qualify for the AIME.

But there are other math contests, some of which are more approachable. For middle schoolers, [MATHCOUNTS](https://www.mathcounts.org/) is the most well-known --- don't get too caught up in it though, because it rewards speed over depth of knowledge more than any other contest. [Math Kangaroo](https://mathkangaroo.org/mks/) is good. Though the [Winter MAT](https://mat.mathadvance.org) is geared towards people with a little more experience, it may be suitable for some people reading this page. (Disclaimer: I direct the MAT.) If there are any local contests in your area, I recommend taking them too.

I weakly anti-recommend college contests for beginners (BMT, SMT, PUMaC, HMMT, etc), because their difficulty scaling is quite ridiculous. However, if your friends are all going, I recommend you go as well --- it is a fun social experience at least.

## Resources

A small collection of useful beginner-friendly resources.

- [Alcumus](https://artofproblemsolving.com/alcumus)

    Online math learning tool. Good for absolute newbies, but if there's something else you're doing that's already working, do that instead.
- [David Altizio's website](https://davidaltizio.web.illinois.edu/mathlinks.html)
- [Evan Chen's website](https://evanchen.cc)

    Caveat: Evan Chen's website is mostly for olympiad math (i.e. proof based). My advice and resources are focused for people who want to get into computational math contests (i.e. short answer) as an intermediate step, typically people who have to take computational math contests to qualify for olympiad in the first place.
- [Justin Stevens' website](https://numbertheoryguy.com/)

## What to avoid

### Don't do every problem 

You do not need to do every problem in a contest, chapter, or book. Nor should you arbitrarily decide "I will do the last 20 years of the AMC 10/12". While it is commendable to put in a lot of effort, it is just as important to make sure it actually gets effective results.

Being a completionist keeps you from moving on when you should.

### Study plans

You should not be making detailed study plans, especially if you haven't actually opened a book and started reading. This is not to say you should have no idea what you are going to do. Having a rough idea in your head of what you want to study next can help provide direction and motivation. But you should not be making detailed study plans because *you frankly do not know what you are doing yet*. (By the time you knew enough to make a study plan to get to a certain level, you'd already have gotten there!) Be flexible and be willing to give up on something (say a textbook) if it doesn't work out.

Let me share an anecdote. When I was in 6th grade I was just starting math competitions, was barely aware what MATHCOUNTS was, thought the AMC 8 was "the competition" of the year, and had no clue what the AIME was despite nearly qualifying for it until February or March. My preparation mostly consisted of doing random Alcumus problems with my friend during English/History class, and we occasionally did past problems from the AMCs and AIMEs and marveled at the prospect of making AIME. My lack of knowledge about math competitions and my having no idea of what training was supposed to look like meant it made some degree of sense for me to do Alcumus back then. As I learned more about the math competition scene and got my values and philosophy aligned, I gradually transitioned to using better resources.

It would make no sense and be terribly inefficient for me to do Alcumus now on the scale I did it before. I wasn't naive enough back then to think I'd have an idea of what I'd be doing in five years, and I am not arrogant enough to think that I'll know what I'll be doing in five years from now. If I had to guess five years ago, my answer would've been embarrassingly wrong. I don't doubt the same will happen if I guess now what I'll be doing in five years. So the worst thing you can do is let past you dictate future you's actions.
