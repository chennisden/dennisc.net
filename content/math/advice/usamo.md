---
title: Advice for getting into USAMO
---

This advice is meant for students doing fairly well in AIME (scoring around 7-8 in an *official contest*)[^clarify] who are interested in qualifying for the national olympiad. You should have a good idea what you are doing now, so most of my advice will be fairly big-picture unlike the rest of my advice pages.

[^clarify]: To clarify, this means qualifying for the AIME and taking it officially, not taking a practice test.

Disclaimer: My index is usually fairly borderline. Unlike my AIME advice, which will probably work for most people if you just work hard enough, this advice is only enough to make you *competitive* for the USAMO. You will have to get to the finish line yourself.

## Unsortable

Once you've done a good job [sorting](aime) everything in your brain, you'll be left with three types of problems:

- problems that are trivial,
- problems that are sortable,
- and problems that are non-trivial and unsortable.

Typically these unsortable problems will appear in the middle of the test, not the end. Examples include [Fall AMC 10B 2021/7](https://artofproblemsolving.com/wiki/index.php/2021_Fall_AMC_12B_Problems/Problem_5) and [AIME I 2022/6](https://artofproblemsolving.com/wiki/index.php/2022_AIME_I_Problems/Problem_6).

The reason they are unsortable is because these problems are the kind you just have to do. They don't really have a main idea or connect to much else. To put it a little less nicely, they are mental manual labor. You have to build up some endurance to doing these problems because they will pop up in the AIME, especially in recent years.

So how do you do these problems well? The answer is not just blindly practicing (though practice is still important --- especially with these kinds of problems). [Have you ever seen a person carry a refrigerator on their back?](https://www.reddit.com/r/nextfuckinglevel/comments/hb77mt/guy_carries_a_fridge_on_a_bike/) Even though it's just applying force, the person carrying the fridge has to figure out how best to apply it. The same goes for when you're trying to solve these problems. Yes, you will have to bash these problems out --- there is no "clever" way to solve it, because there isn't really any substance --- but you can still make a series of unsexy optimizations that make the problem manageable.

The only way to practice this skill is by doing more problems. No one except for you can teach you how to do this, there is no way around it. However, hopefully now you have an idea what you're aiming for.

## Speed

At this point, you should probably be able to solve almost every question of the AIME within an hour.[^each] So on the surface, the differentiator is speed. But my advice on speed is the same as it was for beginners: it is the wrong thing to focus on. Instead, what you really want to be focused on is *consistency*. You do not have to frantically finish every step of the problem in 5 minutes. But if you can consistently recognize how to do the 8 or 9 problems out of the first 10 that are sortable within 7 or 8 minutes, you will be in good shape to tackle the last 5.

[^each]: Obviously I don't mean that you can do all the questions together in an hour, but each should take no more than an hour. (Most will take much less.)

Obviously, consistency also entails accuracy. In my [AMC advice](amc) I emphasized building raw solving power. In my [AIME advice](aime) I emphasized seeing the big picture. Now, you have to zoom in and see the details. Where before you could get away with *knowing* how to solve a good number of problems, you now have to actually be able to solve them all.

Again, the only way to get better at this is with practice, but hopefully the target is clear: consistency, not speed.
