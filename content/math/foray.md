---
title: A Brief Foray Into Euclidean Geometry
desc: A textbook for an honors high school geometry class. Also suitable for math contest prep.
---

You are likely sick of geometry textbooks that spend the first 20 pages explaining what a point and a line are. So am I. That's why I've tried to do things a bit differently with this textbook.

Features:

- Jumps straight into the action: we all know what a line is
- Eases students into proofs: in the first chapter, we take the time to discuss **if and only if**, finishing proofs **by symmetry**, etc
- We follow through with definitions: Power of a Point is not just a theorem that states $PA\cdot PB=PC\cdot PD$
- The book is organized by how theorems are proven, not just by what theorems appear similar on the surface. Thus proofs of theorems actually make sense
- A decently sized, *completable* selection of quality exercises, rather than a tsunami of garbage
- Problems actually have hints and full solutions, not just answers
- Witty, relevant quotes at the start of each chapter! (OK, maybe some of them are not so relevant)
- **Concise**

Not all problems will have solutions, but most of the more difficult ones should have hints or a solution.
