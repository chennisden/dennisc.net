---
title: Math
desc: Personal math materials and recommendations. See mathadvance.org for MAST materials, including samples.
---

## Recommendations

- [Math Advance](https://mathadvance.org)

- [Apply for Math Advance's Fall 2023 course](/apply-2023.pdf)

- [My advice](/math/advice)

- [Mock Contests](/math/mocks)

  A list of mock contests that I've written for and mock contests that I personally recommend.

- [OTIS](https://web.evanchen.cc/otis.html)

  Evan Chen's olympiad program, which recently has had an RPG component added to it! I've taken many pages out of his book for MAST.

## Reflections

- [Contest Report for Summer MAT 2022](/reports/summer-mat-2022.pdf) [(TeX)](/reports/summer-mat-2022.tex)
- [Contest Report for Summer MAT 2021](/reports/contest-creation.pdf) [(TeX)](/reports/contest-creation.tex)

  A report on the first year of the MAT, which ran online.

## Notes

### Linear Algebra

I am much more partial to edition 2 than edition 3 of Axler for some reason. Perhaps it is because it feels more concise, or there is less wanton "put everything in a box" formatting. But either way, anything that relies on Axler will be referencing ed. 2, not ed. 3.

- [Linear Algebra, in a nutshell](/writing/blog/linalg)
- [Additional exercises to Linear Algebra Done Right, ed. 2](/math/axler)

### Abstract Algebra

- [An Abstract Algebra Primer](/alg-primer.pdf)

    This is a medium-length (110 pages) introduction to abstract algebra. Intuition is emphasized, fully formalizing arguments takes a back seat, the theory is extremely streamlined (so many important and interesting ideas are omitted). [Errata here.](/writing/blog/algebra-primer)
- [The Jordan-Holder Theorem](/jordan-holder.pdf)

### CS251

Teaching assistant notes for Carnegie Mellon's CS251.

- [My reflections on TAing CS251](/writing/blog/251-reflections)
- [filter(L): an example of DFA threading](/filter.pdf)
- [(Un)countability and (Un)decidability](/countability-decidability.pdf)
- ["Fooling Pigeon Set" is a horrible name](/writing/blog/fps)

### Formal Logic

Mostly based on CMU's 21-300 Basic Logic course.

- [Inductive Structures](/writing/blog/inductive-structures)
- [Unique Readability](/writing/blog/unique-readability)
- [Propositional Formulae](/writing/blog/propositional-formulae)

### Miscellaneous

- [A proof of the Principle of Inclusion-Exclusion](/pie.pdf)

    These are notes from my Fall 2023 AIME class that I decided to publicly share.
