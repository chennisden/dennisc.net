---
title: CMU Courses I took
---

Context: Class of 2027 SCS major. Seeking to get a Masters in Mathematics in parallel.

Classes each semester are roughly ordered by how much I care about them (descending). These are my opinions and you will probably get a more balanced perspective from [cmu course reviews!](https://kaileyhh.github.io/course-reviews/), which is a compilation from multiple sources. In particular note I am not trying to give an unbiased review or be helpful to the average CS major.

*However*, if you have exceptionally strong background and want to challenge yourself academically, then I think these opinions will become a lot more valuable to you.

## Freshman Fall (2023)

### 15-150 Principles of Functional Programming

**The good**

This class teaches you an entirely new paradigm of thinking. I'd already seen many of these ideas before, but it was cool to see some of them implemented in practice. And many other ideas I had not seen before. HOFs and CPS were incredibly cool, and seeing how SML (an eager language) could also exploit lazy evaluation was very interesting too. The TAs are incredible too: all of them are knowledgeable and passionate about the course, are very kind-hearted, and are exceptional at explaining things.

Because functional programming is really cool, a lot of people have devoted time to writing out amazing explanations of the core concepts (CMU people in SML, some other universities in OCaml, and normal people in Haskell). A few examples:

- [Learn You a Haskell](http://learnyouahaskell.com/chapters)
- [Brandon Wu's 150 Notes](https://brandonspark.github.io/150/)

So you can learn functional programming from plenty of sources, and I strongly recommend you mix and match to get a more complete understanding.

**The bad**

But there are a few kinks to be ironed out. For one, very few people go to lecture, so "lab" (which is largely a review session) is usually painfully slow. (I'm unsure what can be done about this, and I hear it gets better in the spring, but this is still a pain point.) There also seems to be very little communication between the TAs and the professor, so the professor just says words and the TAs run the brunt of the course. This means that you will just randomly have very hard problems on homework assignments (the worst offender being Datatypes 5.5), but honestly in retrospect nothing was absurdly unreasonable. Still though, it's utterly hilarious that the TAs can just do whatever they want and the professor will have no clue.

Also, Games is somehow the least fun I've had in any class so far this semester, despite "Games" being the name.

**The ugly**

Now, for some more substantive criticism than "course administration is janky". Unfortunately, this is moreso "what I didn't like" (and what you will not like if you already have background in FP), rather than "here is what the course should do". This class is far too applied, in the sense that problems look more like "solve genuinely tricky problems with these tools" rather than "learn the theory behind functional programming" (of which there is a lot).

Some things I expected to see but did not:

- Formal logic
- Monads
- Type theory
- Category theory
- Curry-Howard Isomorphism

Granted, it would be really bad if 15-150 just became "crazy PL stuff exactly 3 people would benefit from", and objectively, the correct response is "OK just go take more PL courses". But if you're one of those three people, be prepared to be very ambitious. Email the 15-312 professor and ask if you can sit in or something. (I know someone who tried to skip 15-150, with really strong reason to, but in short --- good luck with that.)

Verdict: Inspiring (for a 100-level course, at least) but somewhat janky. (For what it's worth, I'm applying to be a TA for this course during Spring 2024.)

### 15-122 Principles of Imperative Computation

15-122 has the unenviable position of teaching a potpurri of (imperative) programming concepts, so the content is not organized as well as you might like. However, on the plus side, the materials are fairly thorough: unlike some of the other classes (math especially), you will never need to look outside of the course materials to learn a concept. (Footnote: Whether this is actually a good thing is up for debate. I, for one, think it is good to expect students to do their own research and enjoy taking courses where the syllabus isn't just "precisely what was covered in class.") Iliano is notorious for being a poor lecturer, but I personally thought he was alright. Not the absolute best lecturer I've ever had, but passably good.

Now for the minuses. And there are a lot.

- The writtens (i.e. "fill in info here" worksheet style assignments) are kind of utter bullshit. A lot of fill-in-the-blank questions, with the worst traits of them exemplified (e.g. four things make sense here, but you need to guess which three the course staff want you to use), and not much value gotten out of it.
- The programmings are merely ok. Nothing inspiring, but not horrible.
- Don't get caught cheating. Seriously. This class hands out a lot of AIVs (Academic Integrity Violations), and you are hosed if you get one. Two and you are expelled. Seriously, just don't.
- The tests are the WORST. The format is the same as the writtens, but with the added component of time pressure. Understanding the concepts is not enough to ensure a good grade; you have to actually be good at the exam format. No other class I've been through has had exams as unfair, and even much harder classes have fair exams. (This class was the only class I ever consistently got Bs on in exams, despite --- in theory --- being the easiest of them.)

In particular, if you are aiming for an A, you should only aim to do OK on the exams (in the 85-90 range; even 80 might be fine). The way grading works, homework will more than make up for it.
The content itself is not extraordinarily difficult, but it is something to pay attention to, particularly in the second half of the semester. If you have prior programming experience, especially in C, it may very well be a cakewalk. But for most people it will work the brain, though hopefully not fry it.

For exceptional students: you could realistically learn a superset of this course over the summer. However, this course is largely unavoidable. If you somehow manage to skip this course, that's good: just make sure you understand how C works generally, and also some simple data-structures and algorithms stuff.

Verdict: It's alright, but largely uninspiring.

### 08-180 Nature of Language

Objectively, this is the best class I've been in all semester --- yes, even better than 15-150, and by a lot. Despite this, I have somewhat mixed feelings about this class.

On the one hand, the content is genuinely very interesting. The plural morpheme, how words combine to form phrases and phrases combine to form sentences, and phoneme distributions are genuinely very interesting. But somehow I don't feel the drive to continue; maybe this is just the right amount of linguistics for me.

The reason I have mixed feelings may largely be because of the weekly quizzes. I'm not a great test-taker and would honestly prefer not to think about having a quiz or a midterm every week. But to be honest, none of the quizzes are actually that hard. The quizzes about word and sentence structure were quite easy, and the midterm on it was too. I just bombed two of the first few quizzes, which is fine given that the lowest two quiz scores get dropped, but in retrospect I should've just tried a little harder (like, "spend 15 minutes more studying for each of the first few quizzes" harder) and I wouldn't have had to worry about my grade for the first quarter or so.

It's a good class. I wouldn't say "you shouldn't take this class", but I'd also stop short of saying "you absolutely have to take this class".

Verdict: It'll change your worldview (on languages, at least) and make you a better person, but just be prepared not to get sucker-punched out of reality in the first few weeks.

### 21-295 Putnam Seminar

Nominally a class about the Putnam, in reality it ends up sort of being a survey course of analysis, algebra, combinatorics, and whatever hodgepodge of topics ends up being taught in the year. It's a fun course and not too bad of a time commitment. I expected to care more but in practice... well let's just say I'm 4 weeks behind on homework as of writing this.

But also Section C goes much slower than you'd imagine. I went to Section D for the number theory lecture and it felt more appropriately paced for me.

Verdict: Sign up for Section D.

### 07-128 First-Year Immigration Course

One of the classes of all time. Realistically you could just not show up and submit the assignments on Canvas, though this may change next year or something. I would just talk to people there.

There's also this thing called "The Amazing Race" (well, two of these things) where you just meet random people in your year of SCS and have dinner with them. Nominally you are supposed to do some sleuthing to solve a mystery, but really it's an excuse to meet more people. Unfortunately, these things happen so late in the semester that by the time it rolls around, no one gives a shit anymore and we're just going with the motions.

It's not a bad idea, but here's my suggestion: just delete all the content in the entire course (really, none of it matters, no one's listening, let's put an end to this charade). Instead, just replace it with social time. Games. Fun. All that good stuff.

Verdict: I skipped a lot to go to Schenley Park. I do not regret it.

### 76-107 Writing About Data

You get an assignment basically between every class period. The final assignment (the CGA) was also absolutely boring. We were writing about writing, but in the least interesting sense possible. Also, attendance is mandatory. (Though I did use both of my "skips", mostly because I just really needed food those days.)

Verdict: Take Advanced First Year Writing instead.

### 76-108 Writing about Public Problems

You actually have to email people. I somehow slept through my meeting with CMU Dining too. Not recommended if you are an introvert like me. Otherwise it should be fine. Also, attendance is probably mandatory. I don't really want to find out the hard way whether it is or not.

Verdict: Have a problem you want to solve in mind before you begin the semester.

### 09-101 Introduction to Experimental Chemistry

I suffered every Tuesday. The cycle of do the prelab, show up to lecture for an hour, go to Putnam practice (which conveniently happens to be during the break between the lecture and the lab), do lab for 3 hours, finishing up my data report while getting food at the Underground because it's due before midnight, and then waking up the next day to actually complete the data analysis was draining.

The actual lab itself is alright. We use equipment with really crappy accuracy, but I don't really believe that first-year students who give 0 shits need to be using state-of-the-art equipment.

The one really annoying thing is that my homeworks would retroactively have their grade reduced almost a month after it's returned. So I have a lot of anxiety about my grade because I have no clue what it actually is! And the feedback is very unhelpful; "circle a large superset of the relevant section and -6, yay!" The fact I have to read the syllabus to try and grade grub is a really bad sign.

Granted, I don't know how much of this is just because "the TA is trolling" vs. "the prof is making the TA go back and regrade stuff". I honestly have no clue, but I am extremely annoyed.

Verdict: Shorter than Experimental Physics. The course itself is alright, but I endured a lot of mental pain, possibly more than I did for Nature of Language in the start of the semester.

## Freshman Spring (2024)

### 21-610 Algebra I

For context, this is CMU's introductory graduate level algebra course. What this class is like is probably very dependent on the professor, since at this level, math courses are just "whatever the professor feels like doing." This iteration was with Dr. Cummings.

This class, in one word, is **goated**. For those not in the know, Dr. Cummings likes to wing his lectures, but he somehow gets away with it most of the time just by knowing the content really deeply. As a consequence lectures often tend to run overtime, sometimes going 5 minutes over. Also there are no lecture notes, because these lectures get cooked on the fly.

Objectively that sounds pretty bad. But the whole point of a course is to build intuition, because you can't really learn without getting down in the weeds and reading a textbook. And this course does that fantastically: subtle points are expounded and arguments are well motivated.

I think the course would probably benefit from an assistant/grader/TA/etc, but small classes tend not to get assigned one.

### 21-269 Vector Analysis

With Dr. Leoni. (The class is somewhat professor-dependent, though you can predict whether Dr. Leoni or Dr. Tice is teaching by taking year mod 2.)

This class is really weird. We started with standard Rudin-style analysis (though unfortunately our definition chain was $\mathbb R \to \mathbb N \to \mathbb Z \to \mathbb Q$ instead of the $\mathbb N \to \mathbb Z \to \mathbb Q \to \mathbb R$, the latter of which explicitly constructs $\mathbb R$ with Dedekind Cuts/Cauchy Sequences/etc which I prefer). Then we did topology stuff, which made me quite happy, then sequences stuff which made me sad and then actual calculus stuff which makes me very sad. There's a lot of things being covered and I wish we'd focus on analysis from a more topological perspective, but this class is responsible for teaching a lot.

This course also has "practical exercises" in the homework and exams, where we take "real limits" (with functions we have not yet rigorously studied, like $\sin$ or $\arcsin$) and sometimes use techniques which we have not formally learned. So it feels like there are two courses proceeding in parallel, one of which is "all the stuff you officially learned here" and another of which is "grungy computations that relate somehow to what we have learned." Given that I am allergic to computations, this is not my favorite part of the course, but I do acknowledge that being forced to learn how to take a limit seems like a good thing.

Now for the logistics, which is the weirdest part. We have bi-weekly homework which alternates with bi-weekly quizzes, and the quizzes are a new thing this semester. I think the quizzes are uniquely horrible and the homework is too sparse. (Basically, I think quizzes should be replaced with homework as it was before.) Also, I'm never entirely sure what's going on in recitations.

To be clear, Dr. Leoni is a great professor and teaches well. And this class is a *great* class (minus the quizzes). It's just also very *weird*.

### 15-251 Great Ideas in Theoretical Computer Science

Probably the easiest CS course I've taken so far (frosh fall and spring), at least in terms of "how scared I am about my grade in the class". (Granted, 122 and 150 present very stiff competition, so being easier than those classes is not saying much.) The bucket system and rounding makes homework very forgiving in that you can make quite significant mistakes and not be penalized for it.

I will say that going attendance-mandatory does help to force you to get enough exposure to the material. If I didn't go to class I would likely find this course a lot harder.

### 15-259 Probability in Computing (PnC)

I like chocolate.

The course started out really boring for the first two weeks but started ramping up after that; the content, despite not being too challenging, was pretty interesting. The distribution of UNIX jobs having a decreasing failure rate is interesting, z-transforms and Laplace is cool, but otherwise there is not too much to write home about. It's a fine course, but in large part due to previous contest math experience, I found it fairly easy.

You have a lot of leeway to make mistakes, by the way. Homework grading is quite generous, you legitimately just need to solve the problems to do well on the exams (which is not in of itself easy, but certainly easier than the contortionism some classes like 15-122 require), and you get two quiz drops out of what, 6 quizzes? So it's not very stressful to get an A.

Do well on the exams though, you get a box of chocolates if you ace.

### 88-284 Topics of Law: The Bill of Rights

It's mostly a presentation based class: in theory everyone reads some cases every week, and then people present on them. It's fairly chill, but because the professor is a practicing attorney, class is held at a really cursed time of the night.

In theory you should be doing a good amount of prepwork for each case, but in practice I spend too much time gaming/reading manga.

### 05-391 Designing Human-Centered Software

I still am not sure what this course is about. Class does tend to end early, and you get to learn about some interesting empirical design techniques, I guess.

## Sophomore Fall (2025)

### 15-756 Randomized Algorithms

### 21-603 Model Theory I

### 21-235 Math Studies Analysis

###
