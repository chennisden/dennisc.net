---
title: Hanabi
desc: The card game that fixes all your trust issues.
---

Hanabi is a cooperative card game where you see everyone's cards except your own. You give clues (color, number) to your teammates so they learn the identity of their cards, and they give you clues so you learn your cards. The goal is to play stacks of cards like in solitaire --- the default is 5 stacks of 5 different colors, but this can change.

Before proceeding further, I recommend you watch this [video of the rules](https://www.youtube.com/watch?v=VrFCekQb4nY).

I play on [hanab.live](https://hanab.live) with the [H-Group conventions](https://hanabi.github.io/). Conventions mean

- before the game begins, we agree that certain clues carry additional meaning (so a 2 clue doesn't just mean "you have a 2 here"),
- clues that do not fit any prescribed pattern ought not to be played,
- and in general, play is fairly predictable.

So conventions turn Hanabi into a game of guesswork into a cooperative puzzle game.[^flowchart]

[^flowchart]: Contrary to popular(?) belief, this does not turn the game into a flowchart. You can choose what legal move you want to make, use common sense (e.g. a 2 is clued, you have no idea what color it is, but all the 1's are already played so you play it anyway), and there are ambiguous cases where you have to use context. In other words, the game is non-deterministic even if everyone perfectly plays by convention.

You can read [Evan Chen's Hanabi guide](https://paper.dropbox.com/doc/Hanabi-Beginners-Guide-to-the-Beginners-Guide--Bpbv7_ej00cz14cUGt0EyonxAg-imUNLGlHCsxSfkUnKSTVj) for a brief overview of the conventions.

## Additional Conventions

At this point I assume you've read the [H-Group Beginner's Guide](https://hanabi.github.io/docs/beginner/). These are some additional conventions that I think you should know, in order of most to least important.

If you haven't played your first game yet don't worry about any of this!! (Except for DDA if you're playing with 2 players because **you will lose otherwise**. Probably.)

### Double Discard Avoidance (DDA)

If the player before you discards a non-trash card, you must not discard unless you have evidence to the contrary the card you are discarding is not the second copy of that card.

- On [turn 17](https://hanab.live/replay/844344#17) kiffff discards a blue 3.
- On [turn 18](https://hanab.live/replay/844344#18) MutatingPhoenix proceeds to discard the next copy of blue 3, losing us the game.
- So what happened here? b3 was not cluable at any point before kiff discarded since it was not the only copy left (no save clue) and it was not playable (no play clue).
- Since kiff's b3 was never saved, he *correctly* discards.
- Since MutatingPhoenix's b3 was never saved, he discards, failing to consider what the team would've done if he had the second copy of b3.
- Here, MutatingPhoenix MUST do something else. He can stall with a tempo clue, a 5 stall, burn a clue (i.e. do a clue that gives no new information and is otherwise terrible), etc. He just cannot discard.

Now notice I said "you must not discard **unless you have evidence to the contrary the card you are discarding is not the second copy of that card**." Here's an example:

- On [turn 22](https://hanab.live/replay/844369#22) kiffff discards a blue 4.
- On [turn 23](https://hanab.live/replay/844369#23) reychen discards. On the surface this violates DDA --- what if my chop (g3) was the other b4?
- Rewinding to [turn 8](https://hanab.live/replay/844369#8), we see that the g3 is not clued when I receive a 4 clue, meaning that I know for sure it isn't b4. Thus I can discard.

### Reverse/Layered/Multiple Finesses and Prompts

- Play clues on finessable cards can be delayed (reverse finesse).
- There can be multiple cards (sometimes unrelated --- see layered and clandestine finesses) that need to be played for the clued card to be immediately playable.

### Sarcastic Discard

If (say) two b3's are clued, and one person realizes they have b3, they can discard to let the other person know they have the b3. This is the **Sarcastic Discard**.

### 5 Chop Move (5CM)

Read [5CM on H-Group Conventions](https://hanabi.github.io/docs/level-4#the-5s-chop-move-5cm).

It's not super important you know how to do this per se, just that you don't randomly give 5 stall clues when you have something else to do (e.g. discarding).
