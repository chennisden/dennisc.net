---
title: How to use sshuttle through my server
---

Welcome! If you're seeing this, I probably sent this to you. Read [my post on sshuttle](https://www.dennisc.net/writing/tech/sshuttle) for context, let's get started.

## Requirements

Obviously to use my server as a proxy you have to know me, I'm not going to let strangers on it. You also need to be using Mac or Linux for sshuttle to function (BSDs will probably also work). If you're a Windows user I don't care about you, and nor does sshuttle, sorry. Maybe it'll work, maybe it won't, not my problem.

Also learn how to use a command line, I won't handhold you through basic shell commands like `cd`. Oh, and you should also know what the `~` directory is, it's your HOME directory.

Obviously you need sshuttle installed. You can do that from your package manager, and since sshuttle isn't written for Windows anyway I can safely assume you have one.

## Generate an SSH key

Run `ssh-keygen`. I don't care what file you save the key to, but for the sake of this tutorial let's say you saved it to the default location, `~/.ssh/id_rsa`.

Send me a copy of the **public key**, i.e. `~/.ssh/id_rsa.pub`. Just send it as an attachment to [dchen@dennisc.net](mailto:dchen@dennisc.net). It makes life easier for me, don't send it to Discord like a moron.

In the meantime, you want to edit `~/.ssh/config` to the following:

```
Host sshuttle
    Hostname mathadvance.org
    IdentityFile ~/.ssh/id_rsa
    User sshuttle
```

Maybe the file `~/.ssh/config` doesn't exist. (If it's your first time editing this file, it probably does not.) Maybe the directory `~/.ssh` doesn't even exist! Don't be worried if either of these things are true, just make the necessary directory/file and you'll be fine.

Once I've added your public key to the server you'll be able to SSH in (which gives you the ability to `sshuttle` too).

## Usage

It's the same as what I wrote in my post, just run `sshuttle -vvvv -r sshuttle 0/0` (because the host is `sshuttle` as defined in `~/.ssh/config`).
