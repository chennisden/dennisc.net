---
title: How to remember what h, j, k, and l do in Vim
date: 2022-06-05
---

At least personally, the biggest reason I took so long to start hjkl'ing in Vim is because I never could remember what each of them did. Specifically, how to tell j and k apart, because h and l are kind of obvious (h is on the left so it goes left, l is on the right so it goes right).

Here's a trick to remember that j is down (and thus by process of elimination, k is up). j looks like a fishing hook, so you can visualize a fishing hook going down.
