---
title: Arch Install Primer
desc: An overview of what an Arch Linux install is, and why each step matters. (No details.)
date: 2022-03-09
---

This is not an instruction manual on how to install Arch Linux, it's simply a big picture explanation of what the steps sum up to. Meant for beginners, but this post answers "how" and not "why" you would install Arch.

[EF --- Linux Made Simple](https://www.youtube.com/watch?v=1XBqhMLsXYk)

## List

- Partition your disk
- Boot into ISO (can be swapped with partitioning disk, not recommended for beginners)- Mount the appropriate filesystems
- Pacstrap into the mounted filesystem
- Chroot into the mounted filesystem and perform setup operations (timezone, keyboard, locale, etc)

## Why?

### Partitioning

You are installing Arch Linux _to_ somewhere, and it has to be somewhere on your hard drive. (Or SSD if you use that.) When you are partitioning your disk you are telling your system "an OS is installed here". So you basically create a place for the OS to be installed, before you actually install it.

### Boot into ISO

The ISO _itself_ is an operating system, though incredibly minimalist.[^ubuntu] What's useful is the scripts inside it. The scripts don't even have to be contained in a separate OS --- you could just package a set of scripts that installs the OS to the appropriate partition. (In fact, if you are already using Linux, you can do this!) The reason that installing through an ISO is popular is because of accessibility. Most people, _especially_ those installing Linux for the first time, are using a shitty OS like Windows or Mac (which tends to be why they switch). Said operating systems make it harder (particularly Windows) to install the necessary tools than it needs to be, and packaging everything in an ISO is just better for uniformity.

[^ubuntu]: If you've ever wondered why Ubuntu's ISO is so goddamn fat, it's because Ubuntu's ISO comes with a graphical environment. Among other things.

### Mounting

This one is the hardest to explain.

To access a filesystem in Linux you have to mount it. Your partition that you want to install Linux is a filesystem. By mounting your filesystem, you expose it to the current directory tree.

### Pacstrap

By running `pacstrap` you are installing some base packages _into your mounted filesystem_, i.e. where your new Arch operating system will reside. `pacstrap` is one of those utilities provided to you by the ISO, so it should not be surprising that it doesn't exist anymore when you chroot (since you no longer need it).

### Chroot + Setup

Chrooting is largely for convenience, theoretically you could probably install Linux without chrooting.[^mnt] (It would be really annoying to do so.) Basically, by chrooting, you are entering the filesystem of your new OS, exposing its commands to you. For instance, the essential commands provided to you by `pacstrap`.

[^mnt]: You could probably just do this by prepending /mnt to the correct paths when executing commands.

You should know that **when you chroot, you are essentially running inside the OS that you installed on your desktop.** Beforehand, you are running inside the live boot environment, which is an OS that you installed on your USB stick **in order to install Arch Linux to your desktop**. So you will not be able to get access to the live-environment tools once you arch-chroot (which is typically why you do that last; you are supposed to arch-chroot once you no longer need the live environment).

This is also why you can't run `iwctl` after chrooting or when booting into your Arch Linux install _without_ the USB stick, because that tool wasn't installed into your OS; it only exists inside the live install environment.[^networking]

[^networking]: Networking is (in)famously regarded as being difficult to set up on Arch Linux. But it really isn't. You have to understand that you have to setup your network twice for two different reasons: once in the live install so you can properly pacstrap into your new Linux partition, and another time in the chroot so that you can use the internet inside your new OS.

Some of the setup (locale, timezone, keyboard) is non-essential, especially if you use a QWERTY US keyboard by default. You are just setting some system preferences, it does not really matter if you do it now or later. (Best to do it now though, so you can take care of everything at once, but if you somehow find yourself screwing it up that's ok.)

Other parts of the setup are essential. For instance, the bootloader is what helps you boot Linux properly. And the hostname file is also important since it helps you connect to the internet.

## After Install

Don't pay too much attention to this section until you have finished your Arch Linux install.

After the installation process you have a very minimal installation of Arch Linux. All it does it boot, you literally do not even have Vim. At best you set up wifi before you rebooted your system.

At the very least you want a graphical interface which allows you to display windows of programs. To do this, you need a window system at the bare minimum, like X11 or Wayland. You _probably_ want X11, though Wayland is the future.

If you want a traditional desktop environment (Windows and Mac both have traditional desktop environments), you can install Gnome, KDE, XFCE, etc. I recommend you pick something lightweight or else boot times are going to start to resemble Windows machines. If you do decide to do this, you don't need to worry about X11/Wayland.

I strongly recommend you get a tiling window manager (TWM) instead though. For X11, you have i3, Xmonad, dwm, awesomewm, etc. Wayland has some window managers too, like Sway (based on i3), dwl (based on dwm), etc, but the selection size is smaller. It's hard to explain why a TWM is so good, and I certainly won't try to, google it yourself if you're interested.

You need a terminal emulator too. If you are using a desktop environment, one is typically provided for you so you can ignore this. But if you are using a tiling WM, some popular choices are Alacritty, kitty, urxvt, tmux, etc. I personally use Alacritty because it has sane defaults + I don't need tiling in Alacritty because I use a TWM.

You are also going to want to install some programs, like Vim (to edit text), Firefox/Surf/Links2 (for browsing), TeX Live/Tectonic (typesetting TeX files), etc. But this is increasingly swaying further away from the install process, so I will end this post here.
