---
title: Why mapm?
date: 2022-03-20
---

In the vein of [why latexmk](latexmk) I will explain why I wrote mapm in the first place. That way, when you know what annoyed me enough to finally write this software, you will have a good idea as to what it does.

When you are writing a math contest, you typically have `problems.tex`:

```tex
\documentclass{article}
\begin{document}

\section{Problem 1}
What is $1+1$?
\end{document}
```

and `solutions.tex`, which typically contains an answer key as well:

```tex
\documentclass{article}
\begin{document}

\section{Answers}
\begin{enumerate}
    \item $2$
\end{enumerate}

\section{Problem 1}
What is $1+1$?

\subsection{Solution}
It is $2$.
\end{document}
```

Let's say you want to change problem 1. Then you have to edit the problem in `problems.tex` and `solutions.tex`, as well as the answer and solution in `solutions.tex`. That is four places you have to update. And it gets even worse if you want to switch two problems: that's 8 updates. And what if you want to move everything down one slot, because your current contest is too hard? So on and so forth.

So you either have two choices: get lazy and do a sloppy job because editing a draft takes forever, even though you know _exactly_ what you want to put in, or use software that handles compilation for you given a set of problems. I really care about the quality of my contests, so I choose the latter.

With mapm, all you have to do is the following: change

```yaml
problems:
  --- one-plus-one
```

to

```yaml
problems:
  --- two-plus-two
```

One change is one change. That's how it ought to be. The rest of the spec involves telling mapm how to determine what problem `one-plus-one` refers to, and how to properly build a contest. Once you think about it that way, it is very obvious what mapm does.
