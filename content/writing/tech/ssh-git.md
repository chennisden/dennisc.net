---
title: Setting up SSH with Git
date: 2023-01-11
---

So, a while back, GitHub finally disabled authenticating with password (a very good decision!) In this post, I want to cover a couple of things:

- What is the point of using SSH instead of password-based authentication?
- How does one set up SSH locally?
- How/why you upload a public key to a Git provider (like GitHub)

## What's the point?

Let's first discuss why GitHub decided to enforce SSH encryption. The advantage of SSH is that it is more secure. When I send a password, as long as the connection between myself and GitHub is insecure, the password can be intercepted, whether it's on my end of sending or GitHub's end of receiving the password. This means that for common operations (like pushing to a repository), it is best if we avoid sending passwords.

The advantage of SSH (and asymmetric keys in general) is that you do not actually provide anything that can be intercepted; you only *prove* that you have the private key that you claim you have (i.e. the private key that corresponds to your public key). Also, SSH keys are forced to be randomly generated, meaning that SSH private keys are unguessable, unlike your crappy password of `P@ssw0rd`.[^password-manager]

[^password-manager]: Folks, use a password manager.

## How to set up SSH

I don't use Windows, so I have no idea how the specifics work. What goes will mainly apply for Unix operating systems, like Linux and BSDs. (This will probably work for Mac OS work too.)

Using `ssh-keygen`, generate a public/private keypair:

```
ssh-keygen -t ed25519
```

(The `-t ed25519` option specifies the keytype as `ed25519`, which is faster and more secure than RSA, mostly due to implementation difficulties in RSA.)

Then, just copy the public key's contents (its file extension is `.pub`) and add it to GitHub (or whatever).

Now, when you clone repositories, make sure to clone using the SSH URL, not the HTTPS URL. So, run

```
git clone ssh://git@github.com/git/git
```

instead of

```
git clone https://github.com/git/git
```
