---
title: Announcing Beam
desc: A file format that makes it easier to write LaTeX beamer.
date: 2022-04-10
---

beam is a file format that makes it easier to write LaTeX beamer. A beam _compiler_ (synonymously _implementation_) turns a `.beam` file into a `.tex` file and invokes `latexmk`. As of writing one exists in Rust and one for C is planned (partially for greater portability, and partially as an exercise in learning C for my own sake).

I'll just show you what an example beam document looks like, since the spec is so simple.

```
^ \documentclass[12pt]{beamer}
^ \title{beam}
^ \author{Dennis Chen}

\titlepage

# Introduction

~ What is beam?
- beam is a file format to write presentations with
- beam is a program that converts beam files into beamer

~ Why?
beam does for beamer what Markdown does for HTML
- beamer takes far too long to write.
- We only need a subset of beamer's features

# Features

~ Frametitles
Use the ``\textasciitilde'' character to set the frametitle.

~ Bullet points
- You've already seen it in this presentation.
- Bullet points invoke the ``itemize'' environment.

~ Block
> How to make a block
> This symbol makes a block.

~ Exampleblock
< How to make an exampleblock
< This symbol makes an exampleblock.

~ Alertblock
! How to make an alertblock
! This symbol makes an alertblock.

~ Images
Use the ``@'' character to set a background image.

@beach.jpg

# Miscellaneous

~ Stylization
beam should be written in all lowercase, even at the start of a sentence.
```

Interested in using beam? Read the [beam page on my website](/code/beam).
