---
title: Silent improvements to Glee's frontend
date: 2022-11-06
---

Unless you pay really close attention, you probably won't notice a difference between [git.dennisc.net](https://git.dennisc.net) today versus before. That's because I've mostly made semantic improvements to the page plus some CSS choices that work better for non-mainstream browsers like NetSurf. Now the frontend should be tolerable on terminal browsers, and though the navbar does not behave exactly like I want it to on NetSurf, we're at the point where this is probably more because LibCSS doesn't conform with standards (I don't see any way to replicate "stuff on the left and the right" without a flexbox).

Currently links does not work well with redirects. It's not just my site that faces this issue --- the logout link for [SourceHut](https://sr.ht) also does not work. links doesn't handle redirects well, and at some point I just have to shrug and say "not my problem".
