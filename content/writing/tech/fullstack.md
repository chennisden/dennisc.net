---
title: Every web developer should understand "fullstack"
date: 2022-08-03
---

If you are in web development, there really is no exuse not to generally understand the languages and protocols the web is built on.[^js]

[^js]: The exception, depending on what kind of website you are building (i.e. very little clientside interactivity) is Javascript: you can choose not to learn how it works or ever use it and you will be just as effective 99% of the time. Some, including myself, may argue that not having a good grasp of Javascript actually makes you better because it makes you more reluctant to fall back on it.

Part of why people compartmentalize the web is because its standards and features are so expansive. With almost a hundred status codes (and 418 I'm A Teapot isn't exactly helping), media queries for *printers*[^printer], and a million different Javascript-based frameworks, it's no wonder that the frontend devs don't want to deal with the backend on top of all this. And for the same reason, backend developers understandably stay the hell away from this abomination.

[^printer]: I actually think the media queries for printers are useful (I use it on this site), but it is not necessary knowledge for most people.

But rather than learning everything webdev related, you should try to do as much as you can using as little as you can. You don't need Tailwind when CSS variables exist, you shouldn't POST to ajax.php when you can just set up an endpoint for the same page you sent the GET request to for a form (come on, forms aren't proper APIs anyway), and you shouldn't use cloud-based databases like Firebase because that surrenders control and incurs extra complexity. Web development can be a simple process --- you're just requesting and sending data according to established protocols --- you just have to commit to making it so.

I've been saying "frontend" and "backend" developers this whole time. However, a more accurate way to characterize them is "people who don't fully understand the web". There's no shame in admitting it! The internet is filled with awful advice, bloated and unnecessary Javascript frameworks are presented as the norm, and servers are blackboxed instead of being treated as just another Linux machine, just with certain ufw rules set. But if you commit to using a small subset of the web's features, avoid Javascript unless absolutely necessary, and [understand the principles of client-server authentication](client-server-auth), creating full-fledged websites on your own will not be too far off.
