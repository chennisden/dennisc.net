---
title: With apologies to RSS readers
desc: I fixed the RSS log. Also a couple more unrelated screwups that I fixed.
date: 2023-05-17
---

Until very recently, the RSS feed was basically non-functional because I forgot to include the `<pubDate>` tag in each entry. This means that RSS readers assumed that each entry was published on whatever day the feed was refreshed, which made for a very unsatisfactory user experience. Now it is fixed.

With this, the number of times each file's metadata (i.e. the title and date fields) is parsed has increased to three: one for the All Writing page, once for whichever one of the Essays/Blog/Tech pages it falls into, and one for the RSS feed. It would likely be possible to fold it all into one Rust script, which reads only the metadata (i.e. doesn't try to parse LaTeX as Pandoc does, which would already make it a lot faster), and only reads it once. This will probably decrease the build times of my website, at the cost of some additional complexity in the form of another language. (On the bright side, the code will look nicer because I have `fmt` strings.) But my build times right now are longer than 10 seconds, which is unacceptably slow for a static site.

## Puzzle fixes

I fixed a couple of small things in my older puzzles recently too. In Starry Sums, there was no mention of the positive diagonal restriction in the rules, and I just now added the solution to the example puzzle in Alphabet Soup. (By the way, I really recommend you give Alphabet Soup a try.)
