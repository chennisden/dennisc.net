---
title: Using sshuttle to bypass school wifi
date: 2022-08-17
---

So! School is in session again which sucks, and what really sucks is the school wifi which blocks half the internet. Fortunately, if you're on a Mac, Linux, or BSD machine, it's pretty easy to bypass this.

I use a tool called [sshuttle](https://github.com/sshuttle/sshuttle) to tunnel all my traffic through a server which I have SSH access to. (Installation for sshuttle is straightforward and can be found on the project's README.) This set of conditions is not very common since "people who need to bypass school wifi and have SSH access to a server" basically just describes high school sysadmins.


But if this describes you, here's what you do:

```bash
sshuttle -vvvv -r user@0.0.0.0 0/0
```

where you substitute `user@0.0.0.0` with your server and the user you want to SSH into (the user doesn't actually matter).
