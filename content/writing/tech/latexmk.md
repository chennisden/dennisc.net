---
title: Why latexmk?
date: 2022-03-19
---

latexmk is a build script that allows you to automate the LaTeX building process, running exactly enough times to generate the necessary cross-references.

Well, that sentence told you exactly nothing. So instead of asking what latexmk is, why not ask why we use it?

## Cross Refs

Read [A High-Level Overview of TeX](https://www.dennisc.net/writing/tech/tex-overview), particularly the "Build Process" section.

So LaTeX has to run multiple times in order to get crossrefs to work. But how many times?

With smaller files you can just keep spamming pdflatex until you've gotten all of the references. But

- with bigger files each compilation is fairly expensive,
- and it's nice to just run one command and know it will run exactly as long as it needs to.

Latexmk runs exactly as many times as you need it to. And it doesn't get itself stuck in an infinite loop if a reference is just missing, it stops after seeing that it is missing twice.

Why twice? Well, the first time the reference is supposed to be missing, as it might get written to the aux file after it is called. The second time, it should already be in the aux file, so if it is missing that means it was never written.

## Asymptote

If you've ever used Windows + TeXWorks the official asymptote "solution" is

```bash
pdflatex FILE
asy -f pdf *.asy
pdflatex FILE
```

where the second line is replaced by some script in the TeXWorks compilation options.

This is stupid, we should have one command that does the entire process, and optimally, if and only if there are any asymptote files generated. That's part of what latexmk does (at least, if you configure `.latexmkrc` properly).
