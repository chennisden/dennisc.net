---
title: How I use conventional commits
desc: Don't give a tag to every commit.
date: 2022-07-29
---

I like the general idea of [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/): tag commits so that you can filter them through your pager. However, especially coming from the context of Rust development, I think conventional commits do not work well as stated.

I don't really care if commits are feats or breaking (and in early development, many commits are breaking), so I omit these tags from my commit messages. Later on, I do care if a line of changes is breaking. I totally see the value in saying "this is a breaking change" in the commit description, but when I'm scrolling through all I need are the versions tagged. In practice breaking releases will typically not include changes that are really compatible with the previous major version, because otherwise why wouldn't you release those changes separately first? Also, most commits are features, so I'm saving the extra room in my commit header and not writing `feat`.

Instead, I use the absence of a tag to indicate "this is a normal commit that adds some functionality". When I want to fix a logical error up, `fixup`. For formatting errors (which shouldn't ever slip by since I have a pre-commit hook that runs `cargo fmt --check` among other things), I use the `fmt` tag. Tags should be used sparingly and only for commits that are "weird" in some way, i.e. commits you ideally never would have needed to make to begin with.

Of course with Rust, you have to make commits where you just bump up the version number. Here, a `release` tag would be appropriate.

Tags are things that should be used sparingly for out of the ordinary commits. The fact of the matter is that most commits add functionality of some sort to your program, and indicating that with `feat` dilutes the value of your actual tags. Conventional commits are a good set of guidelines to consider for your repository's Git history, but for me they hold little value as specifications: they are too intrusive on the commit history. Especially since I haven't ever used any conventional commit tooling and honestly don't see the point: I just search for `fixup:` or whatever in my Git shortlog.

Overall I think [my approach is working out for me](https://git.sr.ht/~dennisc/glee/log), and I find my history a little more readable than a history where *everything* is tagged. However, there are cases where tagging can present a more concise way to describe the *scope* of your changes, which is one thing conventional commits do well as is.
