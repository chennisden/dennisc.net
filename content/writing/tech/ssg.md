---
title: What I want from a static site generator
date: 2022-08-09
---

[Hakyll](https://jaspervdj.be/hakyll/) is the best static site generator I've seen (though I haven't used it myself). Unsurprisingly it uses Pandoc under the hood. Jekyll, Hugo, etc basically require you to make another repository just for a *theme*, which is a WTF moment.

What I want on top of Pandoc is

- no complex theming system (Hakyll CSS is good, my `static/` directory is good, Jekyll is bad)
- generate RSS and HTML themes (something like a Pandoc template is probably best for the latter, the former could be made much better than Pandoc + Bash)
- Have LaTeX and Asymptote just *work*. Pandoc can do the former (well, kind of, it can't statically compile KaTeX into HTML which would be ideal) and cannot do the latter.
	
	Note that under the hood, you'd want to replace an `asy` environment with the generated SVG which can trivially be generated with `asy -f svg file.asy`.

Maybe this will be a future project sometime later.
