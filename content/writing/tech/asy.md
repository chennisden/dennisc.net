---
title: Asymptote is the best Asymptote compiler
date: 2022-03-27
---

People joke that they like to use AoPS TeXeR in order to compile their Asymptote diagrams, but enough people seriously believe it's a good idea that I felt I needed to write this post.

Asymptote is a language totally separate from TeX. You should treat it as a stroke of fate that it _happens_ to support some aspects of TeX like denoting labels in math mode. Asymptote is a standalone language and it can compile without the help of a LaTeX document or compiler, thank you very much.

Typically I see people who use Overleaf leveraging TeXeR, because Overleaf is a buggy piece of crap that can't compile Asymptote properly used only by dingleberries who are used to other crappy online editors like Google Docs. (No judgment if you fall in that camp; I used to be this person up until a couple years ago.) If/when they move to a local editor, bad habits carry over for Asymptote, because Overleaf has obscured stuff so much that no one even knows how Asymptote works.

Here is a brief summary of how the LaTeX asymptote package, **which should not be confused for Asymptote the language itself**, works:

- When you run `pdflatex` on your `.tex` file, the asymptote package extracts the code inside every `asy` environment and outputs it to a set of `.asy` files.
- You run asymptote (and this time, it is asymptote proper) on each `.asy` file.
- When you run `pdflatex` again, the asymptote package detects the `.pdf` files that you got by running `asy` on the `.asy` files, and inserts them into the proper location in the PDF.

Actually, this is one of the reasons you need latexmk: to automate this asymptote process whenever necessary. This is because --- and I cannot stress this enough --- **Asymptote is not TeX**. LaTeX cannot build Asymptote for you because LaTeX doesn't implement the compiler, Asymptote does. This is in contrast to TikZ, which does not need a whole separate build process because TikZ _is_ part of TeX.

This means that there is a separate compiler for Asymptote. And it is _fast as fuck_. If you want to test whether an asymptote diagram compiles correctly, and if so what it looks like, here is what you do:

- Copy the asymptote code.
- Open up a terminal.
- `vim test.asy`
- Paste the asymptote code inside `test.asy`.
- `:wq`
- `asy test.asy`
- `zathura test.eps&`

Bonus points if you make an asymptote-specific keybinding for the compile+preview process.[^1]

[^1]: Someone smarter than me could probably make it possible to extract the code _inside_ an asymptote environment, pipe it to some temporary file, compile that temporary file, and preview the result.

If you use Vim you have no excuse for using TeXeR. Doubly so if you are also using a tiling window manager. Why take the time to open a browser, wait for TeXeR to load, manually add the BBCode bullshit that AoPS expects from you, and have TeXeR fail on you for no good reason with about a 20% chance?

So what are the reasons you would use TeXeR? There are two I can think of.

- You can't run LaTeX on your machine for some reason. Perhaps your only computer (maybe it's a phone) is super weak, and you need to leverage the browser to perform expensive calculations. Or perhaps you're stuck at the public library and forgot to bring your laptop.
- You suck at using computers.[^2]

[^2]: In all fairness, most people do. A lot of smart people do as well, which is why I changed this from the much stronger claim "you are an idiot".
