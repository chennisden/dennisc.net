import geometry;

size(8cm);
pair O = (0,0);
pair A = (1.5,0);
pair D = NE;

dot("vertex", O, SW);
label("$\vec{v}=$offset", O--A, S);
dot("vertex + offset", A, SE);
label("dir=NE", D, NE);
draw(O--(0,1), dotted);
draw(O--A);
draw(O--D, EndArrow);

perpendicular(O, NE, A);

shipout(bbox(white,Fill));
