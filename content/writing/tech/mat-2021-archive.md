---
title: MAT 2021 is on mapm
date: 2022-03-14
desc: On creating the Math Advance mapm contest archives and porting MAT 2021 over.
---

I just wrote [a script for Linux users](https://gitlab.com/mathadvance/contest-releases) to fetch a mapm contest from Math Advance. This ensures that every contest can be built from source. As the title says, MAT 2021 was added to this archive of Math Advance contests.[^edits]

[^edits]: Yes, I did make some administrative edits (mostly linting for `\textbf` or `\textit` vs `\emph`, and punctuation inside math mode vs outside).

Additionally I posted a tutorial called [From Zero to mapm](https://gitlab.com/mapm/zero-to-mapm), to bootstrap anyone who is generally competent with computers (you should know the basic shell commands and Git commands) with the tools needed to install mapm. The only runtime dependency is LaTeX and the only buildtime dependency is Rust; the tutorial also instructs the user to install Git because templates should be hosted with Git.

Currently the tutorial only has Mac and Linux, but I plan to write one for Windows soon! I'm just quite rusty with Windows, what with it breaking every convention known to man.

I hope this is as exciting for you as it is for me. Even though we do not have a GUI yet (optimistic estimate, mid-summer), the command-line interface is expressive though a bit awkward at times[^compl], and the documentations, tutorials, examples, etc are quite rich for such a simple program. mapm has been battle-tested against some members of Math Advance, who've gone through the grueling work of setting it up manually, writing documentation themselves (thanks especially to Prajith for the Mac OS documentation, which I used as a reference for "From Zero to mapm"), and it works without major hiccups on all major operating systems.

[^compl]: In particular, no Bash completion yet. I would be very grateful if someone sent in a pull request, but of course, no one besides me is under any obligation to work on it.

If you are writing a math contest and really want it to be good I suggest you try mapm, if you've got a tolerance for dealing with any issues that might arise. The ability to recompile drafts in the matter of seconds is super helpful, especially since we can be confident that all problems are coming from the same source, the mapm problems directory. (I'll probably shill harder in the future once we've got a GUI: for now we are sort of in a beta stage. If that doesn't sound like your cup of tea yet, please come back to mapm in a few months!)

P.S. mapm is always stylized with all lowercase letters, even when it starts a sentence. I sort of implicitly set that convention for myself and now explicitly follow it after I realized. You can feel free to respect or break this in your personal conversations, but any official docs should abide by this. Let me know if they don't.
