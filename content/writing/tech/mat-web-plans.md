---
title: Plans for the MAT website
date: 2022-12-28
---

In [Math Advance is looking for Rust developers](mathadvance-rust), I wrote

> We’ll unfortunately still be using Javascript, because I am not rewriting the frontend.

which implies that the marketing website and the contest portal would still be intermingled. Now, I plan to separate the MAT website into the marketing website and the contest portal.

I intend to use Rust to server-side generate the pages (just like with Glee) and only use the bare minimum of Javascript required (timer and autosubmit when time runs out). Then, I'll rewrite the marketing website using just HTML and CSS, because we really don't need Javascript for that. (Maybe I'll pawn the marketing rewriting off on someone else.)

In semi-related news, I am developing multiple choice support for mapm, and I will be integrating mapm with the contest portal (i.e. you can upload mapm contests). So the portal will be able to hold mock AMCs, and I'm considering opening up a community portal for anyone who wants to host their mocks in addition to the official Math Advance portal. But this is all pretty far off into the future.
