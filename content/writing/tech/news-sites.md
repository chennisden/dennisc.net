---
title: News Sites are Annoying
desc: On the annoying thing news websites do, how to mitigate it, and potentially how to automate it.
date: 2022-11-28
---

Every news website spams you with popups asking you to subscribe, login, etc. This is annoying, but fortunately there is an easy way to bypass it: curl.

You can just run `curl URL -O` to save it as a file, and then open that file in Firefox. This works, but it requires some manual intervention. Unfortunately piping it into Firefox also does not work (because Firefox is a browser, not an actually good application that respects pipes --- ugh!)

But we can tidy it into a script:
```bash
FILENAME=/tmp/$RANDOM.html
curl '$1' -o $FILENAME
firefox $FILENAME
```

Now, the Firefox add-on API does not just let you run a shell script. In theory, you could run a script (using JS --- fucking browsers) to

- intercept certain URLs (you'd need to create such a list, and unfortunately it doesn't seem there's a Googleable list in plaintext)
- run the shell script shown above.

I don't know if it's worth writing a Firefox add-on when the API changes so fast. Maybe when I have more time --- right now I shouldn't be on these news sites anyway.
