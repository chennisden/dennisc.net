---
title: How I plan to organize my Git repos in Glee
date: 2022-09-01
---

Glee stores repositories hierarchically, like a filesystem. I plan to host two Git servers --- one for dennisc.net and another for mathadvance.org.

## dennisc.net

```
glee
dennisc.net
dennisc-gemini
dotfiles
	sway
	i3blocks
	... (nvim, etc.)
	bin
		musiclist
		latex-lint
		... (dlatexmk, etc.)
math
	mvc-am-gm
	linalg-notes
	...
misc
	musiclist
	rust-beam
	beam (planned)
	min-tex-install
tex
	bounce
	nature
password-store (private)
```

## mathadvance.org

```
mapm
	cli
	lib
	gui (planned)
mast
	units
		APV-Log
		...
	unit-drafts
		...
	diagnostics
		season1
		...
	lectures
		...
	mast.cls
    mast.mathadvance.org
contests
	mat.mathadvance.org
	archive
	summer-mat.cls
	winter-mat.cls
books (private)
	mabook.cls
	foray
```
