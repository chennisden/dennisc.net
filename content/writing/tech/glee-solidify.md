---
title: Solidifying Glee's design
date: 2022-07-07
---

> Glee is a Git service meant solely to be self-hosted. It stores repositories in a hierarchical manner, much like GitLab or a UNIX filesystem. The web frontend will be simple and contain no Javascript.
>
> Glee is not meant to serve as an ecosystem, and as such is inherently difficult to monetize or offer as a SaaS. Rather, it is meant to act as one part of a software forge, with issue tracking, wikis, mailing lists, etc being pulled in from elsewhere.
>
> In terms of complexity and functionality, Glee is around the midpoint of cgit and sourcehut.

The above is my current draft for the official marketing slogan on the Glee homepage, which will probably be hosted on glee.dennisc.net.

I've begun work on the design for my Git service: Glee. The repository is currently being hosted on [SourceHut](https://git.sr.ht/~dennisc/glee), though the project is unlisted. It is likely to remain that way forever, since by the time it's ready to make public it will also be ready to self-host. I will have to learn plenty, including diving deep into the internals of Git, HTTP, and SSH in order to pull a project of such scope off.

## Progress

I've already made some progress implementing the registration page!

<div aria-hidden="true" style="border-style: solid; border-width: 1px; padding: 0 1rem 1rem 1rem;">
<style>
a.glee {
	color: #2563eb;
}

a.glee:hover {
	text-decoration: none;
}

a.glee:visited {
	color: MediumBlue;
}

@media (prefers-color-scheme: dark) {
	a.glee {
		color: DodgerBlue;
	}
	a.glee:visited {
		color: #2563eb;
	}
}

form {
	max-width: 400px;
	display: flex;
	flex-direction: column;
}

form > label {
	margin-bottom: 0.25rem;
}

form > input {
	margin-bottom: 0.5rem;
	line-height: 1.5;
	padding: 0.25rem 0.5rem;
	font-size: 1rem;
	font-family: sans-serif;
	border-style: solid;
	border-width: 1px;
	border-color: DarkGray;
}

form > input:read-only {
	background-color: Gainsboro;
}

@media (prefers-color-scheme: dark) {
	form > input {
		color: #f0f0f0;
		background-color: #404040;
		border-style: none;
	}
	form > input:read-only {
		color: #a0a0a0;
		background-color: #282828;
	}
}

form > button {
	margin-top: 1rem;
	color: #f0f0f0;
	background-color: #2563eb;
	border-style: none;
	line-height: 1.5;
	padding: 0.25rem 0.5rem;
	font-size: 1rem;
	font-family: sans-serif;
}
</style>
<h1>Register</h1>
<p>You are registering for an administrator account. If you think this is incorrect, contact an administrator for another invite link with the correct permissions.</p>
<p>All passwords must have a rating of 4 when measured against <a class="glee" href="https://lowe.github.io/tryzxcvbn/">zxcvbn</a>. To make a strong password, either <a class="glee" href="https://xkcd.com/936/?correct=horse&battery=staple">use a string of unrelated words</a> or better yet, use a <a class="glee" href="https://www.passwordstore.org/">password manager</a>.</p>
<form>
<label for="email">Email address (you can't edit this)</label>
<input name="email" readonly type="text" value="dchen@dennisc.net">
<label for="password">Password</label>
<input name="password" required type="password">
<button type="submit">Register</button>
</form>
</div>

As a reminder for how registration will work on Glee, an admin will send an invite link to your email through the admin page and you will use it to register for that specific instance of Glee. This takes advantage of Glee's self-hosting design: since one instance should correspond to exactly one organization, having a user account should automatically confer you with some permissions. 

In Glee, invite codes are sent directly to the email address, which means that if a user registers an account with that invite code, they obviously have access to the email. Thus we skip the email verification step that so many other sites have. Where on other sites you have

- user registers for SaaS (say, GitHub)
- has to manually verify their own email
- sysadmin has to invite them

in a Glee instance, the sysadmin just invites the user and all is taken care of.

Obviously, the only way to register for a Glee instance is with an admin's invite code. Otherwise, there would be no point having invite codes in the first place.

## Self-Hosting

Some issues I want to take care of before self-hosting though, because once I self-host I do not want to wipe the OS of my server with the source of truth for my Git repos:

- Switch to a sane operating system. Ubuntu is utterly unusable as a server OS, and I'm currently still stuck with it. This means either some BSD, Alpine Linux, or Adelie Linux. I recognize that BSD has some technical advantages, but Alpine has the technical advantage of being able to run `apk add sourcehut`.[^lists]
- Get rid of everything consuming a stupid amount of memory, and downgrade from 2GB mem to 1GB[^money]:
    - Mailcow: I don't really want the UI anyway, and this is the biggest memory hog of them all. I can just use Postfix, though migrating the data out of Mailcow and into Postfix will require careful consideration.
    - MAT-web: It's taking 200 fucking megabytes of memory. What the absolute fuck?
    - MAST-web: I am an anti-Javascript crusader anyway, so even if it uses much less memory than MAT-web it has to go too.

[^lists]: Although my mailing lists might not use SourceHut whatsoever, see the final section.

[^money]: The reason I want to downgrade my RAM is because if I halve my memory cap, I also halve my server costs. This, in effect, extends how long Math Advance can run for by a factor of 2.

I also have to time it properly: I can't be nuking Mailcow when we're hosting a contest or contacting sponsors, so this will likely require active coordination with the rest of Math Advance. As of now, my sysadmin plans just remain idle speculation.

Before I get Math Advance's server on a 1GB memory Alpine OS, I might mirror Glee on an instance of Glee as a proof of concept, but it will not be the single source of truth.

## Frontend

I've already been letting out hints of "No JS", and though on my [last post](/writing/tech/glee-addendums) I made some idle speculation about using Svelte(Kit). Fortunately, I realized the errors of my way and promptly discarded that idea.

Basically, the way I'm generating HTML is just some glorified string insertion. For maximum interoperability with the backend, I'm serving HTML files with Rust and directly using the backend libraries. My web framework of choice is Axum[^tide], but I literally do not think it matters which you use for your project. I just get Axum the best out of all the options I've looked at (which isn't a lot!) and you might prefer something different.

[^tide]: I know I said Tide in my last post but I like axum better now. Its ergonomics just make more sense to me, I never really got why we had to have `tide::Result` when you can just have an HTTP Response returned. I'm sure there's a reason, but Axum doesn't behave like that, and a small number of pet peeves like this just added up to my Axum switch for this project.

A JS framework is just way too much to do what `format!` does for you. If you want abstractions, define constants or functions. My experiences have further solidified my belief that JS frameworks solve a problem that no one has.

You might notice that the CSS is pretty much copy-pasted from dennisc.net and mathadvance.org. This is intentional and will remain that way, though I may offer the ability to customize CSS and favicon.svg. Speaking of the CSS style, we've been joking in Math Advance about "dennis.css". I may make it a real thing: a style file with a lot of sensible defaults, meant to be used for inspiration and as a fill-in for other websites until their frontend developers create their own styles.

Also, I'm dropping Redis and just using Postgres, even for state management. I can just run a cronjob to remove any outdated entries, which I will probably put in a `contrib/` directory somewhere. (This doesn't relate to frontend whatsoever, but I had nowhere else to put this.)

## Groups

Here is a very tentative design for groups, which I am actually leaning on *not* implementing:

Group permissions work the same way as user/admin permissions (you set the highest permission on a repo/directory, or just `inherit` it).

You can be in any number of groups, and your permission for some repo is the highest permission of any user group you are in, including the admin group (which makes being in a custom group kind of pointless). It's simple and flat, groups themselves are not stored in a hierarchical manner because that is really difficult to design well and even harder for users to wrap their heads around. (Just look at how GitLab does it --- it's a total disaster!)

## Mailing lists

You may recall that I said the following about programming Glee proper:

> The thing about this sort of design is that it has so few details, someone must have done it before me. Maybe I am wrong and everyone else decided to use 7 levels of access control but only 1 level of nesting (username/repo or org/repo). I hope I am not, though, so if you know a self-hosted Git service that sounds something like this, please let me know so I don’t have to build it myself. Because I would really rather not.

I feel the same way for the hypothetical mailing list I am about to describe, and this time, I can probably approximate what I want without writing a single line of code.

What I want from a mailing list is [lists.sr.ht](https://lists.sr.ht/) plus support for private lists. SourceHut literally already has private lists, so the only reason why I'd want to write my own mailing list is to share the same user database and access control rules as Glee. That is really not a convincing reason to write a whole separate mailing list when SourceHut Lists is really good and the de-facto standard anyway. Thus, I will likely be running a Math Advance instance of SourceHut Lists.

The intended way to use Glee is to combine it with the best mailing list software or issue tracker for your purposes, rather than using an "integrated" suite with a memory requirement that blows past the roof and doesn't exactly do what you want. As such, writing a mailing list and baking it into the Glee package may actually be a net negative.[^flag]

[^flag]: Although I can just gate it behind a feature flag.

## Who won't Glee be for?

Individuals may see compelling reason to use Glee --- I am considering creating a separate instance of Glee for my personal projects --- but they are not the group being optimized for. For individuals who are self-hosting and don't plan on accepting outside contributions, CGit may be the better option. You will have to set up git-http-backend yourself, so convenience may be a compelling reason to use Glee.

The point of Glee is to provide a seamless experience for small organizations with a semi-experienced development team (so no newbies). You can see this in the registration design, the lack of a web IDE[^shit], and the commitment to low RAM and a lightweight web UI.[^kb]

[^shit]: Web IDEs enable and perpetuate shitty committing practices, which is why I consider them an anti-feature. However, objectively speaking, it does lower the bar of contribution --- if only temporarily.

[^kb]: The size of the registration page is just over **1 KB**. SourceHut's is just under 4 KB, which is a perfectly reasonable size (the difference is just pocket change and probably can be accounted for by the extra text on sr.ht's page), but everyone else can basically go die in a fire. GitHub manages to send 5 MB of bullshit (and that's not counting what UBlock Origin blocked) and none of it is cached. GitLab does a lot better, with *only* 2.4 MB --- as if. Half of way too much is still too much. Codeberg is a lot better and only sends about 20KB, except when it's busy sending me megabytes worth of styles and fonts, and Gitea, which Codeberg is based off of, has a size around that of GitLab's.

    All numbers are uncompressed, by the way.

## What's next?

As of the time of writing I will be going to Europe for just under 3 weeks. This means I will make no progress on Glee. After that, I expect authentication to take around two weeks, and it may take several months of research to properly handle Git repos, including HTTP operations (read, so clone/pull) and SSH operations (read/write). Since I've already covered account creation and authentication, my next post will likely be on handling HTTPS/SSH Git requests and access control (resolving permissions recursively).

By the way, I don't plan on setting branch access control policies. This will make Glee unsuitable for large groups with inexperienced (or just lazy/inept) developers, because there is no guarantee some dumbass doesn't (force) push to master. It reduces the complexity of the software (said complexity is a big reason I don't like GitLab as much as I used to, and part of what spurred me to write Glee).
