---
title: Linux Distros
date: 2022-07-05
---

You've all heard it: "All Linux distros are the same". This came in response to distro-hoppers basically just trying out reskins of Arch/Debian with a different desktop environment, many of which ultimately provide a more opaque experience. In that context, they are totally right. Debian is still Debian under the hood, no matter what coat of paint you apply to it. But distros have very real differences, some of which you cannot just "patch out" or "work around" without serious effort and a dedicated team. For example, "Arch Linux but without systemd" is a whole separate distribution that has to be maintained (Artix).

So what are the main differences? Here's a list in roughly descending order of significance:

- package manager (apt, dnf, pacman, etc...)
- libc implementation (for almost every distro it's glibc)
- init system
- kernel version
- default desktop environment installed and default theme (important for overall "market share of Linux", which I don't really give a rat's ass about, but totally unimportant for an individual user)

Obviously this post will be opinionated, otherwise it'd be useless.[^1] So for transparency, my allegiances:

[^1]: "So Debian uses `apt` and you can do `apt install BLAH` while Arch uses `pacman` and you can do `pacman -S BLAH`" --- real helpful, buddy. I can read the manpages myself.

- apk
- musl libc (but some people *need* glibc)
- runit, but openrc is also quite good
- I typically run a very recent kernel.
- sway (but please don't install shit with my distro, I'd rather login from a tty and manually run `sway`)

I have three machines --- a desktop running on Bedrock, an old laptop on Alpine Linux, and a new laptop running Void Linux.

I'll be going through the distros in roughly descending order of popularity. A couple of distros I know exist but decided to omit due to lack of knowledge are Gentoo, Fedora, and RHEL/CentOS. Since most systems use glibc/systemd (and the purism of your libc and init system probably don't matter much to you), I will make no comment unless a distro uses something other than glibc/systemd.

This post isn't really meant for Linux beginners, by the way, although I think Linux beginners can glean some useful information here.

## Debian-based (e.g. Ubuntu)

Even though they certainly have a place in the Linux world, I really don't like Debian-based distributions (mostly Ubuntu, which I will primarily focus on for the rest of this section). That said, if you use Ubuntu, you are by no means a "bad Linux user" --- ease of installation should be a pride point and there's nothing cool about pre-archinstall Arch being super hard to install.

Debian's package manager (technically frontend for the real package manager `dpkg`, but come on, who actually uses `dpkg` for anything besides `.deb` files) `apt` is one of the more intuitive package managers, at least on the surface. But this is only on the surface. Dependency management is hell and there are a bajillion different binaries you need to actually manage your system. It's also super slow and super out of date. It is literally an order of magnitude slower than `pacman`, and `texlive`/`pandoc`, two fairly significant programs, are *years behind* (texlive is on 2020 and the stable pandoc package is using a version from 2019). Of course, you can mitigate this somewhat by going on an unstable branch, but even then there is no way to get anything super recent. For programs like pandoc, there are subtle breaking differences between one version and the next, and all things consider you really want to be on latest.

(Yes, I know Debian recommends `aptitude`, but the fact that exactly 0 Ubuntu/Mint users know it exists is kind of my point. There are just way too many frontends for `dpkg` and no one can make heads or tails out of it!)

That said, it is almost always not a big deal to have a Debian server, because most critical infrastructure is up to date enough. You really shouldn't even have Pandoc installed on your server, because the way you are supposed to update a website is by building and then sending a tarball with your files to prod... same for TeX Live, really. However, I'll probably stop using Debian when I clean-install a new server because I see very little technical advantage in continuing to use it. Desktop though? Yeah... no. Have fun adding PPAs for shit while everyone else is using the AUR or xbps-src.

Also re Ubuntu snaps: LOL, let's hate on Ubuntu for a bit.

- It's closed source. (Yeah, parts of it are open source, but you cannot make a useful Snap app store because you don't have the closed-source backend.)
- It's slow as FUCK, and not just for installations --- apparently Firefox takes 15 seconds to start if you're calling the Snap. (This is likely an exaggeration and not collected using `time`.)
- Chromium and Firefox are snaps by default. If you run `apt install chromium` you get a Snap. Linux is all about transparency. Even if you have nothing against snaps, there is literally no reason for `apt` to use anything other than a `.deb` file from either the Debian repositories or a close downstream derivative.

At this point Ubuntu is literally pulling the wool over your eyes much like Windows (but not as bad). Sure, everything is technically documented, but everything's so opaque by merit of "a bunch of crap no one wants is shoved in your face." So yeah, for new users: I don't really recommend Ubuntu. Ubuntu also asks you to update your software a bajillion times. Mint patches out the Ubuntu-specific bullshit, so it seems to be a better choice for now, but I really recommend `archinstall` and [setting up Sway on Arch](https://wiki.archlinux.org/title/Sway) if you have a weekend to spare instead.

## Arch

Arch's package manager `pacman`, unlike `apt`, is fast, recent, and totally unintuitive. Where in Debian you run `apt install` or in Alpine you run `apk add`, in Arch you run... `pacman -S`. And then to update, instead of running `apt update && apt upgrade` you run `pacman -Syu`. The options are literally alphabet soup, and you could certainly understand pacman with some effort, but when you compare to the good package managers which don't require any time to understand `pacman` leaves something to be desired. Also `pacman` is kind of slow, but my standards for speed are based on xbps/apk now.

On the bright side literally anything you could ever want is in the AUR. On the not-so-bright side, as nice as some AUR wrappers are, I just want to manage my dependencies with pacman and I can't. (Well, unless you use the Chaotic AUR --- but that's a huge mess too for reasons I won't get into right now.)

Arch is also very DIY, just like pretty much every not-Debian distro. You have to set up Xorg/Wayland on your own (though a lot of Systemd-related stuff is just installed and set up for you). Networking is also annoying, just like on every other distro.

## Alpine

Alpine is my favorite distro. It doesn't do everything, but for the people who'll be satisfied with its capabilities it is the best in its class by far. And its limitations aren't because of technical flaws with Alpine, but flaws with the programs you want to run. (Namely, companies who release proprietary binaries only build them against glibc, so if you use musl libc, you're screwed.)

The main sell of Alpine for me is the `apk` package manager, particularly how small and neatly divided each of the packages are. For instance, package `foo` usually does not include docs, and the docs are contained in `foo-docs`. This is because if you're on a server you don't need the docs, you should be reading the manpages on your own system. Granted, docs don't really take up that much room, so it's not the biggest deal. For your development environment, you can and should just install the `docs` metapackage to get the documentation for each program you install.

`apk` is super intuitive too. If you want to find the package that contains file `foo`, simply run `apk search foo`. `apk` is also much faster than any other package manager, including `pacman` and `xbps`. It's expressive enough to do anything you'd ever want and I don't know why you'd ever use another package manager at this point.

Since Alpine uses OpenRC, not systemd, and barely has anything configured by default, you may have to do some basic hardware configuration yourself. For instance, in every other distro

- the brightness value on boot is taken from whatever it was on shutdown
- wifi autoconnects pretty easily
- laptops hibernate on close lid

yet with Alpine, I had to set all these up on my own.

Alpine does have limitations, but there are ways to get around it. You can use a chroot of a different operating system (or have Bedrock handle it for you) or a universal package manager like Flatpak, which bundles it dependencies as well as the binary. You can choose to use different software --- and I don't mean to go all FSF "you must never under any circumstances use proprietary software" here. There are genuinely good interfaces that still maintain compatibility with the proprietary backend. For instance, `6cord` is a good Discord terminal client that just works and `rclone` lets you treat your Dropbox folder as a remote filesystem. Steam not working is a bit of a pain point, but I grind Spider Solitaire in KPatience all day anyway so it's OK for me. In fact if you want to focus, Alpine is a really good distro for a work environment.

Adelie Linux also seems very similar to Alpine Linux (I haven't personally used it myself). It uses the same package manager (apk), the same init system (OpenRC), but a different userland (Adelie uses GNU while Alpine uses BusyBox). I get why Adelie uses GNU coreutils --- BusyBox does have some problems, like `sort` not seeing whitespace as a separator by default when you pass in the `-t` flag.[^fixed] (I do like BusyBox a lot though! Full disclosure, I use BusyBox on most my systems.) The `gcompat` library, a glibc compatibility layer, is also developed for Adelie, so that is one of the many reasons Adelie might be worth considering. (`gcompat` also can be installed on Alpine, though I've had zero success in getting it to work.)

[^fixed]: [This was fixed in Git.](https://git.busybox.net/busybox/commit/?id=00f2a35b835c6f49617f5379073e9063e7e683ce)

## Void

Void is similar to Alpine but it uses runit (IMO a better init system since it makes you manually symlink stuff, which is basically what happens under the hood with `systemctl enable` but without any black magic). The package manager is very similar, but unlike Alpine, Void also lets you build a vast array of packages from source with `xbps-src`. Everything under the sun can be found there (even PolyMC, a Minecraft launcher!) though it does feel a bit clumsy compared to the AUR.

Void also uses glibc unlike Alpine, so pretty much everything is compatible with Void. (You can choose to use musl libc if you want, but to me that eliminates 90% of the benefit of using Void over Alpine.) For 99% of people this is the ideal desktop/laptop distro.

## Bedrock

You know how you can make a chroot of one distro in another (usually done for compatibility purposes)? Bedrock is like that but on steroids. You can mix and match coreutils, kernel version, package managers, etc.

However, making a chroot directory for another distro is a reversible operation, while converting to Bedrock is not. Thus I only suggest it if you are totally out of options otherwise. (I use it on my desktop because Gnome/Ubuntu just *works* with NVIDIA monitors while everything else is totally pure agony, though if I were to set it up all over again, I wouldn't.)

## Verdict

For servers, use Alpine, hands down. The proprietary programs you're "missing" for Alpine are mostly for personal use, you shouldn't be running a server with proprietary programs anyway. (That said, if you want to run Alpine in production you probably should also run it on your dev machine.)

Personal computers are a bit more complicated. If you want to lean more heavily into gaming with Steam use Arch or Void. I personally think that if you're going to install more "random" packages[^proprietary], Arch is the better choice, mostly because I hate xbps-src. I run Alpine on my "work" laptop, but Dropbox, Discord (I use 6cord, a terminal client for Discord instead on Alpine), and Steam are all major PITAs.

[^proprietary]: Mostly proprietary crap that you only use because everyone else does, and you have enough social sense to not go all RMS and tell your teacher "But I can't use Zoom, it's proprietary!"

There's also an argument for installing Bedrock on top of a sane base like Alpine Linux. It's like chrooting but you don't actually have to care about building packages yourself, or setting up the environment. It is a bit bulkier though since you pretty much have most of a distro's base installed, minus the init parts.

Final note: If you are a soon-to-be new Linux user planning to install a "user-friendly" distro (usually a Debian-based distro, and said Debian-based distro is almost always Ubuntu), don't let my criticism of Ubuntu stop you. For all the flaws of Ubuntu, it is still bona fide Linux and a thousand times better than Windows/Mac.
