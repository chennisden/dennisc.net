---
title: Bedrock Linux
date: 2021-12-21
desc: My Linux setup (and what to use when setting up Arch is way too hard).
---

> Can't set up Arch Linux? Maybe that's just because you're a dumb-dumb. Maybe you just aren't smart enough, and deserve to use Noobuntu for the rest of your life. You'll never get to use the AUR, muwahahaha...

Until you learn how to use [Bedrock Linux](https://bedrocklinux.org/), that is...[^read]

[^read]: It would be a good idea to briefly read over the website before returning to this post.

## My brl setup

A long, long time ago, I installed Arch manually on my laptop, so it was the distro that I hijacked and it is the distro I boot into. For my desktop, Ubuntu is the distro I boot into, because I accidentally nuked my system by doing a `sudo rm -rf` in the wrong place at the wrong time.

On my desktop, I use Ubuntu to work with my hardware (including, amazingly enough, an XP-Pen Pro). I'm sure I could set it up with Arch as well, but I do not know nor care how. On my laptop, which is a Lenovo IdeaPad, the hardware has been very cooperative with me and easy to set up, so after adding a couple of scripts to my i3 config it was up and running.

For most of my software, I use the Arch package manager. It's incredibly fast and between the official repos and the AUR, pretty much everything is there. If I'm really particular about a particular program (say, one I use often for work like Vim), then I will compile it myself with Gentoo.[^vim]

[^vim]:
    I bring up vim specifically because in Arch, it does not come with X11 support (i.e. no `+` or `*` registers). I've tried configuring my own custom vim build with pacman, but because my brain is small and I latered switched to brl with Gentoo as one of my strata, I gave up pretty quickly. Gentoo is better suited to this sort of thing anyway.

    And yes, I know neovim exists.

If I'm feeling especially braindead one day and want to copy some Ubuntu tutorial on the internet, or get something that's really annoying in Arch (looking at you, printer drivers!) I have an Ubuntu stratum on both my desktop and laptop.

### Installation

Because I liked the idea of Bedrock almost immediately, have some of the biggest cajones on planet Earth, and have literally no important files on my computer (my dotfiles and code are in Git repos AND synced with Dropbox[^dropbox]), I just went ahead and installed it on bare metal. So I don't get flamed, I'm going to pretend that I totally have a backup of my old system somewhere, even though I literally have never used it in forever.

[^dropbox]: Well, just my code, actually. This is so I don't have to make some nonsense commit, push it to a nonsense branch, and delete it later.

Maybe you don't want to commit to installing Bedrock just yet and want to use it in a VM or something. You can do that and see how it goes, but from my experience if you're using Ubuntu/Arch it really makes no difference anyways, nothing breaks. Running recovery scripts, though, does get a lot more annoying since you can't just `chroot` in `/`, you gotta chroot into `/bedrock/strata/<some_strata>`.

If you have an already existing machine, installing brl is so simple that the official documentation is actually all you need for once. Since you'd already have a desktop environment (and I'm counting the likes of twms like dwm and i3) setup, there's not much to say on that front. I think what's most interesting is how to set up a new computer.

What I do with new machines (and indeed, how I setup my desktop after bricking my old partition and giving up) is install an Ubuntu variant called [Regolith](https://regolith-linux.org/). It's like Ubuntu but with `i3` on top of GNOME. It's a bit annoying if you already have your own `i3` dotfiles and want to use them instead, and you will have to Google where Regolith puts its own i3 config files so you can symlink yours there. Then I just clone my dotfiles, run dotbot's install command, fetch Arch and Gentoo, and just setup whatever apps I need.

Fortunately all my hardware cooperates with Linux more or less (though pulseaudio volume scaling is annoying to deal with and is the only thing that doesn't work out of the box), your experiences may differ.
