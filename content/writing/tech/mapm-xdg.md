---
title: mapm and XDG
date: 2022-03-04
---

Recent versions of the mapm-cli tool put all the mapm files in the `.config/mapm` directory (i.e. `$XDG_CONFIG`). I don't know if this is entirely correct, particularly for `profile` and `problems`, but here are my justifications.

Data that a user manually edits makes the most sense in config. If you look at `.local/share` on your machine, chances are that it's filled with incomprehensible machine-readable but not human-readable data. (And that it's all from GUI prorams.) And even though problems are not config files in the traditional sense, you are writing `key: val` pairs anyway, so in spirit it really is a config file.

Also, it makes things ten times simpler if all mapm data is stored inside the same directory. This is why `profile`, which arguably _does_ belong in `$XDG_DATA_DIR`, stays in `.config/mapm` just like the rest of mapm-cli's files.

As for mapm-gui, comments etc will not be directly edited by users, but will be interacted through the mapm gui interface. So I am thinking that mapm gui information will go in `$XDG_DATA_DIR`. That makes the setup process a bit more complicated for people working together, but whoever's in charge of tech in an organization using mapm should be competent enough to write an install script that abstracts it all away. (That "someone" in Math Advance is me right now.)

A little off-topic, but thoughts on mapm-gui: since LinkedHashMap doesn't implement Serde in Rust (or is it the other way around?) there are no guarantees for the structural integrity of problem files, _particularly with respect to comments_, if you edit fields through an interface. So I am thinking that similar to a forum post, you see the "rich text render" when you are not editing, and when you are editing the problem yml itself pops up in a pane somewhere. Trying to work out manual text replacement/editing seems too buggy to be worth it, especially since YAML is pretty simple/readable anyway.
