---
title: Why I Switched to GitLab
date: 2021-11-09
desc: On migrating all of my (serious) projects, including mathadvance, from GitHub to GitLab.
---

Last weekend I migrated all of my serious GitHub projects over to GitLab ([@chennis](https://gitlab.com/chennis)). A couple of my friends, who are hobbyists who haven't developed as much, asked me why I'm making this move and why I'm not using GitHub instead, especially since it's so much more popular. Well, here's the reasoning behind my decision.

## Git, GitHub, and GitLab

For a well-informed discussion about these two tools, it's necessary to get a clear understanding of what they _are_ first. (If you don't already know the difference, this is just a brief introduction --- I recommend you look this up in greater detail on your own time.)

Git is a version control software. If you're using the command line to interact with Git, then all the commands you run are built on Git. They have nothing to do with GitHub or GitLab.

Git hosting services, like GitHub and GitLab, are just places where you can host Git repositories so as to centralize them, to some extent. This way people actually have a codebase they can consistently _reference_, which is especially helpful for beginners.

The important concept to grasp is the direction of dependency. GitHub/Lab rely on Git, not the other way around.

### GitLab

The term "GitLab" is even trickier to define because it can mean two sort-of different things. Because GitLab is open source (hooray!) you can host your own instance of GitLab. So when I opened with "I moved all my serious projects to GitLab," it was actually ambiguous: did I mean the website [gitlab.com](https://gitlab.com), or my own private GitLab instance?

It turns out that for people like me (developers with their own server), it doesn't really matter. I can migrate my GitLab projects from the public GitLab website onto my own private Git server, and vice versa. The reason I'm hosting on the GitLab website is because of _accessibility_. This way, all my personal code is publicly accessible, and whatever non-sensitive Math Advance stuff is also publicly accessible. It hasn't been long enough after the switch for Google to start caring about my GitLab projects, but I'm sure that over time it'll eventually start displaying something vaguely related.

## The Good

Every comparison of GitHub and GitLab I see lists features of each tool and does a superficial side-by-side of them. The conclusion is always the same: there are reasons to choose one or the other, it depends on your circumstances. This is fair and all, but not very helpful if you don't actually elaborate on which tool better fits which circumstances. In my eyes, **GitLab is vastly superior** when it comes to organizations that are creating a large number of discrete things.

Keep in mind that I didn't say a large organization, just a large number of discrete things. For instance, MAST has a lot of units (around 30 at least at the moment), even though the team actively working on them is quite small. In contrast, if your organization is really only working on one project or website or whatever, GitHub will be fine, though I don't think GitLab is inferior for these purposes either.[^consistency]

[^consistency]: The reason I'm going to stay on GitLab even for personal projects or one-project groups is because of _consistency_. It's easier to use the same service for everything, and GitLab is perfectly good even when you don't have complicated directory structures. And even then, I might realize that I need a group or some subgroups (see the disastrous organization of my TeX repos in GitHub), and I can just move stuff.

**The real nice thing about GitLab is groups/subgroups.** Groups function similarly to organizations in GitHub. But you can also think of a group as a root directory, and subgroups as nested directories. Learning this is what _finally_ got me to make the switch over.

Back when MAST was still using GitHub, we had _one_ repository with all the units inside of it, along with some extra garbage. Why in tarnation would we ever do that, when the LaTeX for two separate units has _absolutely no relation to each other_? There was absolutely no reason for it --- not even when using GitHub --- but I believe that one reason was because there were no nested directories in GitHub. If I made a separate repository for each MAST unit, I'd end up flooding the organization with `mathadvance/CQV-Perspectives` around thirty times over. Oh, and I'd also have to grant access to the MAST team for _every single one_ of these repositories.[^api] Maybe keeping everything in the same repository _wasn't_ a mistake, and that is horrifying.

[^api]:
    It's possible to write a script that uses
    [GitHub's API](https://docs.github.com/en/rest/reference/actions#enable-a-selected-repository-for-github-actions-in-an-organization)
    to programmatically grant access to your team in, say,
    every folder in a directory.
    But at this point you're better off managing your own Git server, whereas on GitLab, permissions are _inherited_.

In GitLab we have `mathadvance/MAST/units` as a subgroup, and unit repositories in the subgroup. Much simpler, and navigating through the group actually makes sense now without external READMEs or whatever, which helps with DRY --- no need to note in separate places how your projects are organized if it's self evident.

## Differences

These are not issues with GitLab. Rather, they are challenges that I faced because I was used to GitHub, or just things that are plain _different_.

First off, GitLab doesn't have the equivalent of GitHub for students, or whatever it's called, that gives students free GitHub PRO. As far as I'm concerned there is no significant difference --- if your organization or codebase is big enough that the limits start posing an issue, you can afford to pay for whatever you're using. (Or self-host on a server.)

Something I really miss is a personal README (a la [chennisden/chennisden](https://github.com/chennisden/chennisden) on GitHub). This is totally a vanity feature, and I don't need it whatsoever because I have a _website_ to show off my tech stuff. But it'd still be nice to have it on my GitLab profile too.

I think GitLab by design encourages a lot more _caution_, which probably makes it more suited for organizations whereas GitHub might be better for individuals who are just starting out.[^ssh] Repositories (or projects, as GitLab calls them) are private by default. And by default you have to wait seven days for a project to soft-delete in a group. Then there's how GitLab interacts with Git: by default, you can't delete protected branches (or any branches, depending on who you are), you can't force push, etc. It's nice to have this here, actually, because I only really need the ability to force push in certain repositories anyway.

[^ssh]: The one exception, though, is authentication: GitHub forces you to authenticate with an SSH key, whereas in GitLab, you can just use a plain username/password combo.

The one real struggle I had with GitLab was the lack of a central CLI tool. There are plenty: there's one in Go, several in Ruby, one in NPM as well --- and depending on what you search for, your first result will be totally different. And some of these CLIs have incredibly abtruse documentation; I couldn't figure out how to specify a (sub)group to make a project in for one of them, for crying out loud. In contrast, GitHub's official CLI is very self-explanatory, and you can use it without having to search for documentation.

I will be explicit and say that I think GLab is the best of these by far. In fact, it's might be better than GitHub's official CLI tool is for GitHub. And, at least on Arch, it's just as easy to install --- one `yay -S gitlab-glab-bin` and you are done.

## Concluding Thoughts

Contrary to what it might seem like, this isn't me trying to get _everyone_ reading to start using GitLab instead of GitHub --- just _some_ people. Specifically, if you're doing organization-style stuff you might want to check out GitLab. But if you're just a beginner with two wholly unrelated repositories, the granular level of control GitLab gives you is probably not necessary _yet_.

Something I never mentioned yet is that GitLab makes it very easy to import your GitHub repositories. This made it a lot easier for me to migrate over, but if you have more projects you may want to consider scripting with GLab instead and doing everything on the terminal.

### Addendum

Update from Dec 5 2021: Apparently you cannot create subgroups under a user. This kinda sucks, and apparently [I'm not the only one who thinks this](https://gitlab.com/gitlab-org/gitlab/-/issues/19944). The workaround I'm using is just making a group called 'chennisden' but that's kinda nasty to be honest.
