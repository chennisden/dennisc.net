---
title: Two Kinds of Bad
desc: What flaws in exposition are reparable, and what kinds of flaws aren't?
date: 2022-04-23
---

Let's say Alice and Bob are doing a competitive programming contest like USACO, and they happen to be stuck on the same problem that requires an $O(n\log n)$ solution. Alice has an algorithm with $O(n\log n)$ time complexity, but her implementation doesn't even compile. Perhaps she doesn't know how to use C++ and thus the arrows in her `cin`s and `cout`s are all in the wrong direction, or maybe the indices in her for loops are off by one. Meanwhile, Bob can't get the last few test cases because he has a perfectly working implementation of an $O(n^2)$ algorithm.

Which situation would you rather be in?

## Against local optimizations

In most situations you'd rather in Alice's situation: even if what you have doesn't work at all, with a bit of debugging your code will do exactly what you want. In contrast, no matter how much Bob improves his code, whether he improves the constant factor by $10\%$ or $100\times$, he'll be no closer to really solving the problem, besides whatever intuition his $O(n^2)$ algorithm has given him.

The difference between Alice's code and Bob's code is that hers is reparably bad while his is irreparably bad. With Alice, you can give specific pointers to fix the code: line 11 is missing a semicolon, for loops must be scoped by braces, etc. With Bob, all you can say is, "your algorithm is $O(n^2)$ where this problem requires an $O(n\log n)$ solution, start over".

If you accept that Alice's code is better than Bob's, then you accept that it is possible for 0 points to be, at some intermediate point in time, better than 200 points. And this is when you strictly define "better" as "will get me more points in the end"! In other words, there are cases where **your score now is not the best predictor for your final score**.

I used competitive programming as my primary example because it's the most precise: your optimizations can get you something tangible (points). But a good non-technical analogue, though not as precise, is writing.

When you revise your essays there are two things you can do: make local optimizations, like fixing spelling or replacing awkward syntax, or fundamentally expanding on and changing the ideas in your writing.[^english]

[^english]:
    On that note, I think English class totally misses the point of writing because it focuses on minuta like not starting your sentences with "and" or MLA citations, instead of letting people discover, "What kind of writing am I good at, what do I want to write, and how do I want to go about it?"

    I'm pretty pessimistic about English classes because you can't teach coming up with ideas. You can learn it and get better at it the same way you can get better at running, but it's something that comes mostly with practice and experience.

With writing, I find that it's easy to get stuck in the trap of local optimizations. You can spend forever fixing up sentences while still feeling doubtful about the whole essay. Conversely, I find that any essay I end up publishing is one I like from the get-go, polished or not.

When I say "writing" I don't just mean my essays. It took me a year to write my first draft of my geometry textbook and I wasted the better part of the next two years making local optimizations. I ended up starting over when I realized that most of my exposition didn't really have a core --- it was just a collection of shoddy notes.

## Reparable vs. Irreparable

The "two kinds of bad" I alluded to in the title are the reparably bad and the irreparably bad, and you really don't want the latter (it's not fixable, by _definition_). Given this, I think people don't think enough about the difference between the reparably and irreparably bad. Because for the former, the best thing to do is to find and stamp out the flaws, whereas for the latter you should just give up and start over.

So what does irreparably bad mean? The pedantic might point out that technically, anything can be repaired if you try hard enough. But that's not a particularly useful definition. Instead, I think something is irreparably bad if it takes less effort to start from scratch than to fix it.

In elementary school (and television[^anime] for elementary schoolers) you get told to never give up. This advice is terrible, because presumably the reason you don't give up is because you can make things better or accomplish your goal or whatever it is. But unlike Attack on Titan[^rumbling], in real life you will rarely solve your problems just by approaching them headstrong without thinking. If your goal is "I want to solve this USAMO geo" you don't just commit yourself to bashing it, you have to learn to give up when it doesn't pan out. And when I say "when it doesn't pan out", I really mean "when you notice bashing is an irreparably bad approach."

[^anime]: When I was that age anime was the worst offender. Maybe it's different now.
[^rumbling]: (Spoiler alert, although at the time of writing anyone who's watched the anime knows this already.) When I say "Attack on Titan" I mean "Eren screaming that he'll kill the titans." And hell, when the existence of Marley is revealed, Eren realizes "I can't just kill Titans, my way of thinking is **fundamentally flawed** because Titans are Eldians and Marley is going to invade the crap out of us". So even in anime, you can't just decide you want to solve X really really badly without being willing to give up and take completely different approaches.

But when you're working on something and a part of it is fundamentally flawed, the situation isn't as bleak as "you wasted all of your time, now start over". Take math contests as an example: there are multiple ways a contest draft can be fundamentally flawed. It might be the case that all the problems are bad, which is the worst case scenario since writing problems is the bulk of the difficulty in compiling a good contest. But it could also be a bad contest format, or a bad subject/difficulty distribution.[^reparable] Even if you have to start over, like I did with my geometry textbook, you will still have learned something through the process.

[^reparable]: Depending on how bad the subject/difficulty distribution is, it might even fall in the "reparable" category.

Even on the scale of individual problems there are reparable flaws and irreparable flaws. A reparable problem might have typoes, a bad answer extract, or a stupid formulation, whereas an irreparable problem just does not have an interesting idea.

It's not entirely accurate for me to say "fundamental flaws", emphasis on "flaws". More often than not, something is irreparably bad if it's _missing_ something, rather than having something extra.[^docs] This is mostly semantics, though, and you can probably just substitute "fundamentally flawed" with "irreparably bad."

[^docs]: Not always, though. Writing lecture materials in Microsoft Word (Google Docs counts, since it uses the same file format) would make them irreparably bad, and here the word "flaw" would apply.

Ideally whatever you make isn't flawed in any way, fundamentally or not. But especially for first attempts (e.g. first drafts of a novel), it's usually not possible to make sure everything is perfect as you're coming up with it. Your time, energy, and abilities are all limited.

When people tell you the first thing you write will be bad they're usually right. In general, even if it isn't your first rodeo, what you make is going to be flawed in some way. But you can control, to some extent, what flaws you catch and how quickly you catch them. **You can control what kind of bad it is.** So when you're writing something, whether it be a program, a novel, an essay, or a textbook, spend more time making sure there aren't any irreparable flaws. The reparable flaws are just that: _reparable_. You can always deal with them later.[^o]

[^o]: Another way you can look at this is: how much will the problem compound if I leave it alone? You can probably use big-$O$ as a framework to think about it (exercise left to reader).
