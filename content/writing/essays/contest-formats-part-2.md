---
title: Another way to tell if your contest format sucks
date: 2022-12-26
---

Quick! How many problems can you recall from

- MATHCOUNTS?
- The AMC 8/10/12?
- AIME?
- USAMO/IMO?
- College contests like HMMT?

If your answers are anything like mine, you'll have noticed a pattern: *contests with less problems are more memorable*.

On the surface this is a really obvious observation. If there are less problems, it's easier to remember each individual problem. But why does this matter?

The primary purpose of math contests is education. You want good, thoughtful problems on contests, and you want to learn as much as you can for each problem. This is why, besides "for fun" events like Number Sense, you don't really want filler problems to appear on contests.

The second purpose of math contests is for math socialization. You conveniently bring everyone together to do the same set of problems, thereby giving them something to talk about: said problems. But especially with MATHCOUNTS and HMMT, there are so many problems that *the problems one contestant wants to discuss can easily be forgotten by other contestants*. And this is literally seconds after the contest ends!

So here is the second way to tell if your contest format sucks: if you have more problems in one contest than you can remember, the format sucks. If you have so many problems that a contestant cannot reasonably discuss them all, the format sucks. Put a small number of problems in your contests so everyone can learn from every problem. 
