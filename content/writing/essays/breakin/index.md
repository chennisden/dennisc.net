---
title: The break-in is all in my head
date: 2023-07-30
---

When I set puzzles, obviously I will use setting software or at the very least, pen and paper. But increasingly, I find that I am setting my breakins entirely in my head.

Not that I'll have all the details down: many times I will adjust the breakin so it flows more smoothly or because it subtly breaks in some way. Often I don't even determine important aspects of the puzzle like, "how many rows and columns will it have?" or "where will the breakin be?" Nevertheless, the entire *idea* of the breakin is in my head.

As an example, take my latest puzzle *River*. The main idea is that Renbans of length 4 or greater must have at least one of 46. This is about how far I was able to get with the breakin in my head:

<img src="/writing/essays/breakin/river.svg" alt="Image of River breakin.">

I've mostly begun doing this out of necessity: there are a lot of situations where I don't have access to setting software (sleeping, dinner with family friends, graduation, etc) and I get bored easily, so I start thinking of puzzles in my head. But I do think that constructing breakins in your head does ensure they are good: it ensures that there is actually some idea behind the breakin, rather than just being a series of details to whittle down. Because if your "breakin" is just a bunch of uninteresting details, you won't be able to remember all that in your head.

P.S. If you spot me spaced out, it's probably because I'm constructing a puzzle in my head.
