---
title: Idea Density
date: 2023-03-27
---

The reason you can watch TikTok for 6 hours straight but can barely get through 2 pages of your real analysis textbook is because they have different idea densities. In other words, there's much more substance behind real analysis than an average TikTok.[^caveat]

[^caveat]: One important caveat to make: the *actual* idea density of, say, some guy doing a trickshot is probably really high: there's a lot of knowledge you need to actually land it. But the *communicated* idea density, which is what really matters here, is low.

This is obvious, but I think the framework of idea density is pretty useful, especially since it'll help you compare apples to apples. Let me explain why.

## Attention spans

The common refrain is that "kids these days have no attention span", and this is a bad approximation of what is actually happening --- why else do people keep tabs on what Brittney sent on SnapChat, and oh my god did you hear Samantha got accepted to Harvard? Arguably people are better at paying attention now (or predatory companies are better at getting it now). It's clearly just *sometimes* that people can't pay attention.

What's actually happening is that the idea density people default to has drastically decreased. For instance, people used to use phonebooks and actual paper maps, instead of a Contacts list and Google Maps. Most of us zoomers (myself included) would start sobbing hysterically if asked to do the former. We've also gotten accustomed to rapid context switching, such as looking at your phone every 5 seconds while doing work.

Most people would quickly categorize both these phenomenons as "bad". I'm not so keen on agreeing with that; my opinion is mostly neutral, in that I believe "this is just a thing that's happening" rather than "this is inherently good or bad". But the bad effects it does have are undeniable, and crucially, I think they can be somewhat mitigated.

The most obvious and most important mitigation is to expose yourself to a wider range of idea densities. Since the average person spends most their time with low density media, *in general*, people should try to increase their time with high density media. On the flipside, if you're the guy always reading stuff like Slate Star Codex or Scott Aaronson, maybe try watching Cocaine Bear or something too.

Let me take a step back and actually explain why this balance is important. If you only engage with low idea density media, you won't actually learn much or use your brain often. And if you only engage with high density media, you'll miss out on a lot of "softer" value, like pop culture, much of the humanities, and socialization[^important] too.

[^important]: Which I now think is really important, maybe even more important than knowing math.

With that said, here are some ways to increase your propensity towards high idea density (and crucially not just a list of high idea density things to expose yourself towards):

- Make smart friends who you can talk about interesting stuff with, and ideally, work with. This won't make you do more idea-dense stuff, but it will increase your threshold for what idea-dense means.
- Learn more math. This is the most real, easily-understandable, and information dense subject. At the same time, it is convenient. You do not need to do any hands-on work or buy equipment or whatever: the activation energy is low. And, most importantly, when you can understand the world through the lens of math, it will be easier to observe and process higher density information. (For instance, actually knowing calculus makes statistics much clearer.)
- Read general non-fiction (i.e. not textbooks), specifically history/psychology books.
- Improve the hygiene of your low-density activities (this deserves its own section)

There's no list for low density, because there's only one thing you need to do: talk to people more often, **especially those who'll drag you into quality, low-idea density experiences**. (More on quality later.) Walk under bridges at 12 AM, watch Hamilton, lift weights together. Do stupid shit together and have fun.

### Addendum: meditation

Meditation is interesting because you're trying to clear your head, which means that the idea density should approach 0. I haven't tried it yet, but thinking of meditation as a tool to increase the range of idea densities you're exposed to might just be the push I need.

## Junk food

People shit on sugar because their model of sugar is "high fructose corn syrup" and not "group of macronutrients you need to survive". Similarly, as I alluded to earlier, people shit on low-idea density media because their model is "TikTok" and not "humanities + actually talking to people". Just as a rich diet consists of eating a ton of fruits and veggies (many of which contain a good amount of sugar), a rich life consists of a lot of high quality low-density activities, like watching movies with friends, going out to eat, and casual conversations (bonus points if you are running together while talking).

It's somewhat unhealthy that studying is viewed as "the right thing to do" while socializing is viewed as "the wrong thing to do", just because the prototypical high idea density thing is good and the prototype for low density is bad.[^categories] (Though these days there's a much better emphasis on balance --- perhaps too much, since people are really shying away from making high school students *read* these days.)

[^categories]: And these same people aren't consciously aware of idea density. For instance, tiger parents will replace social media with studying rather than getting their kid to do a real sport (i.e. not marching band), which quickly leads to burnout.

So when you're on your phone scrolling through TikTok for the 6th hour, the correct impulse is probably not "I should open the damn textbook", because you will be too pooped to get anything done. Instead, call up a friend and ask if they want to hang out. Chances are, they are *also* scrolling on TikTok and need someone to break them out of it.
