---
title: Math Club
desc: A republished essay on math clubs and good topics for them I wrote in freshman year.
date: 2022-10-04
---

*This is a republishing of an essay on my old website. First published April 25th, 2020.*

Ever since the fall of my freshman year, our high school math club was declining. Even before the coronavirus, the math team didn't do much: there was no one writing the highly anticipated geometry bee and the officers I've spoken to didn't seem to know what the club was planning. Despite the number of strong math students in our school, few people seemed to be interested in the club.

The fate of our math club after the pandemic was predictable. The officers failed to submit the club application on time, marking the end of a club with decades of history.

This hasn't just been happening in my school. A number of non-elite high schools have seen their math clubs getting worse. What's happening here?

## Interests

I'll be the first to admit that I don't really care what happens in my math club. I have the resources, connections, and experience to study on my own.

I think the same holds for most students in the math contest community. A large part of this is because almost all of them are working on their own projects within their own organizations. There's a lot of advantages that working outside your school brings, and they are too good to pass up.[^1]

Most smart people will not choose to host a math club at lunch for outreach. [I know I chose differently.](https://mathadvance.org) But there is still a place for math clubs and those who care enough to improve them.

## A Sense of Urgency

Clubs don't die because people are lazy. They die because people are complacent. They might seem similar on the outside, but their root causes are polar opposites.

Laziness happens when people don't care about bad results. Complacency happens when people care too much about bad results. These sorts of endeavor often end in failure, and it's very hard to stomach the idea of something you put your heart in turning out worthless. But that's why it's so important to entertain the idea. The only way to prevent it is to prepare, and you can't prepare without knowing what sorts of danger your club is in.

It's no coincidence that organizations see the most activity the weeks before an event. It's not just because there's excitement. It's because there's anxiety. Whatever the reason, people start getting worried about the event and checking things. This is why so many changes are made last minute.

Relyin on this is a very risky strategy, if it can even be called that. Near the date of the event things happen faster, but mistakes are often made because it's so rushed. Even worse, these mistakes aren't the type you can prevent or catch with diligence. They only become apparent after time.

So how can you emulate this sense of urgency without waiting until the last minute? The secret is manipulating your sense of time.

People naturally get antsy two weeks before the event and start making small changes rapidly. This is how people get caught up in the atmosphere and start contributing. But the atmosphere isn't caused directly by the impending event. That's what forces your sense of time to shorten. But it's the feeling of things changing that gets other people to adjust their sense of time.

Make small changes fast. As far as I know, this is the most reliable way to bring significant positive changes.

None of this is specific to math clubs.[^2] I've seen other clubs barely scrape together enough candidates for officer positions. I've seen math contests get so caught up with themselves that they forget to check their problems for correctness, let alone quality. These are both caused by an excitement for your product and an anxiety surrounding failure.

The important thing is not to let one overshadow the other. If you're too excited you won't be diligent. If you're too anxious you won't get anything done. The best balance is maintained by frequent and granular changes.

I've been talking about complacency, but it turns out small changes are also the antidote to antipathy. At the very least, if you find yourself struggling to get small changes made, you know that the club is going to fail and don't need to waste more energy on it.

## Mass Appeal

It's very noble to believe that "math club welcomes everyone." But this sort of idealism might end up doing more harm than good.

It's correct to say that math club is a gateway to bigger things. You can't do a lot with a lunch break every week or two.[^3] But that doesn't mean you should make it do nothing or turn it into a social club.

Surprisingly, the best way to encourage long-term growth is to care less about it.

If your club is just a social club, you might attract more people initially. But people want to do meaningful things, and the people who might've been interested at first will leave for another club or stop caring entirely.

Not everyone is interested in math the same way not everyone is interested in psychological thrillers. If someone's asking you for recommendations, you'd give them the best movies you've watched rather than the most popular ones. Math club is the same way. You should teach what appealed to you when you learned it, rather than what you think will appeal to most people, because you will be right about the former and wrong on the latter.

Going for mass appeal will lose the interest of casual members and the respect of enthusiasts. The people who showed up are largely there because they want to get serious about math, and they can tell if you're not giving them that. 

## Specific Suggestions

The goal of math club is different than more intensive math classes, whether contest-based or higher math. You should be aiming to have everyone walk away with a good understanding of everything you've presented. This means you should be conservative with the problems you present, and you should usually be conservative with the complexity of a lecture.[^4] I believe most good lecture topics will be combinatorial because it's the most accessible topic for those without prior knowledge.

Some math classes adopt the mentality of "throw stuff at the wall and hope some of it will stick." I have mixed opinions of its effectiveness in general, and I know math club is not the place for it. You do not want to actively scare people away. While you do want to challenge people, you don't want to frustrate them. [ARML's philosophy of gimmes versus gettables also is very helpful.](https://blog.evanchen.cc/2018/02/02/an-apology-for-hmmt-2016/) To be explicit, I believe the vast majority of math club lectures should only consist of gettables.

What constitutes a good topic depends on how long your meetings are. Unfortunately, you can't do much if your meetings are less than an hour. That being said, here are a couple of topics that should be new to most people, which will level the playing field a bit.

- Group theory. Although it's not an easy subject for high-schoolers by any means, it's useful and surprisingly intuitive.
- Infinity. Like Hilbert's Hotel, countable infinity vs. uncountable infinity, bijections, ordinals, cardinals... there's a lot to go over.
- USAMTS puzzles. I would not rely on this too much, but it's good for filling in some meetings after intense events. (Think AMCs.)
- Mock contests and discussion. You can either mock old tests or AoPS contests --- [here are a few mock contests I like](/math/mocks), which is only a subset of the abundance of treasure in the AoPS Mock Contests subforum.
- Transformations in geometry. Did you know that the area of the triangle with the lengths of the medians of $\triangle ABC$ has $\frac{3}{4}$ the area of $\triangle ABC$? Unless you're already very experienced in math competitions, this obscure topic is probably not one you're very familiar with, but it is still approachable for those with less experience.[^transformations]
[^transformations]: When I say transformations, I am referring to a particular class of problems that appears in math contests. If you're interested in further details or obtaining the Transformations MAST Handout --- a student favorite --- email me.
- Freedom. Sometimes, whether in Perspectives or States style counting questions, you will be given a framework with a fairly complex set of possibilities that you can simplify by disregarding what you don't care about. Examples include gems like MAST Diagnostic 2021/8, a problem from my MATHCOUNTS tryouts, HMMT 2003/C10, and NARML 8.

    You could explain this idea to a sixth grader and have them understand it, but problems like Andy the Unicorn (the colloquial name for MAST 2021/8) have a low enough solve-rate to indicate these problems are genuinely difficult. I think this is one of the best things you could do near the start of the year. It really showcases the magic of mathematics: it's not just about formulas, grinding, and manipulation. Incredible shifts in perspective can be made too. It is no exaggeration to say this could change people's views of math entirely.[^5]

- Remainder Theorem. This is something I think the underclassmen in Algebra 2 will appreciate knowing, and besides is a very cool topic with plenty of applications. Best of all, it builds on something that nearly everyone knows how to do: polynomial division.
- Factoring. The basics matter, and it's also quite satisfying to do.
- Telescoping. This is relevant to the upperclassmen in Calculus (think partial fraction decomposition), and is also just cool and satisfying.
- USA TST Cars. (Sorry not sorry.) I know that nobody will solve this, but when you take the last 5 minutes of the meeting to explain it, everyone will be simultaneously amazed and annoyed by the solution.

You will notice that there is no geometry or number theory in this list. I think that Geometry is too detail oriented for math club, while Number Theory requires a significant amount of time to set up. In general, lectures should be as discrete as possible for the purposes of flexibility.

I think that assigning reading and homework should be done, but you should also stress that it's optional and there's no shame in not being able to find the time to brush up on the prerequisites. The main reason math clubs exist is because it's difficult for people to start learning on their own. That's why giving them something to take home and finish on their own time is so important.

[^1]: The two main factors I have in mind are school politics and geographic constraints. School politics are annoying because you have to apply to leadership each year to get approval and there are school policies you have to follow. Perhaps the worst is that officers are decided through election, which is just a popularity contest.

	Geography is the more impactful factor. Just by random chance, the best and most motivated problem writers or handout writers are going to be scattered across the country. Only being able to work with people in your school is very restrictive. The coronavirus sort of forced the nation's most motivated students together. They won't want to return to "normal" after this.

[^2]: I actually developed this philosophy by creating math contests. Math contests are something that requires a lot of granular adjustments, which is why this sense of urgency is so important.

[^3]: If you can get away with it, try to avoid telling people that math club doesn't really mean much. You shouldn't pretend math club is sufficient. But that doesn't mean you have to make a point of saying the opposite.

	People will get interested in math at their own pace. You probably don't need to tell them "if you want to get better at math, do more of it" --- that's obvious.

[^4]: I say "usually" because there are times you should present difficult lectures. Especially if you're introducing a subject that most people have never heard of, such as real analysis or group theory, some people will be interested and fill the gaps in on their own. This experience, more than the contents of any particular lecture, is the most valuable takeaway that you can get from math club.

[^5]: I really like this topic in particular since the Andy problem was inspired by my experience with the MS MATHCOUNTS Tryouts problem and a penchant for misreading (I misread CMIMC Team 2019/7 and found an idea I deemed interesting), and NARML #8 was a problem originally too similar to Andy the Unicorn that got saved by a random suggestion (among a list of terrible suggestions) that I make the scientist loop through the number line, which somehow was enough to make something interesting and novel.
