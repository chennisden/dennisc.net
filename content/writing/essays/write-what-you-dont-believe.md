---
title: Write what you don't believe
date: 2022-10-22
---

The goal in my essays isn't writing what I believe --- it's writing what's true.

It seems counterintuitive, but writing about things you don't feel passionately about can actually make you better at writing --- and it might turn out to be better writing to boot. It's the difference between acknowledging something as true and ardently believing that it must be. With the former, you are just finding the dots and connecting them, and with the latter, you can get easily blindsided because you are putting *yourself* out there.

Not every essay on something you're passionate about will turn out bad. But not every good essay has to be on something you're passionate about. The reflective side is just as powerful as the passionate, and it's okay to sometimes be a little disconnected from your words. That's the power of writing: you can say things you never even would've thought about otherwise.
