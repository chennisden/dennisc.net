---
title: "On advice: direction and magnitude"
date: 2023-09-24
---

When giving advice, it should generally be aimed at the direction of people's efforts, not at the magnitude.

What do I mean by direction and magnitude? Perhaps a couple of examples would best illustrate the distinction. "The algebra 1 professor this semester does not teach well, take algebra next semester and analysis 1 this semester instead" is *directional advice*. It substitutes one thing with another thing of a similar difficulty. On the other hand, if you see someone working 60 hours a week, "take some breaks and stop working so hard" is *advice on magnitude*. It removes (or possibly adds) difficulty.

These components are not precisely orthogonal. But the overlap is small enough that this is still a useful distinction to make. And both forms of advice can be useful! "Stop working 60 hours a week" is still good advice, full stop.

But by and large --- especially for people who already have things figured out --- advice on magnitude is at best useless. Unless you have known someone for very long, chances are they know their limits much better than you do.

So when someone is trying to do something ambitious, the least helpful thing you could do is say "don't try to be ambitious". *They're not going to listen*. They aren't looking for advice on magnitude. They are looking for advice on direction.

It also works the other way: if someone is spending 50 hours a week and still struggling, the least helpful thing you could say is "well just try harder". What do you mean, *try harder*? They're already going to every lecture, every office hours, and making extensive use of resources like Piazza. There is no possible way they could try harder.[^exception] This is why many of the people being generally unhelpful to the people who are ahead also cannot really give good advice to the people who are struggling.

[^exception]: There's only one kind of exception I can think of: people who excel spend their downtime (e.g. time waiting for the bus) reading, say, articles about Haskell, whereas people who struggle do not. But that kind of effort, I argue, must accompany a change in direction.

However, where the correct[^loaded] *magnitude* of someone's efforts is largely determined by internal factors, the correct direction of someone's effort is more affected by external factors. For instance, the CMU math department does not have a lot of opportunities for algebra. So if you want to study math at CMU, it makes more sense to direct your energy towards another field or cross-register at Pitt. Either way, *something* in the direction of your efforts should change because of this external factor.

[^loaded]: "Correct" is a very loaded term here that could mean a bunch of different things. You don't need to think too deeply about what it means.

Whereas the internal factors affecting someone are best understood by themselves, the external factors are a different story. Say you are an upperclassman giving some freshman advice on what courses to register for. You don't know nearly as well as they do what they can handle, but you do know much better what the courses are about. Closing this knowledge gap is probably the most helpful thing a piece of advice can do, and I think it should be the explicit *aim* of said advice.

Of course, this isn't to say "course X is generally regarded to be hard" is bad advice to give. But maybe your gut reaction to "I want to take course X" anyway shouldn't be to issue a blanket statement discouraging them. Just my two cents.
