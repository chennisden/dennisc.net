---
title: Competence
date: 2023-04-11
---

The median score for the math section of the SAT is 550.

You and I, as people who are good at math, both know the SAT is incredibly easy. All you have to do is not suck at middle school math. Given that, it's kind of impressive that the average person fails a quarter of this test.

---

By the way, the median mile time for a high school guy is around 9:00.[^made] Which is kind of impressive, considering that you just have to not go super slow on lap 1 and survive for the next 3 to get under 6:00.[^arbitrary]

[^made]: I made this number up, I can't find what it actually is. But from anecdotal experience, this time is about right, and all that really matters is that this time is really slow.

[^arbitrary]: 6:00 is an arbitrary time, people who are better than me could easily claim the same for a 5:30 mile, and so on.

## Both statements were correct

If you don't run, you might be thinking, "well most people can't run under 6:00 so clearly it must be hard." You might even take it one step further and conclude, "hey, getting an 800 on SAT math should be hard too, since most people can't get it." **But look at the damn test.**

The alternative explanation for these scores is that people *really* suck at math. And since the SAT just is not hard, I'm inclined to believe it.

But there's nothing special about math that makes people suck at it. I've already given another example of something most people suck at: running. And if you can do math and running, can you

- cook?
- use Linux? (For extra credit, any non-Ubuntu/Manjaro/etc distro.)
- program in a language that isn't Javascript?
- write a crossword?
- do a handstand?

If you talk to someone who can do any of these things, they will tell you that they started out believing it was hard, but once they committed to actually doing it well, they realized it was easy. But you, who cannot do it, will believe it's hard.

---

When I was in elementary school, I would loudly complain that math class was really boring and too easy. Meanwhile, half of the kids would be in class going "Huh? What is a fraction?"

Then some angry adult would tell me, "no, the math we're doing here is *hard*, can't you see that half the class doesn't get this?" And I was really tempted to say, "well, maybe they all just suck at math", but by then I'd learned better than to say these things.

But it turns out 10-year old me was right. Nobody was ever willing to consider that most people just sucked at math --- everyone just had to be at least competent at everything.[^fool] And so their measure of hard was based on failure rates, because if a bunch of competent people do something and most of them fail, then it *is* hard.

[^fool]: I'm sure that elementary school would tell people that everyone was *good* at everything if they could, but not even 10-year olds are that gullible. So they settle for the next best thing: no one's allowed to point out that most people suck at some given subject.

It's true that difficulty is a factor in failure rates. But so is competence. And if people are incompetent, then something doesn't need to be so hard for it to have a high failure rate.

## Doing just fine

Yeah, everyone sucks at almost everything. But the people who can't do algebra or run a mile under 5:00 or shoot 3-pointers consistently are doing just fine too. There are a lot of things that we can't do that would make us better people. So many, in fact, that it's hopeless to be competent at *everything*. And you know what? There's nothing wrong with that.

But it's important to be consciously aware, "hey, in the grand scheme of things, I basically don't know anything and can't do much," so you don't keel over and give up after realizing you suck at most things. Not being able to learn everything is no excuse to learn nothing. That's why people work on self-improvement their whole lives: it's because there's just that much to improve.

Putting in an effort to get genuinely good at a few of the thousands of things you currently suck at might only decrease the percent of things you suck at by 0.1%. But I think it makes sense to look at skills from a [success-oriented framework](mediocrity). Because going from being good at 4 things to 5 things is a genuine accomplishment, and these days, anyone looking for talented people look at what you can do and not what you can't.

So even if you failed every academic subject in school as well as phys ed, that's still okay. You should expect to be a failure at most things. But when you find the few things you have an aptitude and inclination towards, treasure it. Get good at it rather than just aiming to suck less than everyone else. It might be hard, but it probably isn't *impossible* --- it's just that most people haven't tried hard enough, given how many other things there are to try. So be ambitious. Be one of the first to do whatever it is you want to do, and **do it really well**.
