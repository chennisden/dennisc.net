---
title: Puzzle Construction
date: 2023-07-25
desc: Tips on constructing your very own logic puzzles, plus some anecdotes.
---

These are my thoughts on constructing logic puzzles. Some of this is advice tailored towards beginners, other thoughts are personal musings.

## What is a logic puzzle?

When I say "logic puzzle" it could mean many things to many different people. Some people will take it to be riddles or paradoxes ("when does a heap of sand stop heaping?") while others will think of the 100 people with green eyes. However, what I am referring to are **pencil puzzles**: things like sudoku, kakuro, etc. Basically, you have a set of very concrete rules that are hard to misunderstand. You don't have to rely on "vibes" or "convention" to know your answer is correct, unlike in a crossword or puzzle hunt.

The appeal of logic puzzles is twofold:

- You have to use concrete, mechanical rules to make air-tight logical observations. It's kind of like doing math, you are given a set of initial rules and an initial setup and have to figure out all the implications of the two. Figuring out the rules is not the hard part, but you have to *figure out the rules* (as in what they're ACTUALLY doing) :)
- It crosses cultural and linguistic barriers. I don't know British English so I'd be hard-pressed to solve a British cryptic crossword. (Not that I can solve American ones, but that's besides the point.) Logic puzzles aren't like that: if you understand the rules, you can rederive the logical principles used to solve it. (The more theory-intensive the puzzle/genre is though, the harder this becomes in practice. More on theory later.)

(An exception is instructionless. Those are a very fun subversion of the whole premise of logic puzzles.) 

## How to start

Obviously you have to solve puzzles to set puzzles, just as you have to read books to write books. But you really don't *know* how important solving other people's puzzles is until you do it yourself. I want to underscore just how vital it is.

Most people who "solve puzzles" go to sudoku.com or NYT and do the "Hard" sudoku. These sites have low-quality, computer generated sudoku. Normally I wouldn't pass judgment on that sort of thing: whatever floats your boat, I guess. But setting puzzles is a form of *art*, so if you want to set puzzles you must have demanding standards. (Most people serious about setting know this, but I'm including this for the people who casually think, "Oh puzzles are cool it sounds like a thing I want to create". Disclaimer: that used to be me.)

So where do you find good puzzles? *Cracking the Cryptic* and *Logic Masters Germany* are good places to start; in fact, I nearly exclusively solve puzzles from those two sites. *puzz.link* is also quite popular, but a lot of genres there are more theory-based: Heyawake, Slitherlink, and even some *star battles* are quite theory intensive.

Every puzzle I've set would not have been possible if I'd stopped doing puzzles. For many of them, I can name the exact variant/genre or even **the exact puzzle** that, if I didn't do, my puzzle would definitely not exist:

- Pointer Fillomino: KNT's "Happy Birthday, Gabi!"

    When I saw Japanese Sums being combined with pentopia and rotation, my mind was immediately drawn to the connection between number placement (the Japanese Sums part) and pointer-flavored geometry (how far the nearest of some structure was, in this case, pentopia). The weird rotation mechanic really sealed the deal: it got me thinking the number in an arrow should mean *something*, and I naturally gravitated towards distance.

    Then I had a pentopia fillomino ruleset written up, and I realized, "Hey, arrows can also form region borders if we make them self referencing." That was the ruleset done: I just had to think of a break-in (and settled on something of the structure, "adjacent arrows in the same direction can't be consecutive in one direction") and it was off to the races.
- Alphabet Soup: rubenscube's "The Aquarium"
    
    The idea here in The Aquarium is a ton of swordfishes. However, I thought of it as "there are exactly three 1's in the top row of their box" (you can substitute 1 for any digit and "top row" for any row/column of a box), which was a more global perspective. With that I thought, "what if I take this deduction to its logical extreme: you have to use this fact to place digits in each given box, but you don't actually know how the boxes align until later". Only later did I realize that my setup was insane enough *to take 1 and 2 in consideration at the exact same time*. In hindsight, I'm glad I didn't know what a swordfish was and had the balls to set a really ambitious puzzle back when I was starting out. (I'd definitely be too lazy to do the trial and error until I stumbled on something that just magically *worked* so well now.)

This is just a small selection of my puzzles that I can definitely say were inspired by something in particular. Some puzzles have less specific origin stories, and others I have forgotten. If you solve more puzzles maybe one of them, or some combination of puzzles you have solved, will inspire something specific. Maybe solving puzzles will just generally make you better at spotting things and help you testsolve your own a wee bit faster. I dunno, just solve more puzzles. (But make sure they're actually teaching you something.)

By solving more puzzles, you increase the range of ideas you're floating in your head. The primary difficulty of puzzle-setting isn't the part where you put clues to fill in the grid; that part's only hard when you do a dumb and make major oversights that subtly (or not so subtly) break your puzzle. Rather, the hardest part of setting a puzzle is *deciding what rules to use*, and then *convincing yourself that your ruleset is tractable* (by creating an interesting breakin). The first part is less applicable for vanilla puzzles, e.g. German Whispers, but then you have to make the interactions logically consistent. If you solve more puzzles, you will have more ideas. If you have more ideas, more of them will be accidentally good.

## Don't set a puzzle to set a puzzle

Once upon a time, I wrote a mock USAMTs. For those who don't know, USAMTs is a math contest that also contains puzzles. The puzzle part is all that matters for this story.

I decided "OK now I need to write a puzzle for this contest." I had no idea what I wanted the puzzle to be about. I didn't have some sort of logical idea I wanted to share with the world, my thought process was just "OK and now we need a puzzle because -external reasons-". Predictably, it ended up terribly.

**Your motivation should not be setting a puzzle just because you want a puzzle**. Of course, there is an intrinsic joy to setting puzzles for its own sake, and "I want to set a puzzle" works well as a *global* motivation (i.e. it's what keeps you going for the long run, not what gets you through Puzzle X). But you can't just sit down and say "now I will magically write a puzzle" any more than you can sit down and say "now I will magically write a book". You have to have an idea you've been mulling over, that would be interesting if it worked out. What should be going on in your head when setting a puzzle is, "Gee I wonder if -this interaction- could happen in some interesting non-broken form" *and then you work to make it happen*, not just "Oh setting puzzles sounds cool and now I will throw shit at the wall until I accidentally get something unique".

My point is, each puzzle should actually offer something unique. For example, X-Lines Sudoku was born out of me wondering if I could combine sets with arithmetical logic. When you set a puzzle, you should actually be able to point out, "and these ideas are why I decided *this puzzle was worth setting*", even if it was born out of external motivations. (External motivations, e.g. presents, are fine! They fall in the category of "Stupid Inspirations" that shouldn't make sense but somehow do after some mental gymnastics.)

## Where to set?

Some people start out by setting on pencil and paper. Of them, many are just unaware there are online tools.

The major two are [Penpa](https://swargoop.github.io/penpa-edit) and [F-Puzzles](https://f-puzzles.com). Also puzz.link exists if you want to use that.

### Penpa

Penpa is the go-to option for non-sudoku pencil puzzles, e.g. Fillomino, Star Battle, Kakuro, etc. It doesn't actually constrain your inputs based on your puzzle type, which can be a little daunting. It's like the Swiss Army Knife of setting, it can do pretty much everything 2D (including non-square grids). However, it doesn't provide any techniques for logically checking that your puzzle a) has *any* solutions and b) has no more than one solution, which makes sense because you're just putting *stuff* on the grid. Penpa doesn't know what any of that stuff means.

You may want first class support for variant clues, like German Whispers or XV. None of those exist. Hell, even finding how to make a Killer Cage is pretty hard. The general idea is, if you want to put a clue down, it's going to be some cosmetic decoration. There is no strict 1:1 mapping between decorations and clues, and you will have to find many clever workarounds.

### F-Puzzles

**I recommend F-Puzzles for beginners who are setting sudoku.**

F-Puzzles, by contrast, is much more limited but also much friendlier. Though it doesn't include some variants, you can install [Renban and Whispers](https://gist.githubusercontent.com/dclamage/3d76a56c9153a8546888a31d65765cb7/raw/fpuzzles-newconstraints.js) as a Tampermonkey extension. (By a few months time, this will likely be native in F-Puzzles.)

The boon of F-Puzzles is that it points out logical errors. If you accidentally put two 1's in the same row, it will highlight that for you. Or if your thermo has no possibilities. F-Puzzles provides some mechanical things you can do to make your setting life easier. It doesn't require brain cells, creativity, or good taste: it just requires a good puzzle setting setup. (On the flip side, some setters believe using F-Puzzles' computer assistance to set makes your puzzles worse. I don't really subscribe to this theory, provided you aren't just blindly putting clues down and "check solution"ing your puzzles.)

Additionally, you can avoid wasting your time because of logical gaffes or super-subtle interactions using Rangks's Sudoku Solver. To simplify, it allows you to check whether your puzzle has any solutions, and can also do scanning for you (so if you missed a naked single it will point it out for you). Follow the instructions to install [Rangsk's Sudoku Solver](https://github.com/dclamage/SudokuSolver/wiki/fpuzzles-integration#additional-scripts) and integrate it with f-puzzles.com.

### CTC Discord

The Cracking the Cryptic Discord is also super helpful. While it doesn't *directly* provide any software, it can lead you to more helpful F-Puzzles tools and teach you the ropes of Penpa/F-Puzzles. You can also ask people to test your puzzle to ensure a) it's correct and b) all the deductions can be reasonably carried out by a human.

## Stupid Inspirations

I've talked about how solving puzzles can provide you with ideas for your own puzzles. However, in some sense the puzzles you solve only provide the initial spark for your puzzles, and you have to engage in some serious mental acrobatics to get to the end result. For example, Pointer Fillomino and Happy Birthday, Gabi! are two completely different puzzles. It's like I boarded a train at the HBG station and after riding 14 stations and switching lines multiple times, I finally got to Station Pointer Fillomino.

The point is, it doesn't really matter what your starting point is, all that matters is you get a tractable puzzle idea in the end. Hell, even [song lyrics](/writing/blog/creative-methodical) can get you there:

> For instance, take my puzzle Monster Without A Name. Normally, you’d think that I wrote this puzzle and found a good name for it afterwards. And that is true for most of my puzzles. But for this one, I came up with the title before I had any clue what the puzzle would be about, because I really wanted to force the song lyric.
> 
> So I first was thinking of just making a really hard puzzle. Problem is, you can’t just decide to make a really hard puzzle, and I did make a couple of semi-hard puzzles but they all had obviously better titles, e.g. X-Lines Sudoku. So I thought, “maybe the constraint should actually have Monster in the name, at least”.
> 
> When I thought of Monsters, I thought of the similar word killers, since you have Killer Cages and Little Killers. And since Killers do stuff with sums, Monsters should do the next simplest thing: products.
> 
> Then the question is, how do I do products? Just doing the product of every cell is really boring, since it removes a lot of nuance and degrees of freedom. So somehow, I would want to do region sums.
> 
> Now I had to decide how to construct my regions. First I tried using shading pencil puzzles like Yin-Yang or Caves to create a hybrid — and I still think that approach has a lot of viability — but after doing a few Chaos Construction puzzles, I realized: the most natural way to do products of region sums, which requires region construction... is to do the variant that uses region construction. The Monster Cages constraint had finally crystallized.

Any sane human would react to that story with "how the heck did you get from point A to point B. It makes zero sense." But here's the thing: you need to be creative, so creative that it crosses the line and actively becomes stupid, if you want to do something *interesting*. Sure, you could do something straightforward. But that's obvious, and obvious is boring, because it wouldn't be obvious if it wasn't boring. If you think of weird things no one else would think of, and your train of thought jumps in ways no one else's would, then you will end up with puzzles no one else would set.

Of course, that isn't to say you should exclusively wait for a really stupid idea to hit you in the head and magically work out. Non-stupid inspirations exist. But stupid inspirations exist too, and they're worth grabbing onto as well.
