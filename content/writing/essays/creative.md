---
title: Creative processes make me optimistic
date: 2023-01-25
---

Say Alice can solve 100% of calculus problems and Bob can solve 80% of them. If Alice and Bob are taking a group test[^gt] together in math class, the existence of Alice totally removes any value that Bob brings to the table. Because if Alice can just do what Bob does and more, what's the point of Bob doing anything anyway?

[^gt]: Yes, these really exist.

Fortunately, this is not how creating things works. Let's say that Alice writes crosswords and sudoku, while Bob only writes sudoku. Even if we were to argue that Alice is a better puzzle constructor than Bob is, because Alice can do strictly more things, that doesn't mean Alice's sudoku puzzles are better than Bob's. Or even that their sudokus will cover the same ideas, mind you.

In the group quiz example, Bob is practically useless because Alice *will* do everything that Bob could've. *Will*, not *can*. The difference is that, in the real world, a great puzzle setter certainly *could* construct something as good as your puzzle --- but they didn't. Because there are so many things to create (such as puzzles) and discover (such as math theorems), it is actually very unlikely for someone else to come up with and execute on your idea, provided it is sufficiently novel.

So yeah, there are probably people who are better than you at making whatever it is you make. But the upshot is even if there are people better than you, you can still provide value that they don't. Because if both you and Alice can contribute something, then chances are, you won't care so much which one of you two is better at it.
