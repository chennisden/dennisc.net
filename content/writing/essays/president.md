---
title: What does the president do?
date: 2022-08-27
---

A non-profit has a minimum of three officers: a president, a secretary, and a treasurer. It's clear what the secretary and treasurer do. The secretary is in charge of rote administrative stuff, such as emails and maintaining notes, and the treasurer is in charge of finances. So what does the president do?

On the surface the president is in charge of making decisions, doing PR, and providing a vision, among other things. But really the president's job is to maintain standards.

The way [we](https://mathadvance.org) have things set up, I'm the president for life and I have the power to make any of my decisions final. This means that anything we do has my approval, tacit or explicit. But there's a lot of things that come up when you're running a nonprofit, so ideally people will make decisions on their own as often as possible. Plus, when it comes to the hard questions, my perspective can be insufficient or straight up incorrect. So having other people with the same values help make decisions is very helpful.

This means that the type of person you should put in charge is the type that thinks and cares about these things. Evidently I do: besides this essay, I've written [an essay on frameworks](framework) and [Math Advance's strict and detailed contribution guidelines](https://mathadvance.org/contribute). The president also has to be willing to wrestle with the jobs no one else can or wants to do well. In my two years as president I've already picked up programming, system administration, legal responsibilities, public relations, writing, and design. The upshot is that these responsibilities give you the push to learn a lot of skills, and having high standards ensures that you learn and do them well.[^cut]

[^cut]: Doing all of this while maintaining high standards is hard, which is why I recommend limiting the scope of your organization and cutting when you need to. (This is why we put [MAST on hiatus](/writing/blog/mast-hiatus).)

A bit of history: Math Advance started forming last May[^mac] when Jerry asked for help on a mock AMC 10 (which would become the [2020 JMC 10](https://mat.mathadvance.org/JMC10.pdf)) on the AoPS fora[^aops] and a couple of people who would become the initial founders agreed to help. On the surface I didn't do a whole lot[^2nd]: I wrote two easy problems (which pretty much anyone can do for a mock AMC 10) and two hard problems. The vast majority of the problems were written by Jerry with problems not written by him sprinkled in by a variety of other contributors.

[^mac]: Though we didn't know it would become what it is now until about August.

[^aops]: In hindsight this is an incredibly bad decision. If you need help with a creative process (which I consider math contests to be), you should be asking people whose judgment you have reason to trust, not just randoms. This is doubly true in math contests where most people have bad taste and low standards.

    For my part I ended up joining partially because I saw Jerry's posts about anime. I'm not kidding.

[^2nd]: I'm being a little unfair to myself: I was the second most prolific author in that test and the solutions manual used my document design (which was awful back then, by the way).

Looking from the outside at this point in time, it would've made the most sense to put Jerry in charge. So how did it end up being me?[^mast] It was because I was willing to call the shots no one else did[^matter], I had relatively[^relatively] high standards to the point where I was called "quality control", and because I was willing to do the annoying stuff like typesetting solutions. When we had to make a formal decision while filing our corporate registration it ended up just being me by default. The president doesn't have to to be the most capable or hardworking out of everyone, though most good presidents have both these qualities. Rather, their job is to make it as [easy as possible for capable and hardworking people to contribute and create something of value](force-multipliers).

[^mast]: I was the director of MAST from day 1, although it came after MAC (the now internal name we use for our contest team). MAST and MAC ended up merging since the overlap between the two teams just kept growing, so it made some sense I was in charge. But even before that I was effectively (maybe even officially?) in charge of contest-side.

[^matter]: Which sounds really cool until you realize that no one was willing to make these decisions because they didn't really matter. Stuff like naming things (ironically I didn't come up with the name "JMC", although I did for everything else) and setting dates.

[^relatively]: I say "relatively" because if any of my colleagues' standards now were as low as mine in freshman year I would have made fun of them. Back then I said really stupid things like "I think formatting doesn't particularly matter, anyway."

Outside of setting standards I'm just another member of Math Advance. My standards apply equally to me --- sometimes the problems I write aren't up to par, and more often they are up to par and just can't fit well in a certain draft --- if they didn't, no one else would take them seriously.

Being the president requires a lot of restraint. Whenever I'm on the "losing" side of a disagreement it's easy to just say "forget you, I'm going to do things my way because I'm in charge". This behavior isn't dangerous because I'm taking advantage of my position, which I already am by using the company server to host my own projects, including this website. Nor is is that I'm exerting disproportionate influence, because I already am --- besides the JMCs, I'm one of the leading proposers in every contest we've released so far, and I'm not even close to the most prolific proposer in Math Advance.[^mapm] It's dangerous because it erodes trust in standards. If it's okay for the person creating the standards to circumvent them, why should anyone trust that they'll continue to be enforced?

[^mapm]: The primary reason for this, at least recently, is because we build contests using [mapm](https://mapm.mathadvance.org), meaning that every final draft resides in our mapm Dropbox. If I'm pretty the only person using mapm, obviously I'm going to be the one in charge of all the final drafts...

Sticking to your standards can lead to dissatisfaction from time to time. Do it anyway: if the people you're working with are mature and patient, they'll understand (and possibly even agree with or propose) decisions that undermine their individual influence or "prestige" in favor of group prestige. One of my colleagues and former students, Aprameya Tripathy, is really good at writing hard olympiad-esque geometry for computational contests, and he writes a lot of them. If you do math contests and haven't heard of him, it's probably my fault: I haven't counted, but I think he's released a single-digit number of problems with us, and the number of problems he's released under every other group combined is even less.

The reason we release so few of his problems is because a good MAT only has room for one really hard geometry question. At this rate, most of his geometry questions will never get used seeing the rate at which he writes new ones.[^geo-contest] In most other organizations this would not go over well. Most people would be tempted to take their ball and go home if it takes a month of debate just for one of your questions to get on the contest. Even if you're patient and willing to wait for the payoff, in this case the wait is on the timescale of years! But it's the standards which MAT is held to that makes it worth the wait if you can appreciate the value of these standards. Ditto for the AMCs and AIME, from what I hear.

[^geo-contest]: We're trying to rectify this though! Be on the lookout for a geometry contest from us.

Speaking of Aprameya, his problem statements are largely polished from the get-go now. But this wasn't always the case. His problem statements used to be really messy: points were defined haphazardly, you'd see stuff like "$AB$, $BC$, and $AC$" instead of "$AB$, $BC$, and $CA$", and the answer extractions were convoluted. The ideas were good, but none of the polish was there.

The easy solution is to fix these issues in the editorial process. But the problem is I'm pretty much the editorial process and I'm also lazy. The root of the problem was he didn't have the right standards for his own problems, and by working with us he absorbed our standards through osmosis.

When I say "he didn't have the right standards" I don't mean that he didn't care about writing good problems. Everyone cares about writing good problems,[^nope] and "standards" doesn't just mean "a general expectation people do things well". Rather, it's a specific set of guidelines and principles I'm talking about: for example, problems have to be neatly presented, so any symmetry inherent to the problem should be made apparent. This is why it's not okay to write $AC$ instead of $CA$ because then you lose cyclic symmetry.

[^nope]: Okay, this is far from true in general, a lot of problem writers feel obliged to continue or are doing it for their ego and could care less. But somehow or another, the people who've wanted to work with us largely do not fit this description.

Fortunately it's easy to communicate your standards as long as you are earnest about them: show yourself following them. In other words, set a good example. Like a team captain reprimanding their teammates during drills, you will occasionally have to hold people accountable and remind them what you guys are all about, albeit not as directly. 

That's all it took. His problems, along with everyone else's, got so much better in a couple of months. Of course it's important to acknowledge that these improvements happened with their efforts, not mine. All I did, whether implicitly or explicitly, was provide them with some direction.

Having high standards doesn't just make your product better. Paradoxically, it makes the production process that much easier as well: if doing things right starts coming naturally to you, then you'll do it right the first time and won't have to fix it the second time around. 

You should have high standards for the people you work with. Inviting people and giving them official responsibilities is one of the least reversible actions you can take in a nonprofit, so be very careful who you let in. Obviously existing members should always give input when a new members wants to join, and I do think a final vote is a good idea. However, most people just use a simple majority as the threshold to confirm someone, which is a terrible idea.[^veto] If 5 people want a person in and 4 people don't, do you think adding them is really a good idea? When it comes to setting policy, I always think about how it reflects on our standards. The same way we don't want to release shoddy work, we also shouldn't invite people unless pretty much everyone likes them.

[^veto]: The system we use is yes, meh, and no. There's no formal voting system (no votes, even after additional discussion, have ever been close enough to warrant it), but it looks approximately like this: three mehs or one no and the candidate is rejected, and we wait for all the people who frequently give input on recruiting before deciding (which means in practice we need at least 70% yeses).

Some other math organizations I've worked with have been allowed to grow their scope recklessly. People are allowed to do whatever they want without considering how it will reflect back on the parent organization. The result is that the content becomes mediocre and the organization gets a reputation for being unreliable. They become last resorts for whatever they're doing: they might cover more niches than everyone else, but slowly other people will start doing what they do better and the organization will decline.

Of course a good president also has to listen. Part of how I do this is by ceding control and slowly letting other people take charge of more projects, especially as we all get a clearer picture of the way we want to do things. I suspect that many other presidents also do this, which is why to the public eye they may seem like little more than figureheads.

Sometimes my colleagues consider ideas that are clearly bad and others that seem good but upon further inspection are bad.[^fairness] My role as a final arbiter isn't just to block them. It's to create a culture where no one decides to implement them in the first place.

[^fairness]: In all fairness everyone comes up with stupid ideas, and they come up with them less often than I do. Coming up with stupid ideas is a sign that your group is willing to be innovative, implementing them is a sign that your group has low standards.

The secretary's job is to help with logistics. The treasurer's job is to handle finances. My job is to put myself out of one.
