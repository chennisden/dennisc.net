---
title: Does your contest format suck?
date: 2022-06-24
desc: A litmus test for math contest formats.
---

In these past three or four years there has been a proliferation of new student-run math contests. Most contests adopt their own format, which means that each new contest is a new format you have to learn. This alone is reason enough to make your contest format simple, but not only does your format need to be simple enough for contestants to understand, it needs to be simple enough for the organizers to write a good contest.

With that said, here is a litmus test to see if your contest format sucks: can you explain it in no more than twenty words?

- AMC 8: 25 MC in 75 minutes. (5 words)
- AMC 10/12: 25 MC in 75 minutes with 5 options. 6 points for correct, 1.5 for blank, 0 for incorrect (18 words)
- AIME: 15 questions with integer answers 0-999, 3 hours, 1 point for correct 0 points for wrong (16 words)
- USA(J)MO/IMO: 2 days of 3 questions, 4.5 hours each. Proof-based, responses graded from 0 to 7. (15 words)
- MAT: 3 sets of 3 problems with positive integer answers, 30 minutes each, plus a set of tiebreakers broken on time. (20 words)[^tb]
- HMMT Nov: 10 questions 50 minutes for general and theme. 10 questions in 60 minutes for team. 12 sets of 3 for Guts, last set is estimation. (25 words)[^convoluted]
- HMMT Feb: 10 questions 50 minutes for 3 subject rounds. 60 minutes for team, proof-based. 12 sets of 4 for Guts, last set is estimation. (23 words)

[^tb]: In an ideal world MAT wouldn't even have tiebreakers. And in fact, for PMAT --- the beginner version of the MAT --- I'm inclined to get rid of tiebreakers entirely, which would reduce PMAT to 11 words and make it the simplest format out of all the contests listed here besides the AMC 8, which is ideal for an introductory contest.
[^convoluted]: Yeah, I think HMMT is a bit convoluted. Could you get it down to less than 20? Probably, but the amount of work you have to do is so much that it de facto fails the litmus.

In all of these cases I didn't even have to try particularly hard to get it under 20 (except HMMT). I suspect I could've gotten most of these under 15 if I word-golfed hard enough. If you can't get your format under 20 words, then you should change it before it comes to bite you into the ass in the form of a poorly designed test.
