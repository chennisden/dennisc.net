---
title: Teaching ideas, not just results
date: 2024-06-21
---

There is a proof of the [Bolzano-Weierstrass Theorem](https://personal.math.ubc.ca/~feldman/m320/bolzano.pdf) that roughly goes like this: 

- If we divide an interval in half, then one of these intervals must have an infinite number of points in the sequence. (Else the union of the intervals contain a finite number of points.)
- We can take the interval with infinite points, keep doing this, and converge to the limit of the lower bounds of the intervals.

The natural thing for a professor or textbook to do afterwards is assign exercises that either directly use Bolzano-Weierstrass or look very similar. And to be clear, this is *good*: if we're teaching an idea, we better use it. But I would additionally assign [Konig's Tree Lemma](https://proofwiki.org/wiki/K%C3%B6nig%27s_Tree_Lemma): on the surface it looks fairly different, but the underlying ideas in the proof are quite similar.[^isl]

[^isl]: Another problem that uses the same idea is ISL 2021/C1:

    > Let $S$ be an infinite set of positive integers such that there exist four pairwise distinct $a$, $b$, $c$, $d \in S$ with $\text{gcd}(a, b) \neq \text{gcd}(c, d)$. Prove that there exist thre pairwise distinct $x$, $y$, $z \in S$ such that $\text{gcd}(x, y) = \text{gcd}(y, z) \neq \text{gcd}(z, x)$.

    It is a little more involved than Bolzano or Konig, but worth trying if you want to cement this idea in your head.

When I read the proof of Konig's Tree Lemma, I understood it very quickly because I had seen the proof of Bolzano-Weierstrass. So it stands to reason the converse might be true: learning the proof of Konig helps you better understand the proof of Bolzano-Weierstrass too. Explicitly, we ought to assign a couple more exercises using the *ideas* of theorems rather than their *results*.

There is some subtlety here. For one, we need to avoid being overly broad with what we assign. The idea that "a finite partition of an infinite set must have an infinite part" is generally useful, but it is also known as the Pigeonhole Principle. Konig is a suitable exercise because it recursively uses this method to generate an infinite "path", just as Bolzano-Weierstrass does. It is this high degree of similarity that keeps it from being just another randomly assigned exercise.

This is hard to do well, and I'm skeptical we could afford to do this for *every* theorem we proved. But I think we could stand to do it more often.
