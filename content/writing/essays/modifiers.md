---
title: Modifiers
date: 2021-12-06
---

There is plenty of advice telling you to drop modifiers such as "I think" from your speech and writing. There is virtually no one telling you to do the opposite, which is a shame, since modifiers are quite useful for communicating.

If an honest writer drops the modifiers in their writing, just like their teachers tell them to, they typically also drop its nuances. Any essay consisting only of absolute, unqualified truths is not worth reading. [It's not even worth writing](/writing/essays/hook). That's why every high school English paper looks like it was written by an elementary schooler. It's not because the author is a dunce. It's because they don't allow themselves to say anything a dunce doesn't already know.

Besides the obvious function of modifying a statement, modifiers have another use: pacing. When you say "I think" or "maybe" it weakens the statement. When you omit modifiers and pointedly deliver a line, you emphasize it. But why would you want to express weakness in your argument? Because it helps you communicate clearly. What do you think, and what do you _know_? What's concrete advice, and what's just a potential option?

Any advice telling you to discard a tool that creates variety is almost always wrong. You might not think removing modifiers would change the substance of your writing all that much. But if you want to write better, you have to change the way you write. (Or in this case, you have to stop letting your English teacher change the way you write.) Sometimes a modifier might not seem to convey any additional information. But modifiers aren't just a matter of practicality, they're a matter of style.

And I think it makes for damn good style.
