---
title: Fast vs. short
date: 2022-08-30
---

The goal of writing is to communicate ideas efficiently, both to others and to yourself. A lot of people think that efficient communication means less words, maybe because they've been conditioned to by the word limits plastered in every process. But efficient communication is about having your words be read and understood as fast as possible, not word golf.
