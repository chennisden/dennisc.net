---
title: Notes
date: 2022-08-25
---

In high school many of your teachers will force you to takes notes.[^cornell] They seem like ridiculous wastes of time, and the notes you take in high school typically are. But if done right notes can be very valuable.

[^cornell]: Extra credit if they make you take Cornell notes.

Obviously the most effective way of taking notes will differ from person to person. This is just the way I think about mine.

## Value

The whole point of notes is that they become the central authority for you on a certain topic. It's not supposed to be the Spark Notes of a certain topic. Otherwise, why not just skim or read the textbook again? It might work for a test[^not], but you won't gain much else by summarizing.

[^not]: I don't even think it works well enough to justify taking "summary" notes anyway.

Making your notes on a subject better than any existing material seems like a tall order. And a tall order it is indeed. But it's not impossible!

Your notes just need to be the best for you. Sometimes no good reference material exists: consider the lack of an introductory geometry textbook, or up until [EGMO](https://web.evanchen.cc/geombook.html), an olympiad geometry textbook. Other topics are more complex and not as linear: Linear Algebra Done Right and Linear Algebra Done Wrong present topics in different orders and choose to emphasize different connections.

Some texts are just unparalleled and can't be feasibly improved on.[^few] In these circumstances, I don't think there's much of a point to taking notes. But most topics are either complex enough that different textbooks fill different niches, or otherwise textbooks don't exist/are terrible. Sometimes both. For neither of these statements to be true is very rare.

[^few]: However, this class of textbooks is so small I couldn't think of any examples to name here.

## Structure

- You want to have semantic separation of content line up with visual separation of content.
- You can't always arrange information linearly, or sometimes it just doesn't make sense to. An example from my upcoming geometry textbook: when it comes to angle chasing and similar triangles, sometimes there are questions that use $\frac{bh}{2}$ even though it makes sense for area techniques to be introduced later. Just try to be explicit about when information isn't or can't be presented linearly, especially when two sections both reference each other.[^textbook] A broader example in math is Linear Algebra and Multivariable Calculus --- each subject relies on knowledge from the other to some extent.

[^textbook]: This happens mostly in larger projects like textbooks. This also might be an explanation for content gaps: a certain subject might not fit in linearly, so it gets cut off.

	I personally think it's okay to assume students have some outside knowledge. They should be learning multiple things at the same time because *learning isn't linear*. (Which is why you can't always arrange *information* linearly!)

- Follow through. If A implies B then you should put B close after A. For example, most introductory geometry textbooks include Power of a Point as $PA\cdot PB=PC\cdot PD$ but neglect to mention that $PA\cdot PB$ really represents $OP^2-r^2$. If you're going to include the Law of Sines or $\frac{1}{2}ab \sin \theta$, which follow the common theme of "manipulations of lengths and areas in triangles", maybe you shouldn't wait five chapters before mentioning $\frac{abc}{4R}$.

	This general principle helps you "lock the pieces in place", so to speak.[^discover] Since $\frac{abc}{4R}$ should follow soon after the Law of Sines and $\frac{1}{2}ab\sin\theta$, that also means that the Law of Sines and $\frac{1}{2}ab\sin\theta$ should be in close-ish proximity to each other --- like in the same chapter.

[^discover]: Side note: I don't think you're really *inventing* a new framework when you structure your notes well, you're just *discovering* one of the few that make sense.

## Polish

Yours notes should be polished. The really obvious reason is it makes it easier for you to go over them. But that goal shouldn't be to make it easy for yourself, it should be to make your notes presentable to other people.

If your notes are good and make sense for you, they will also make sense for people who think like you. If they can learn the material from your notes for the first time, chances are [it'll be much easier for you to rederive the facts it covers yourself](write-smart). The whole point of taking notes is that you'll forget the specifics, so write them in a way that makes it as easy as possible to get up to speed. Once you've done that, it just happens that you haven't just made your notes useful for yourself --- you've made them useful for other people as well.

The reason you want to make other people your target audience rather than yourself is so you avoid making assumptions about what the audience knows. Right now you know a lot about yourself and how much you know, but over time much of that will change. If you want your notes to not implicitly rely on these things, the first step is to have them not explicitly rely on them.

In the end [my notes become my teaching materials](https://mast.mathadvance.org). Because if you don't use your notes to teach other people, how are you going to check that they make sense for other people?
