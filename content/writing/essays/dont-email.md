---
title: "To parents: stop emailing me"
date: 2022-04-21
desc: Why I encourage students, rather than parents, to email me.
---

I'm really, really hesitant to issue a blanket statement that amounts to "don't contact me." I like to think of myself as a pretty nice person who tries to help people learn math when I can, and I also like to think that most of the time my emails are helpful. But there is one big exception: parents.

Most of the time when parents email me I don't really know how to respond. And it's because I've only had the perspective of a student, which means that any answer I give will be tailored for students. Since students won't be reading emails sent to their parents, it honestly feels like a waste of time.

Let me put it this way. I receive about a dozen emails a month asking for math instruction ("Can I apply for MAST" included). I'd say about 2/3 of it comes from students and the other 1/3 come from parents. So far every student has gotten a helpful answer from me, and as far as I can tell, no parent has gained anything from emailing me.

So if you are a parent, what should you do? I think the answer differs depending on how old your child is and who you're asking, but here are a couple of pointers.

- If you are forcing your kid to do math contests (especially if it is for college), just stop. I have a lot of harsh things that I want to say to these people, but I will leave such comments to the imagination.
- If your kid has just started math contests and might not be as willing to cold contact people, then cc your child in the email. The reason parent emails are a waste of my time is because the kids don't read them.
- If your kid is really young, i.e. too young to send an email themselves, unless they are an AIME qualifier I don't think I'll be of much help anyway. If they are an AIME qualifier, look at [MAST](https://mast.mathadvance.org).
- If your kid is old enough to send an email, then please have them do it themselves. Typically I can glean a lot more information from students than I can from parents.

I wrote this essay not to discourage people from emailing me --- there are certainly many useful emails parents can send me! But I do hope that my future emails become more productive. Every email I've received from parents are of the form "my kid is in Xth grade and wants to prepare for contests A,B,C". Really, the only difference X makes is whether the kid is old enough to send an email on their own, and the contests they want to prepare for invariably are the AMC 10 and maybe AIME or MATHCOUNTS. It's very hard for me to give any guidance, or even to tell whether my classes would be appropriate, with such little information.

So in summary: have students email me if possible, cc them otherwise, make sure I have a direct avenue to communicate with them, and give me specifics. If you write and send your email in a way that makes it easy for me to help, then there is no reason for me not to.
