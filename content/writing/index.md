---
title: Writing
desc: Dennis Chen's writing.
---

You can subscribe to my [RSS feed](/writing/rss.xml) to catch any updates.

<div class="fancy-links"><a href="/writing/all"><h3>All</h3><p>View all my writing at a glance.</p></a><a href="/writing/essays"><h3>Essays</h3><p>Thoughts that are meant to be relevant in the future.</p></a><a href="/writing/tech"><h3>Tech</h3><p>Tutorials and thoughts on technology I've used.</p></a><a href="/writing/blog"><h3>Blog Posts</h3><p>A catch-all for miscellaneous content, typically time-sensitive.</p></a></div>
