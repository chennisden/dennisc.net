---
title: The Best Colors
desc: Some colors with funny hex codes that happen to look good.
date: 2022-06-02
---

For some reason, if you append the strings "42", "69", and "1337" to make hex codes, you will get pretty good colors.

- <span style="color: #426942">#426942</span>: I use this color for my Polybar (X11) and i3blocks (Sway + Wayland, though I hope migrate to swaybar eventually). Out of all the colors listed, this is my favorite.
- <span style="color: #421337">#421337</span>: I love this purple.
- <span style="color: #691337">#691337</span>: I love this magenta even more.
- <span style="color: #133742">#133742</span>: A nice dark turqouise blue. It's not bad, but not my favorite.
- <span style="color: #133769">#133769</span>: A decent blue, but not the best color. I personally prefer richer blues, like <span style="color: #003366">#003366</span> (dark midnight blue).
- <span style="color: #694269">#694269</span>: Might not look great in isolation, but it's similar to [MAT](https://mat.mathadvance.org)'s light purple. Don't hate on it, it could be a good complement for a design!

OK, these were the stupidest color reviews of all time. But these colors are unironically pretty good. I know I'll be trying at least one of these out in my next design.
