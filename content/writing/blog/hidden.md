---
title: Hidden Pages
date: 2022-03-18
---

Some of my favorite personal websites have lots of hidden gems hidden throughout. Not explicitly hidden, mind you, but just pages that don't particularly stand out or call to your attention. These pages, however, tend to be the most interesting.

As two examples, take Evan Chen's Taiwan TST/IMO/USAMO reports and Drew DeVault's Gemini-exclusive blogposts. (There are a couple on his website that conspiciously jump out at you, by virtue of saying "this is gemini exclusive use gemini to access", but some others are not.) If you _know_ how to find them, it will be very obvious how to get there. But if you're new to a site it won't.

I don't think it is by design that these pages are out-of-the-way. Typically it is the miscellaneous, for-fun, trivia, etc pages that are in this position, and not the "main content". And interestingly enough, because these pages are not as important or mainline, they tend to be the most fun to read.

If you want people to stay on your site, have pages that are non-essential for the user and don't get in their way. Think about all the Wordle clones; the reason they got boring quick was because there is no novelty, due to infinitely many words being generated; it's all the same. You can't churn out unique and organic content quick enough so there's something new every day, but just like a fibre does for carbohydrates, hidden pages can make people digest your site for longer.[^1]

[^1]:
    Your pages will be unique by virtue of being unimportant trivia. Because there are only so many things to say about math contests or whatever, you'll probably have seen anything I have to say before. (That's why I don't write on preparing for math contests as often anymore.)

    But it frankly does not matter if you never see [the design for a small-scale filesystem-like Git service](/writing/glee-design), which is why very few people write about it, and the people who write about it don't really push it that hard.

You don't have to try too hard to do this on purpose. Just by virtue of having enough stuff to say, your website will necessarily have stuff pushed to the side. But if your content is interesting enough, could you use that to get people to make changes for the better?

What Drew DeVault did was putting some exclusive content on his Gemini blog instead of his WWW blog. Another thing that I've been doing is jotting down some notes in a small notepad. But because I haven't been transferring my notes over to a more permanent medium (i.e. a blog), I have subconsciously kept my notepad neat by not writing stuff down when I think of it if it doesn't fit into a certain page's structure. Which sort of defeats the purpose of a notepad where I can write anything I want.

So I can put the two together. I'm going to make a Gemini mirror of (some subset) of my website, and on it I will add exclusive status updates. I'll probably put the first one on my WWW website, just to give people a taste of what's on my Gemlog.
