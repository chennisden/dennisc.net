---
title: An Abstract Algebra Primer
date: 2024-10-31
---

I have written an [abstract algebra primer](/alg-primer.pdf). The official pitch on the math section of my website:

> This is a medium-length (113 pages) introduction to abstract algebra. Intuition is emphasized, fully formalizing arguments takes a back seat, the theory is extremely streamlined (so many important and interesting ideas are omitted).

There is not much to say here that I have not already said in the primer. This covers much more than a standard one-semester undergraduate course and is probably closer to the content of a graduate course, but a lot of details and exercises are not included since brevity is one of the main goals. I imagine the level of detail is somewhere between Evan Chen's Napkin and Dummit/Foote.

## Errata

If the date displayed on your PDF is later than the date listed, the error has already been fixed.[^clarity]

[^clarity]: To be clear, the date listed is the last version to contain the error.

**Only errors are listed here.** Other changes, such as additional exercises, streamlined notation, etc. are not included. To see all changes, you may peruse the [Git log](https://gitlab.com/chennisden/handouts/algebra-primer).

### Nov 25, 2025

- **Products/Coproducts**: All mentions of a function $f$ were supposed to be $\phi$.

### Oct 31, 2024

- **Exercise 2.11.** Significant typo; meant to write $g_1^(-1) g_2 in H$ rather than $g_1 g_2^(-1) in H$.
- **Exercise 2.29.** Significant typo; meant to write $N \cap H \lhd H$ rather than $N \cap H \lhd G$.
- **Proof of Theorem 2.31 (Second Isomorphism).** Minor typo; meant to write $N \cap H$ instead of $n \cap H$.
