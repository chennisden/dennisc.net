---
title: The real numbers
date: 2024-04-04
---

You probably have a good intuition about the natural numbers ($\mathbb N$). Just count: $0, 1, 2, \ldots$. You also know how $+$ and $\times$ work on $\mathbb N$ very well. From there defining the integers ($\mathbb Z$) and then the rationals ($\mathbb Q$) is quite natural: it'd be great if we could reverse $+$ and $\times$, and defining $\mathbb Q$ is how we do that.[^field]

[^field]: In more technical terms, $\mathbb Q$ is a field: we have a $+$ and $\times$ operation, an identity with $+$ (i.e. $0$), an identity with $\times$ (i.e. $1$), and additive/multiplicative inverses. Actually, $\mathbb Q$ is the smallest field containing $\mathbb N$.

Getting from $\mathbb N$ to $\mathbb Q$ was not hard. But where on earth do the reals ($\mathbb R$) come from?

## Wrong answers only

We're going to work backwards a little. We'll touch on natural ideas that edge closer to the real numbers, then shine a spotlight on why the rationals are lacking (and how they naturally lead into the reals), and then finally revisit some of these topics with our newfound understanding of the reals.

At the same time I will reference the reals without explaining what they are yet. Sorry about that, but it helps to be able to say "this idea doesn't perfectly match the final answer" even if we don't know what the final answer is yet.

### Algebraic numbers

You might think we'd like to invent the real numbers because we want $\sqrt 2$ to exist, or more precisely, we want $x^2 = 2$ to have a solution. Not entirely. There are two problems with $\mathbb R$: it contains too many numbers and at the same time too few.

Let's take our desire of $\sqrt 2$'s existence to the extreme. We want roots for all rational polynomials: this necessarily implies the Fundamental Theorem of Algebra holds, i.e. that we can factor every polynomial into the form $k(x - q_1)(x - q_2)\cdots (x - q_n)$ where all $q_i$ are rational.

Actually, the set of real numbers that are solutions to a rational polynomial are called the algebraic numbers ($\overline{\mathbb Q}$). Algebraic numbers are countable (because polynomials are countable) yet the reals are uncountable. By the way, the algebraic numbers are "enough" in this setting: the Fundamental Theorem of Algebra holds for $\overline{\mathbb Q}$. There is no reason to go "beyond" $\overline{\mathbb Q}$.

So there are real numbers that are not algebraic. The real numbers are, in one sense, more than the algebraic numbers. So ensuring roots to all polynomials could not possibly be the rationale.

At the same time there is no solution for $x^2 + 1 = 0$ in $\mathbb R$. So the algebraic numbers contain too many numbers, that is they contain complex numbers.

### Cauchy sequences

> I'd like to be able to write an infinite sequence of digits, like $3.1415\ldots$, and still have that be a number.

Yeah, this is pretty close. One of the official reasons for defining real numbers is so that this sequence of digits is some real number. (It's a bit more general than that, but it is close!)

### I want Intermediate Value Theorem to work

Yes! For the Intermediate Value Theorem we need continuity, which doesn't do much for integers. A concept of continuity does exist for rationals and works reasonably well. But actually the intermediate value theorem fails for rationals.

Here is an example: consider the function $f(x) = 0$ if $x^2 < 2$ and $f(x) = 1$ if $x^2 > 2$ where $f$ sends rationals to rationals. This is continuous: if you know $\delta$-$\epsilon$ proofs you can check this yourself, and if you don't you can take my word for it. (In the reals this is discontinuous only at $\sqrt 2$ and since $\sqrt 2$ doesn't exist it is now continuous!) Clearly we have $f(0) = 0$ and $f(2) = 1$, but we have no $x$ satisfying $f(x) = \frac 12$. Ouch.

The same goes for many other theorems like Mean Value Theorem. All these things where we need "something in between" fail on $\mathbb Q$!

## So why is $\mathbb Q$ lacking?

Consider the set $S = \{x^2 < 2 \mid x \in \mathbb Q\}$. You can easily bound this set from above. For instance, every element in $S$ is less than $2$.

But is there a *least* upper bound? No. No matter what upper bound you give me, I can always give you a smaller one. If you give me $2$, I will give you $1.5$. And then you can give me $1.45$. And so on and so forth: we can keep one-upping each other.

Obviously if we took the reals for this set we'd have a least upper bound: $\sqrt 2$. But we want something more general. And in fact the following is true:

> Given any non-empty set $S \subseteq \mathbb R$ bounded from above, $S$ has a least upper bound $\sup S$.

By "least upper bound", we mean given any upper bound $b$ for $S$, we have $b \geq \sup S$. Also we'd obviously like for $\mathbb R$ to contain $\mathbb Q$.

## Constructing the reals

But we cannot just will $\mathbb R$ into existence. We actually need to prove it exists somehow. Here is the construction:

> **Dedekind Cuts.** A *Dedekind Cut* is a non-empty set $A \subsetneq \mathbb Q$ satisfying two properties:
>   - $A$ contains no largest element.
>   - If $a \in A$ and $b \in \mathbb Q \setminus A$ then $a < b$.

Intuitively, we cut the rational number line in half. All points to the left are in $A$; all points to the right are not in $A$. Of course, this point at which we cut the rationals is not necessarily a rational. We cannot even rigorously define such a point (if we could, no reason to bother with Dedekind Cuts): we can only reason in terms of the elements of $A$.

Here is the kicker. *These Dedekind Cuts are our real numbers.* They don't merely *represent* real numbers. They are, under the hood, what we are referring to when we say "real number". Now it is important to note there are other ways to define the real numbers such that they satisfy the properties we want them to. And it turns out these notions of the real numbers are all equivalent (formally they are isomorphic). This is well outside the scope of this post, but for now we take this definition of the reals and run with it.

Recall our wishlist: we wanted a least upper bound property and we wanted $\mathbb R \supset \mathbb Q$. Implicit in this wishlist is a notion of order (i.e. $\leq$), a notion of addition, and a notion of multiplication. Furthermore they have to be consistent with the respective notions from $\mathbb Q$. Let's get down to defining and checking these.

### What are rational numbers in the reals?

A rational number $q$, represented as a real, is simply $\{x \in \mathbb Q \mid x < q\}$. It is easy to check this is a Dedekind Cut.

### Ordering

We say $A_1 \leq_{\mathbb R} A_2$ if and only if $A_1 \subseteq A_2$. It is easy to check that $q_1 \leq_{\mathbb Q} q_2 \iff A_1 \leq_{\mathbb R} A_2$, where $A_1$ and $A_2$ are the reals associated with $q_1$ and $q_2$.

Also we always have at least one of $A_1 \leq A_2$ or $A_2 \leq A_1$, and when both are true $A_1 = A_2$. (When people say $\mathbb R$ is a *complete ordered* field, this is what they mean by "complete ordered".)

### Addition

We say $A_1 + A_2 = \{a_1 + a_2 \mid a_1 \in A_1, a_2 \in A_2\}$. We can check this is a Dedekind cut and it preserves ordering the same way addition does in the rationals.

Also we may define the additive inverse: $-A = \{q \in \mathbb Q \mid -q \not \in A\}$ where $q$ is not the minimal element in $\mathbb Q \setminus A$.

### Multiplication

This is a bit more annoying. First we have to define positive and negative reals in the obvious way. (Remember reals are cuts and comparison with $0$ is through subset comparison.) For positive reals we define
$A_1 \times A_2 = \{a_1 \times a_2 \mid a_1 > 0, a_1 \in A_1, a_2 > 0, a_2 \in A_2 \} \cup \{q \mid q \leq 0\}$. If $A_1$ is negative and $A_2$ is positive then $A_1 \times A_2 = -((-A_1) \times A_2)$. There are more cases that I cannot be bothered to list here because they provide no further instructive value.

## Right answers only

The details above are not particularly important. Here is what matters: $\mathbb R$ is a superset of $\mathbb Q$ that preserves ordering, addition, and multiplication. Furthermore, it gains the least upper bound property ("any non-empty set bounded from above has a least upper bound"). Through that it gains some other properties.

### Cauchy sequences

The terms in the sequence $(a_n) = 3, 3.1, 3.14, 3.141, 3.1415, \ldots$ eventually get arbitrarily close to each other. Formally, for any $\epsilon > 0$, there is some $k$ such that for all $n, m > k$, $|a_n - a_m| < \epsilon$. (Any sequence satisfying this condition is called a **Cauchy sequence**.) If we consider $(a_n)$ as a sequence of rationals, it doesn't converge to anything. We know this because it converges to $\pi$ in the reals, which is not a rational number, and limits are unique in the reals. (This is not obvious and requires proof, but here we take it for granted.)

So there are rational Cauchy sequences that do not have a limit. This is the other sense in which the rationals were lacking. The reals, however, do not have this problem. Any Cauchy sequence in the reals also converges to a real number.

### The Intermediate Value Theorem

*The full proof in all its detail is in Section 1.6 of Charles Pugh's Real Mathematical Analysis.*

Formally, the Intermediate Value Theorem states

> Let $f$ be a continuous function on $[a, b]$ and suppose $f(a) \leq f(b)$. If $f(a) \leq k \leq f(b)$ then there is some $a \leq c \leq b$ such that $f(c) = k$.

Continuity is defined with the usual $\delta$-$\epsilon$ criteria: if you don't know them and are curious you may easily Google it.

This is true in the reals, and the proof fully uses the least upper bound property. The proof also relies on the facts that continuous functions on a closed interval of $\mathbb R$ are bounded: because it is longer we do not reproduce it here. We just take it for granted. It is proven in a very similar fashion.

> Let $V(x)$ be the set of values taken by $f(t)$ for $a \leq t \leq x$; in set notation, $V(x) = \{f(t) \mid a \leq t \leq x\}$. Obviously each $V(x)$ is non-empty and we took for granted that $V(x)$ was bounded, so $V(x)$ has a least upper bound; let it be $\sup V(x)$. Set $X = \{x \in [a, b] \mid \sup V(x) \leq k \}$. Clearly $X$ is non-empty as $a \in X$ and $X$ is bounded from above by $b$. We claim $\sup X = c$.
>
> We may use the $\delta$-$\epsilon$ condition of continuity to show that $f(c) < k$ and $f(c) > k$ both lead to contradictions. Very informally, continuity tells us that if $f(c) < k$, then there's some $d$ slightly larger than $c$ such that $f(d) < k$ as well (and also $f(t) < k$ for all $c \leq t \leq d$). Likewise for $f(c) > k$. So we must have $f(c) = k$.
