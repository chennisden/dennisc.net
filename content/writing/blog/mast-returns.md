---
title: MAST returns, sort of
date: 2023-08-17
---

The title is a bit clickbaity: MAST stays dead, and what I'm going to be doing will differ significantly. But the substance is there: **starting this August, I will begin accepting students again.**

It will be group lessons, once a week for about an hour. It will be held online on some platform that is **NOT ZOOM**. There will be 10 official lessons a semester, all taught by yours truly. The difficulty will roughly be mid and late AMC/early and mid AIME. **Tuition is $100 a semester (10 lessons)**[^finaid], slides/handouts will definitely be provided, and recordings maybe (if I remember to record). The timing will probably be 3 PM Sunday EST (hard start) to 4 PM (soft stop).

[^finaid]: Obviously financial aid will be offered. If you are in the position where paying for math contest training is a bad idea, then your entire tuition will be waived, no questions asked.

The material will be old MAST contents for everything except geometry. For geometry, I will be piloting the draft of a textbook called "A Brief Foray Into Euclidean Geometry". It is easier compared to the content a prospective student might be used to working with for the other subjects, but I think people *really* suck at geometry. (And it is hard to expect otherwise, given the lack of any good intro level geometry text.) Old MAST materials may have to supplement the text, depending on how advanced the group I get is and how far I get into the draft.

I will not be the sole teacher; however, the amount of instruction other Math Advance staff will do is also to be determined. The only thing for certain so far is I am teaching intro geometry (i.e. the lecture associated with *Foray*).

Something a bit experimental which might not pan out: I want to try having guest lecturers. This is where other Math Advance staff might be able to help out, or any interested other people. If it happens, the content will be whatever they want, maybe keeping in mind your level of experience. No promises this will happen or to what extent though.

Where I would want staff help is the following:

- Answering questions
- Hosting guest lectures

If you are interested in helping as staff, email me at [dchen@mathadvance.org](mailto:dchen@mathadvance.org). Otherwise, if you want to apply as a student, hang tight (and spread the word!) Details will come soon in another post, probably within a week. (Said details include finalized timing, the registration process, and a tentative syllabus.)

## Details

These details will vary as my and the other instructors' lives shift. (For instance, we don't know when we have midterms.) But here is what we know.

- Read the [application document](/apply-2023.pdf) and follow its instructions to apply by **September 3rd**.
- Classes begin September 9th.
- After 4 or 5 classes we take a week-long break for midterms. I will try to time the bulkiest lesson, the intro geo lesson, to be right before that.

### Syllabus

Tentative. No promises what order content is covered in, but the subjects will be interwoven and lectures will be in roughly increasing order of difficulty.

**Algebra**

- Basic algebra, including symmetric manipulations and logs
- Polynomials (division, remainder theorem, etc)
- Summations (and janky miscellany, e.g. AIME I 2023/10)

**Combinatorics**

- Introduction to Counting
- Perspectives

Potentially you will see a guest lecture on efficient casework.

**Geometry**

The primary text is *Foray*. If I magically finish Ch. 4 by the time I teach Triangle Centers, it will use *Foray* too. A copy will be provided, free of charge, for students.

- Angle Chasing (Ch. 1, 2, 5 of Foray)
- Triangles (Intended to be Ch. 4, 6 of Foray, but no way I finish that fast)
- How to do trigonometry (for real). $\sin(a+b)$ and $\cos(a+b)$, complex numbers. Sort of a hybrid of algebra and geometry.

**Number Theory**

- Mods (definitions, FLT, and Chinese Remainder Theorem)
- Prime factorization (incl. divisor count, sum of divisors, and $\nu_p$)

## A Disclaimer

We take our work seriously. This class is meant to be well-designed and teach you things. However, "perfect is the enemy of good", and as much as I don't like this philosophy in general, it does apply to this class. If I were to wait for our team to develop *perfect* content for an AIME class before we taught, we'd be completely out of touch 30-something year olds trying to teach competition math. (Foray, which does not abide to this philosophy, has taken a breathtakingly long time to write: the first two chapters took two years.)

Nor is this an incredibly battle-tested curriculum. Yes, we have used various versions of the materials with an AMC class, an AIME bootcamp, and MAST. But this is not a class that has been taught a hundred times; it is the first time we are using this curriculum. We have made changes after every iteration, and we can only hope they have been for the better.

By all means, I think you will learn a lot from this class if you elect to take it. However, it is not my intention to mislead you as to what this class is. Our team consists of USAMO and MOP participants. Our students have gone on to participate in these programs too. Yet no class, least of all this one, will be the magic bullet that gets you there. It is not some gospel. It is just meant to shift your thinking a little bit and hopefully give you the boost you need to improve.
