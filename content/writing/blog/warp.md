---
title: "WARP 2023: preliminary thoughts"
date: 2023-03-13
---

WARP is an applied [rationality](https://www.lesswrong.com/tag/rationalist-movement) camp from March 5-15. For reasons I will explain later, I am not a rationalist: I didn't come into the camp as one, and I will not be leaving the camp as one. This is the penultimate day of WARP and my introversion has reached a critical mass. So instead of participating in activities, I'll try to retroactively write about what happened in the last weekish.

## Day-by-day

### Day -1 (Mar. 4)

The day before the camp started, I was supposed to run the 1600m/800m at the Dan Gabor Invitational. Then I got pretty sick and didn't want it to get worse because of the rain... which really sucks, because now I have to run a 3200m when I get back. Yay!

I did not do any of my homework, but I started writing two puzzles during the night: [Cars](/puzzles/cars/ctc) and [Monster Without A Name](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000D9A). This was despite the fact I basically needed to wake up at 4 the next day because my flight was really early.

### Day 0 (Mar. 5)

On the plane, I finished constructing the Cars puzzle and put a bit more work into Monster Without A Name. When I got off the plane and found the other WARP people, they started a conversation about robots doing surgery and other nerdy/interesting topics.[^grass] When we got to the campsite --- which is *really cool*, they had archery/a pool/rock climbing (more on that later!) --- we immediately started chucking around a frisbee for 20 minutes standing right outside our dorms.

[^grass]: I promise we all touch grass; my theory is that some subset of the students were like "oh my god I can openly be nerdy" and then they did that.

After some trivial banter/dinner/other stuff I forgot, the opening ceremony began. The only noteworthy part was "due to past events", there was a policy against romance. Not just student/staff (which obviously held true for every camp), but also between student/student. It didn't affect me personally, but I'm including this because one of the quotes was, "There's obviously nuance as to what constitutes 'romantic', but if you hug for more than 8 seconds we'll send you back home."[^warning] I don't know why the director felt the need to elaborate on what constituted romance, but regardless, "8 seconds" immediately became a meme.

[^warning]: I'm putting this as a footnote for reasons of length/clarity, but the *actual* thing the director said was even funnier: it was only after the *second* time that you'd be sent home. The first time, you'd just be warned.

I forgot what happened the rest of the day, but there were no classes or anything of that sort. Since I don't remember, probably nothing much happened.

### Day 1 (Mar. 6)

Some context: the WARP schedule is divided into three sections, the morning, afternoon, and dinner. In the morning there are semi-structured classes, in the afternoon people announce/hold activities on the spot, and after dinner there is usually some staff-run event.

I see the schedule and immediately notice: "(8:00 - 9:00) Breakfast" as the first event. I think, "huh, this seems pretty late!" And surprisingly, there are very few things actually firmly scheduled throughout the day (and as I would soon learn, even the classes scheduled in the morning were ditchable). Of course, a late start just means more room for people to stay up until 2 AM, so a couple of times I almost miss breakfast and once I actually did miss breakfast.

Classes:
 
- The first class I had was *Map and Territory*, and the lesson can basically be summed up into one sentence: "The map (models) do not completely capture the territory (reality)".
- The second class was *Philosophy of Therapy*. Somewhat contrary to what the name suggests, it just explored modalities of therapy (the instructor, Damon --- who is also the director --- is a therapist). Much of this I knew from AP Psychology, but it was very interesting to have an actual psychologist go over what I learned in school.
- The third and last class was Bayes' Theorem. It was just meant to establish that everyone knew Bayes' Theorem (and get the outliers caught up), but
    - no other class really used Bayes' Theorem,
    - and I'm pretty sure everyone knew Bayes' Theorem.

Going into the camp I had an expectation that the classes would be more technical. The website said

> The curriculum covers a wide range of topics, **from game theory, cryptography, and mathematical logic,** to AI safety, styles of communication, and cognitive science.

which I took as meaning "about half of this is technical and the other half is either nonsense (like AI safety) or soft skills (which are very much not nonsense)". But I already noticed a couple of trends in the first few classes, some of which I hoped would not continue:

- There was a strong emphasis on soft stuff, like rationalist philosophies.
- The content density was relatively low. The technical class (Bayes') was pretty simple[^blessing], and a lot of classes were at least partially interactive/discussion-based, which by definition lowers content density.

[^blessing]: In retrospect the Bayes' class was a blessing: especially after Jason (a guest instructor who taught a math class) left, the number of technical classes was basically 0.

In the afternoon, there are typically three 1-hour time slots for events, starting at 3:30, 4:30, and 5:30.

- I was already really tired after three back-to-back classes, so I decided not to show up to any of the 3:30 events.
- At 4:30 Jason led a talk about "math that blows minds" and basically ended up converging the lesson on Lob's Theorem (very interesting, and would end up becoming a theme for his talks). I went to this one.
- At 5:30 I gave a talk on how to write (sudoku) puzzles, which at some point I hope to record and turn into a video.

The after-dinner event this day was two scheduled 1-on-1's, which is the official term for "deep conversations between two people". I ended up not really buying into 1-on-1's, and eventually I figured out that 1-on-1's weren't something I actually wanted to seek out, it was just something I felt like I was supposed to do.[^rebel]

- The first one was with Jason, who had a lot of interesting math problems to share (examples include, "The Kolmogorov complexity of a string is the length of the shortest program that prints that string, what fraction of all strings have provable Kolmogorov complexity?"[^kolmogorov])
- The second one I had was with Srijon, a math olympiad student from Kolkata I kind of knew through OTIS. We talked a lot, both about math and about culture ("how to assimilate into American culture?" My answer was, "don't bother, I think the biggest divide here isn't even nationality, it's interests.[^interests] You have a unique perspective and can provide a lot of value through that instead"). Eventually we realized it was time to go to Max's "Get Stronger" talk, which was obviously the most important thing this entire camp. (I'm not joking.)

[^rebel]: Which doesn't mean I did it, just the opposite. If I'm told I should do something (or it's implicitly stated) and I don't believe it, I often and easily decide not to do that thing. I was one of a small minority of people who felt this way. More power to the people who were able to extract value from 1-on-1's, I just wasn't one of them.

[^kolmogorov]: Finitely many, because consider a program with length L that searches for proofs of the form "String **s** has Kolmogorov complexity greater than L" and halts/outputs **s** when such a proof is found. The Kolmogorov complexity of this string is at most L since your program of length L generated this string, yet you have a proof that its Kolmogorov complexity is more than L, contradiction. So the length of a string with provable Kolmogorov complexity is bounded.

[^interests]: Namely, math and not Keeping Up With the Kardashians or whatever. And if this is your cultural disconnect, I think that's a good thing.

After the "Get Stronger" presentation (which was mostly in lowercase), we went to apply our knowledge by hitting the gym. I'd end up hitting the gym every day, where in cross country/track we only hit the weight room twice a week. I haven't dedicated a whole lot of space to this, since it's pretty simple (eat more protein, sbd, etc --- hit me up if you want more details about exercise!) but I want to emphasize again that this was the most important thing for me that happened in the entirety of camp, it made me actually love going to the gym.

### Day 2 (Mar. 7)

Classes:

- **Math That Blows Minds: Unifying Frames** was about category theory. I think I understood at most 40 percent of this class, but it was really cool to learn about something I have never seen before. Now I am kind of inspired to learn category theory in the summer.
- **Emotions Make Sense** was about how emotions inform us of our wants, and how to accurately figure out what we want (as opposed to what we think we want, what we want to want, etc).
- **Entropy and Agency** started off with something concrete (entropy) and then turned into how "agents" (whatever that means --- you can treat it as humans) basically reduce their own entropy while increasing the entropy of their environment, and how agents interact. I did not understand the second half and do not know how much of it is actually real, as opposed to LessWrong speak.

Activities: I did none of them since I had an idea for **Monster Without A Name** and holed myself up in my room finishing it. (I ended up making mistakes on the puzzle by overlooking possibilities multiple times, so I would hole myself up later trying to patch these mistakes, but never for as long as on Day 2.)

Dinner: I hit the gym after dinner and then arrived in the middle of storytime. (Or I arrived in the middle of storytime for some reason and then hit the gym, I honestly don't remember in what order.) There were a lot of very personal stories, and because they were very personal I will not share any of them even though I remember most of them.

### Day 3 (Mar. 8)

We got our first covid case and all classes were moved outside. The camp premises had two gazebos, which we used for classes that day. Damon (I think) pointed towards one gazebo and said "Andis will be teaching at this gazebo and Jason will be teaching at that gazebo". In typical WARP fashion, the gazebos were forever named "This Gazebo" and "That Gazebo". Wind would end up being very annoying, since it would blow over the easels all the damn time.

Classes:

- **Thingspace**: Basically the theory of categories (but this was a soft class and not category theory, lol). Basically it was about how relating concepts like "brown", "tall", "made of wood", etc to each other is an $O(n^2)$ operation where relating concepts to a central, artificial node like "tree" was $O(n)$. (Also the time complexity for reaching equilibrium in the first case could be exponential in the case of contradictory signals.) Andis went over how our models of categories were a (useful) simplification, and how we could resolve confusion by thinking about things more "purely".

    As a concrete example, for the debate "are hot dogs sandwiches?" he established that a potential core disagreement could be whether there is a continuous clump of sandwich-like things from the prototypical sandwich to a hot dog (when graphed on the hyperspace of relevant traits), or whether hot dogs and sandwiches were two separate clumps.

    This all sounds super abstract but it is because I am communicating the idea poorly. This was really an interesting class and one of the more useful ones to me.
- **Technical Agency**: Basically a lot of decision theory I didn't understand (and am not actually sure is real). Newcomb's paradox is basically trivial unless the prediction is super accurate (implying determinism and a God-like intelligence) or your future actions influence the past (presupposes a simulation or something). Neither of these axioms seem reasonable.
- **How to Train a Dog**: Behavioral conditioning works on animals and maybe people too (in some circumstances). Use positive rather than negative reinforcement. (Most of this I was familiar with because of AP Psych.)

Activities: Again I did none due to a combination of working out and holing myself in my room to write **Monster Without A Name**. 

Dinner: We had a very long talk by Bryan Caplan about how EAs should support open borders. While I think the reasoning is somewhat sketch, especially since I am not an EA, I agreed with the premise of his argument: more immigration, even of low-skilled workers, is probably a good thing.

Since that went on for very long, I slept pretty late and my sleep schedule got slightly fucked.

### Day 4 (Mar. 9)

Classes:

- **How to Change**: Sicong used an example from his own life to explain how present problems (e.g. procrastination) can be tied to past trauma, and how to overcome it. I think the source material was a book but I cannot remember the title of that book. I haven't noticed this really happening in my own life, but the key word is **haven't noticed**. I'll keep this as a tool in my belt: hopefully I won't need it, but if I do, I will know how to use it.
- **Leadership**: The instructor said nothing the entire class and had students basically do whatever with the class. I'm pretty sure the class was trying to make some meta point, but honestly, I walked away with the feeling "it is easy to be clever and hard to be actually helpful." This is one of the few classes I think I should've skipped; the message did not really resonate with me.
- **Uncertainty**: If you've seen Evan Chen's post on the interval guessing game + how we are way too confident in our intervals and need to make them embarrassingly wide, that's what this class is about! We filled in the blank "Washington was born before X" with predictions that we were 80, 95, and 99 percent confident in.

Activities:

- At 3:30-4:30 there was a scavenger hunt with a lot of running around that was pretty fun! We (Aname, my new favorite meme team name) got 3rd out of 4th, which is pretty bad but whatever.

I didn't participate in the other activities for the same reasons as before: working out, writing a puzzle.

After dinner we did Lightning Talks, which are three-minute talks on literally anything. Fionn and Zdenek wrote each others' talks, and Zdenek's[^letter-removal] talk (which Fionn wrote) was about proving that $2n$ was even via induction. I gave two talks: one about why Woodrow Wilson was the worst president (I forgot the Spanish Flu in my talk, which Hugh added in the penultimate talk), and another on the solution to the WARP geometry application question.

[^letter-removal]: Speaking of Zdenek, since his second e has an accent or something, the nametag printer printed "ZDEN K". I would frequently "remove letters" from his name for arbitrary reasons, and at one point he was down to "ZD K". (I gave him his letters back near the end.)

### Day 5, the interregnum (Mar. 10)

This was a "break day", which meant way too much physical activity. There was a high ropes course, archery, wall climbing, and the pool was opened. I did all of these things, in addition to the gym. Thank god the track workout was speed development that day, if it was anything harder I might have actually died of exhaustion.

I also did a bit of work on **Monster Without A Name**.

### Day 6 (Mar. 11)

This was two days ago? It feels like an eternity.

Classes:

- **Language and Logic**: A brief history of the philosophy of language. Very interesting, and the only class I came in with literally 0 information beforehand.
- **Information Hygiene**: Be careful about the info you receive, fact-check anything you spread, and curate a good circle of information. I honestly thought most of this was too much for everyday life, since there are a lot of fun facts I don't actually care about (i.e. there is no difference in my life whether it is true or not). But one point Damon made really resonated with me: **communicate your level of confidence when you say something**. I wrote about this [more than a year ago, while I was still in junior year](/writing/essays/modifiers), and this is one of my essays I strongly agree with even now.
- **Inadequate Equilibria**: Efficient market hypothesis is false. Most people don't know most things and arbitrage is not always possible, whether it's due to transaction costs or some other reason. Also the Prisoner's Dilemma has a suboptimal equilibrium.

I unfucked up **Monster Without A Name** and finally made a reasonable, solvable version. I don't actually know what events happened that day, but I think it's plausible that I didn't do any of them to finish **Monster Without A Name** and work out.

After dinner:

- There was a Fermithon and due to technical difficulties we were basically robbed out of our win >:(
- We had a lot of epic freestyle rap battles where I shit on applied math[^uc] and Texas[^ut], among other things.
- We played Secret Hitler.
- I did a midnight tempo run ($6\times 6$ minutes with 1 minute running recovery).

[^uc]: I applied for Applied Math in UCB and UCLA...

[^ut]: Where I very likely will be going to college...

### Day 7 (Mar. 12)

I was going to write a journal this day but got super busy.

Classes: Probably because of lowering participation rates, every class was offered once instead of splitting into three cycles. Also these classes were more focused on practicality, and were called "workshops" instead.

- **Premortem**: I was pretty familiar with this concept and it seems like a fancy way of saying "be aware of what might go wrong". But the act of actually visualizing the failure modes probably makes it more effective.
- **Resolve Cycles**: You basically decide to do something for the next 5 minutes and pray momentum carries you. I have no idea how useful this actually is and want to find out by trying it. (I ended up writing an email I'd been putting off for the practical part, and let me just say it's kind of pissing me off right now...)
- **Double Crux**: Instead of arguing, try to understand what shapes the core of your beliefs. Then if you can find a core belief that contradicts the other person's (a double crux), you can either work from there or realize you have a fundamental facts/values disagreement that cannot be resolved. Alternatively, maybe it's a factual disagreement that can easily be resolved.

I skipped Afternoon Meta to lift weights. This isn't the first time I skipped for this reason, but it's the only day I remember skipping for gym for sure. This time, I showed up to two activities: Logic (this was actually math, which was quite exciting and refreshing after a ton of days with no math), and Physics Intuition (where we guess what happens for physics experiments). I forgot that rotational inertia was inversely proportional rather than directly proportional to acceleration like an idiot, and then the rest of the questions were just super hard (but still very interesting).

After dinner, there was the second edition of lightning talks, in which people had to give hot takes (i.e. at least half of people had to disagree for you to give your talk). Predictably, the number of reasonable hot takes people had was pretty low, because most people agreed on most things --- that's what happens when you select from people with similar backgrounds and shared experiences (including but not limited to the 10 days at camp). Max used the Cobbs-Douglass function to explain why people should be at the gym instead, a bunch of people changed their minds, then everyone realized the logical implication: if you changed your mind you should go to the gym. So half the people left for the gym and Lighting Talks quickly died.

Then I ostensibly solved puzzles in the Lodge and went on a short recovery run to finish my Sunday minutes. I honestly forget why I slept so late.

### Day 8 (Mar. 13)

The final day of classes, and the penultimate day of camp. I attended every class today, which means that I am one of two people with a perfect attendance record (for classes at least --- I don't count the metas because they are mostly useless).

- **DADA**: How people can use "Dark Arts" to emotionally manipulate you and how to protect yourself from these appeals. Included Secret Hitler references.
- **Visible/Invisible Curriculum**: How explicit and implicit messaging can either synergize or contradict each other.
- **Why Everything WARP teaches is wrong**: I attended at least 40 percent because I wanted the perfect attendance record, but honestly this was preaching to the choir for me. I was already taking in this camp with skepticism and moderation, so "be skeptical and understand our biases and wait to make drastic changes" was something I already was planning on doing. Also it ended up just being a discussion which is never great for content density.

I went to lift and almost decided to skip afternoon meta but went because I wanted to learn about Quest Day. It turns out people were looking for us. It also turns out that it was a bad idea to go to Afternoon Meta, because that shit lasted forever and I was no longer warmed up when I went back to the gym. I technically hit a PR on bench (3 x 35 on each side) but definitely can one-rep max more, and hit a one-rep max of 70 (on each side) for deadlift. I still want to work on squat, because I just started squatting (we don't do squat in XC), and work my way up to what is probably my current limit.

I then went to my room to journal and ended up writing a puzzle instead ([Zoom](/puzzles/zoom/ctc)). The timezone on my computer was wrong so dinner almost ended by the time I was pinged for Hamming Circles, and I sped my way to get some food. Thank goodness dinner was still open. I missed Hamming Circles, though I never planned on going because I honestly didn't have problems I thought WARP would help with ("no sleep and no rizz"). I also don't like talking about my problems when I've made no real effort to solve them because that just feels counterproductive.

And here I am, in the Lodge, writing this journal post with no one around. Seriously, where the hell is everyone else? (I also need to go for a run but honestly might just skip today's workout and do it tomorrow. But it's short so I guess I'll do it today?)

## General thoughts

I expected this camp to be half soft skills and half hard skills. In some sense, I didn't get what I wanted out of this camp: significantly more technical abilities or knowledge. But at the same time I learned a lot of cool stuff, although I will take all of it with many grains of salt.

The content density in the classes was kind of lacking. I feel like all of the content could have been compressed in two days, and it's only two days because we did start out with some more technical lectures.Fortunately, activities were a lot better in this regard (when learning technical stuff was the intent of an activity).

I met a lot of cool people, many of whom touch grass (i.e. are not EAs). I respect the intelligence and character of the rationalists too, even though their lifestyle and worldview is very different from mine. I don't know if I regret not interacting with them more, because on one hand they are very smart people and I could've at least been rational (hah) and learned math from them[^time], but on the other hand the tradeoff would have been working out less which was unacceptable for me.

[^time]: If you guys are reading this, it's because I didn't have the time or energy after being a gym rat, not because I don't think you're very cool people. Maybe next WARP/SPARC/etc I'll get the chance to talk to more of you guys!

But overall I think this camp was a success in my books. I worked out a lot, learned cool stuff, and had fun. Also it was free and I missed school, so the opportunity cost was basically 0.

## Why not rationality/EA?

I'm not going to think about this too hard because I think jumping into that rabbithole is a total waste of time.

I had a 1-on-1 (I know, surprising) and one of the topics that came up was the following:

> Why are you not a rationalist?

> Me: *gives very "rational"/logical reasoning*

> Sounds pretty rational to me bub

On one hand I actually agree with rationalists on a lot of stuff, particularly when it comes to more difficult problems.[^life-extension] I also approach some proper subset of my life using reasoning and logic, just as every other person with more than 3 brain cells does. But on the other hand, I do not think that this set of worldviews or abilities makes for a particularly interesting definition of a rationalist.

[^life-extension]: For one I believe life extension, alternatively the fight to stop death/aging/etc, is a very important problem. I also think that anyone who says "death gives life meaning" is full of shit, even more full of shit than the extreme rationalist/EA community. And I think those guys are pretty full of shit.

I don't make logical reasoning/math the deciding factor of important decisions because I believe the error bars of my calculations would be too wide, whether it's because of faulty observations or faulty logic. Yeah, this sounds pretty Rational with a capital R, but I genuinely just do not approach most of this life with my mindset, particularly because I believe it's not worth thinking about all this extra stuff just for marginal benefit. But rationalists (like Slate Star Codex) are right about a lot of things --- most of them are just things I do not care that deeply about.

But some subset of rationalists (and I argue, the prototypical rationalist) think stupid shit like AI safety or whatever is the most important problem and that the rest of human suffering doesn't matter in comparison because "according to my (shitty) EV calculations".[^error-bars]

[^error-bars]: When your axioms are so sketchy and theoretical, I really think you should consider that your error bars should be really fucking wide and make "according to my calculations" worthless.
