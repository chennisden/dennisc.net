---
title: Paragraphs
date: 2022-02-13
---

It's the Sunday before essays are due, and that means I'm going to have to read a littany of poorly written papers in exchange for having my equally poorly written paper read.

There are two things about English essays that really bother me: the lack of paragraph breaks and the boring titles. These two things should be alarming to any English teacher, particularly because **even the best writers/papers suffer from this**.[^grade]

[^grade]: There's no such thing as a well-written or interesting English paper, so by "best papers" I really just mean "the crap that gets you a good grade".

This suggests a problem endemic to English classes. You can't get rid of the disease just by treating the symptoms, but on the other hand you need the symptoms to figure out what the problem is.

## Press Enter

A lot of people have been given totally wrong ideas about a paragraph since elementary school. And this is because teachers tell you, "write a paragraph about your thoughts", and students always ask, "how long does a paragraph have to be?" So then the teachers, thinking they are doing their students a favor by not letting them get away with the minimum possible work,say four to five sentences.

Later a "paragraph" will mean 300 words, or however much space you need to fit in your mandatory 3 quotes per main idea. And even though no one would say the definition of a paragraph is "whatever the teacher says", the way people are writing sure sends a different message.

In a 4 or 5 paragraph paper you have paragraphs sprawling across the entire page, obscuring the development of separate ideas and jumbling it all into one giant clusterfuck. If you go on arxiv and look at a random paper, chances are that each page has 3 or 4 paragraphs, even when some of the space is taken up by diagrams. So even _math research_ is easier to read than a high school English essay.

Chances are that everyone knows when to start a new paragraph: at the place that most optimizes readability. Length and relatedness as two of the largest factors, but there are plenty more.

If everyone knows when to start a new paragraph, why aren't people doing it in English class? The answer is pretty boring: because the teacher told them not to and they don't care enough to.

## Title

I suspect the only reason English papers even have titles is because the MLA format requires them. And the titles are usually pretty boring: "An Analysis of Luce's Rhetoric" or what have you.

Typically in English class you struggle to pick a title because you can't think of any. When I struggle in my own essays it's for the opposite reason: there are so many good choices I can make.[^although]

[^although]: Although the only one I've even thought of alternative titles for was Settling for Mediocrity. And even then, I knew I was going to use Settling for Mediocrity because that was the old title before I rewrote it.

A title is just a really short version of a hook. And a hook explains why you wrote your essay. For instance "Force Multipliers" is about how tools can have multiplicative rather than additive value. "Hook" is how you get someone to read what you wrote. And so on.

If I were really honest in my titles for English class, it would be "Please give me an A I want to go to MIT." That won't fly, so "Into the Unknown" it is.
