---
title: Puzzle-writing goals, part 1
date: 2023-01-06
desc: A reflection on my puzzle-writing goals so far, and some updates based on what I've already done.
---

I've already written four puzzles and it hasn't even been a week, so the January goal seems pretty easy. However, I'm going to be participating in MIT Battlecode with Math Advance, so I don't want to update this goal because keeping it easy is not bad. Also I have to apply for scholarships :(

For the February goal, I obviously don't know every common variant yet (I recently learned about non-disjoint, for example). So, I'll just consider the common variants I listed as examples, and any (fun) variants I haven't used in January will count. (Which means that if I set one of the variants I list in January, it doesn't count in February.)

The March goal is actually way too easy. I wrote two easy classic sudoku puzzles already, but they don't use intermediate to advanced techniques (partially because I do not recognize them quickly or consistently enough). So, I want to set a classic with at least an X-Wing.

As for what I plan to do for the remainder of January, I want to explore the following two puzzle types I've invented: the close/distant neighbors variant (more generally, positive global restrictions on the difference of neighbors), and Equations (which I think had too few given digits and too many given equations). Stay tuned for more puzzles!
