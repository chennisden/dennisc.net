---
title: Suguru
date: 2023-04-27
---

The last puzzle I set --- and likely the next few I will set --- was a Chaos Deconstruction Suguru, a genre invented by mathpesto. These puzzles are really hard to do wrong, you set one of these and it's probably good just by virtue of existing. Granted, I've always been a Chaos Construction fan, so the region-building aspect is something that might not speak to everyone.

My impression is that these puzzles are in some ways easier to set than sudokus of similar difficulty. When setting a puzzle, you have two problems you need to simultaenously solve: you must make sure there aren't no solutions and there aren't multiple solutions. With a sudoku, you are not really guaranteed that solution at the start. But with a suguru, you start with the solution: a blank board. You just need to make it unique.

Also, since not every square has to be filled out, Suguru is a little more forgiving, since while the *geometric* interactions are plentiful, actual *numbers* do not interact that much unless you specifically design them to.

At the same time, I wouldn't recommend a first-time setter to try a Suguru in particular. It *is* much bigger, and there is less solver support, meaning that verifying a puzzle must be done entirely by hand. Also there's a lot of scanning when you check, and Sugurus feel easier to break.

There are a couple of quirks of this genre too, especially when solving. Since the solution is unique, say you have a cell completely enclosed off, so it can only be a 1. By uniqueness you know it can't be a degree of freedom so somewhere in that row there must be a 1. More generally, you can be sure that only squares that interact in some way with clues --- maybe not directly --- will have a digit. While using uniqueness as a logical basis for solving is cringe, sometimes it is useful to know what must be true and find the logic to confirm it. I got lucky in that uniqueness didn't actually help with my puzzle, and it doesn't really play a big role in any potential solve routes, but it is a minor pain point that exists.
