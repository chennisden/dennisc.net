---
title: Creativity is not methodical
date: 2023-05-05
desc: Aka how I came up with the idea for Monster Without A Name.
---

A couple of days ago I took the AP Chemistry exam, and what struck me about preparing for it was how methodical it was. You just recognize what type of problem it is, remember the steps you take to solve it, and voila! you are done. Sure, it's not always trivial because sometimes you just don't know what to do. Nowhere in the process is there much anxiety about whether you can do the problem though: you either know it or you don't. For the most part, you don't try questions, actively struggle on them for along time, miss the fundamental idea, and then go *damn it I could've gotten that if the conditions were right or I was a bit luckier*.

Well, that's what creative processes are like. Take puzzle-writing for instance: while much of the process is put a clue down, figure out what it does, and repeat, you do have to use some ingenuity to figure out what clues to place. But perhaps far more importantly, determining what kind of puzzle you even want to write is really not methodical. What ruleset do you want to use? How are you going to make the rules interact with each other? That kind of thing.

For instance, take my puzzle *Monster Without A Name*. Normally, you'd think that I wrote this puzzle and found a good name for it afterwards. And that is true for most of my puzzles. But for this one, I came up with the title before I had any clue what the puzzle would be about, because I really wanted to force the song lyric.[^trend]

[^trend]: This is kind of becoming a trend: another puzzle title I forced to work was First Burn. *Your enemies whisper, so you have to scream...*

So I first was thinking of just making a really hard puzzle. Problem is, you can't just decide to make a really hard puzzle, and I did make a couple of semi-hard puzzles but they all had obviously better titles, e.g. X-Lines Sudoku. So I thought, "maybe the constraint should actually have *Monster* in the name, at least".

When I thought of *Monsters*, I thought of the similar word killers, since you have Killer Cages and Little Killers. And since Killers do stuff with sums, Monsters should do the next simplest thing: products.

Then the question is, how do I do products? Just doing the product of every cell is really boring, since it removes a lot of nuance and degrees of freedom. So somehow, I would want to do region sums.

Now I had to decide how to construct my regions. First I tried using shading pencil puzzles like Yin-Yang or Caves to create a hybrid --- and I still think that approach has a lot of viability --- but after doing a few Chaos Construction puzzles, I realized: the most natural way to do products of region sums, which requires region construction... is to do the variant that uses region construction. The Monster Cages constraint had finally crystallized.

The process I took was definitely not methodical: if the puzzles I did or the thoughts I had changed a little bit along the way, I would've gotten a completely different puzzle or no puzzle altogether. In other words, the system is chaotic: small perturbations to initial conditions create a vastly different result. But far more importantly, the process wasn't even *logical*. There's absolutely no reason for me to fixate on, "ah yes let's force *Monster Without A Name*" to work as a title and then go to the lengths I did.

But that's the thing about finding inspiration. You're lost in a sea of possibilities, and having something to anchor you can be really helpful. Even if that thing makes no sense at all. And this, in ~600 words, is how and why I use song lyrics as the titles to my puzzles.
