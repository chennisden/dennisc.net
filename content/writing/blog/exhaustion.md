---
title: Exhaustion
date: 2024-06-26
---

In high school, I found it odd how many kids in TV shows would fall asleep
during school. It was a funny gag, sure, but there was just *no way it happened
in real life*. How the hell do you get so tired that you involuntarily fall
asleep during the middle of the day, sitting in the most uncomfortable chair
ever known to mankind? Meanwhile, I'd try as hard as humanly possible to fall
asleep whenever I finished a test (because we couldn't leave early), and the
best I ever managed to do was lie on a desk for fifteen minutes without getting
bored.

How blessed I was. As a college student now, let me tell you: there isn't a
single class I *haven't* fallen asleep in. And this isn't because the
professors are boring.[^voice] All of my professors during the spring of my
freshman year were downright fantastic, and yet I could not keep the lights on
in my head.

[^voice]: Although there is a certain quality to the voices of some speakers
that makes it easy to fall asleep. During the fall of my freshman year, I
attended the Pittsburgh Number Theory seminar and proceeded to knock out during
the first presentation. The second presenter was much more energetic, and I
didn't even have to *try* to stay awake then. Although maybe the (involuntary)
nap I took during the first presentation helped with that...

And I didn't even find CMU unreasonably hard. I wasn't staying up many nights
to finish assignments. I *was* staying up some nights for really stupid
reasons, like destroying some scrubs in Smash. But even on the nights where I
would sleep at a reasonable time, I'd just be perpetually *exhausted*.

In fact, this has been a concerning trend in my life. In elementary school I
would never want to take a nap because I just had *so much energy*. High
school, I'd wake up tired some days, but I'd be more or less at full power
whenever I needed to. This year, *I don't think I've been fully sharp for more
than 30 days*. This is *terrifying*. Sure, after this year I can tell you all
about the universal property of the free group. But despite this, I now feel
like a useless moron most of the time because *my brain is too exhausted to do
anything beyond the bare minimum*.

Back in the day, I'd take plenty of long walks with no distractions and think
about something meaningful. Or just sit down, breathing in the fresh air, with
the only thing on my mind being "how do I make this puzzle work?" Now in
college (especially during Tuesdays last semester when I had eight hours of
classes) I just shuffle between classes, wishing for the day to end so I can
just go and play Smash with my friends, goddamnit.

Despite the bad parts of high school (and college is by far an improvement), at
least I spent most of my waking hours fairly sharp. Yes, every day of high
school was 8 hours of classes, which was absurdly exhausting too, but I was
just bored. My brain wasn't completely fried, and when I got back from track
practice, more often than not I'd be itching to work on something.

Now I am at an internship, and it is so exhausting that I don't think I spend a
single day being *sentient for more than two hours*. And many days the amount
of time I spend being sentient is a big fat zero.

At least in my internship, I have two saving graces: it is a startup, so the
internship program is much less bullshit[^none] than it would be at big
tech,[^bigtech] and I am going to be back in school at the end of the summer,
which helps to stave off a lot of identity crises.

[^none]: In fact, the few things I find bullshit about the job can probably be
attributed to me being a moron who has no real useful skills. If I actually had
the ability to do stuff in the hardware space, I would probably be extremely
happy and sentient. Alas, my only skill is learning and writing about [abstract
nonsense](/math). This is not a skill that translates over particularly well to
*doing useful stuff*, especially when compared to a background of having done
useful stuff before. Not that I'd trade math for any of it, of course.

[^bigtech]: My friends at a certain big tech company have written three Java
classes in *a month*.

It is no coincidence that in my senior year of high school, *despite college
apps*, I was able to publish nearly fifty puzzles. After college started, I
have only published five. A similar trend has followed in my writing, though
thankfully to a much less alarming extent.[^ratio] And it isn't because school
takes more time --- if anything it is the opposite. And I actually enjoyed most
of my homework, rather than wanting to skewer my brain with a screwdriver
while doing it like I did in high school. So the rigors of college, however
high they might be, do not entirely explain this change.

[^ratio]: At a quick glance I'd say I've written half as much stuff this school
year as I did last school year. Considering my absolutely monstrous pace last
year, and considering the ratio of decrease is only 50% as opposed to 90%, this
is actually a success all things considered.

I can only imagine that --- at least in the average case --- things will not
get much better in the typical software engineering job. If left unchecked, my
life is heading towards a trajectory where I become more lifeless as time goes
on, until one day a Dementor comes and eats my soul wholesale.

## Dreams

> In the months that follow you bend to the work, because it feels like hope,
> like grace – and because you know in your lying cheater's heart that
> sometimes a start is all we ever get.

Of course, I have no such plans to ossify. But it has been important for me to
note that I have been more and more exhausted as time has gone on, and that
this trend does not seem likely to reverse itself on its own.

In some sense I have been living my life by the greedy algorithm. I skipped the
introductory math courses because it would make my academic experience more
interesting. I took on a ridiculous courseload because each individual class
seemed interesting to me. And the hard part is, many of these were good
decisions, at least when taken individually. But putting it all together, I
have spent my life becoming unnecessarily good at math and computer science. Do
I really envision doing this for the rest of my life? Or will the greedy
algorithm just lead me to a local maximum, stuck in the middle of a valley and
surrounded by alternative possibilities far more worth living?

Potentially math. There's a real case made for being a professor, though from
what I can see the road there is quite painful. But I couldn't envision
*programming* becoming anything more than a hobby: if I'm going to program
something, I'm going to take the time to do it well, but I cannot be assed to
spend time writing shitty code I don't give two shits about for eight hours a
day, five days a week, forty weeks a year for the rest of my life. In the long
term, this rules out most big tech. Because if I have to write another line of
React or `import magic` from Python again, I might actually go *insane*.

But in the ideal world, the world where I have a trillion dollars and the skies
in Pittsburgh are clear every day, I'd want to read, write, and maybe even draw
stories. I can't say for certain, but gun to my head, that would be a more
appealing future than doing math --- *especially* if there comes a point when
math literally stops making sense for me. At the very least, it is about
equally as important to me as math. Yet I have spent half of my waking hours in
college doing math, and yet not a single story has been written in the past
three years (let alone been published anywhere, including this website).

Why such the disparity? Because I'm much better at math than writing stories,
and people are much more likely to pay me exorbitant sums to be good at math
than to write. That's the problem with following the greedy algorithm: at any
point in time, specializing even further in the thing I am good at is the only
move that makes sense.

But in the grand scheme of things, I am terrible at writing stories and
just slightly less terrible at math. There is still time to decide to never
listen to another word of math again, learn enough CS to graduate, get a
part-time software engineering job and spend the rest of my life in a Starbucks
with some pen and paper. Ditto for a thousand other things I want to try but am
too scared to take seriously.

Yet I am still in a tech internship building badass robots[^build] and still
enrolled for a graduate class where I will learn how to show that CH is
independent of ZFC. It's not for practical reasons: there is no universe where
*model theory* makes me rich or even employed. But even with the many other
things I want to do in my life, model theory is *goddamn cool*. Math is cool.
There is no way I'm ever completely giving up a subject where you can look at a
quintic polynomial and conclude it is unsolvable, and then turn around and prove
the Fundamental Theorem of Algebra using the same tools (Galois Theory). Life
would be a lot less complicated if math was just this terrible, disgusting subject,
but it just is not.

[^build]: Okay, I'm not the one building the robots, sorry for lying to you.
	But it sounds a lot cooler than "I wrote some crud API that might get
	deployed to prod eventually".

Even computer science is cool. Functional paradigms are cool, Rust is cool,
systems and comparch are cool (but massive pains in the ass). And I really do
want to learn more about each of these subjects, even though going to college for
computer science *is the reason* I'm so exhausted.

But the simple arithmetic of time means if I want to spend time learning how to
write, draw, and push around comically large weights in the gym, something has
to give. It's not going to be sleep, I'm already barely functioning as it is.
It's not going to be socialization or video games because that's the only thing
that unfries my brain. I do need to read less manga, but that won't be enough.
By sheer process of elimination... it's got to be math or computer science.[^hn]
As unfortunate as it is, I really cannot justify learning category theory or the
Curry-Howard Isomorphism if it comes at the expense of writing the things I want
to write. If it comes at the expense of not taking myself seriously in any
domain besides mathematics.

[^hn]: Fortunately I can make a lot of time for myself by just not reading
	Hacker News and learning about random computer science topics that way.
	Unfortunately, the difficulty in life is energy management rather than
	time management, and to me reading Hacker News is unwinding.

	...yes, I am a nerd.

> Have you considered not spending three hours a day solving logic puzzles---

Hahahahaa that's a funny one.

Listen, I'm fully aware someone's going to say "you're just a college
sophomore. Why are you thinking so hard?" You're right. I haven't even truly
experienced the life of the working man. But at some point in my life, I will
be wondering what went wrong and start digging through the past like a
complicated stacktrace. And the stacktrace certainly will not have started now,
this summer, or even this year. It may have started when my stupid little brain
decided to prioritize getting better at multiplying over anything else. But I
can't do anything about that now, can I?

What I *can* do is at least *think about* how I'm going to steer off this
course of being constantly exhausted and only giving half-caring about my work.
I would rather try and adjust my life right now as the naive little idiot I am,
instead of waiting until I am a jaded old idiot wondering why I want to drive a
skewer through my eyeballs every time I see someone commit `.DS_Store` to a
goddamn Git repo. I may not have lived for very long, but it doesn't take a
genius with significant life experience to figure out that *churning out CRUD
APIs with a lifetime of precisely 0 seconds on prod* is not the ideal life.

And unfortunately, though the things I am learning are very cool, the habit of
"focusing on the right things" that studying computer science is building is
very not cool. One day, it will feel deeply wrong to do computer science, and
that is when it will make sense for me to immerse myself in computer science.
But for now, I need to decouple it from my increasing risk aversion and the future
where I sit in front of a screen and write ultimately inconsequential code for
eight hours a day for the rest of my life. Even if that inconsequential code makes me
a lot of money.

## Puzzles

> The silly puzzle man strikes again

This post is sectioned into three sections. The first is on my concerns about
where my life is headed. Then where I want my life to head. Now I will pat
myself on the back for not *completely blowing it*. If you have any
understanding of me or this blog --- an understanding that takes all of eight
seconds to achieve --- you know that I spend half my waking hours thinking
about logic puzzles. Well, I am glad to report that nothing can stand between
me and my puzzles, and I'm goddamn proud of it.

That's not to say my puzzle hobby came out unscathed. A year ago I had the
stamina to write [Alphabet Soup](https://puzz.dennisc.net/#alphabet-soup) and
my brain was fed puzzles zany enough to construct my [Fillomino
puzzles](https://puzz.dennisc.net/?tag=Fillomino). I like to think the last few
puzzles I've published are really good too, but you don't have to be very
observant to realize I've been exclusively using Killer Cages and Arrows for
the past *half year*. I no longer have the chutzpah to cook up a new variant
that'll take an entire week to resolve, and I think both me and the world are
worse off for it. Because if I don't wrap up a puzzle neatly, I get to wake up
to a million other responsibilities that need to be taken care of before I
finish the puzzle. The takeaway is that I need to make time for myself where
things just stop piling onto me, or more realistically, where I at least get to
pretend they don't exist for half a week.

I'll admit it. Right now I'm completely out of ideas. Nothing I've really
solved recently has inspired me, which is no fault of those incredible puzzles.
The last puzzle I posted was the result of a stupid idea I had while lifting
weights, and I just got really damn lucky that it worked out somehow. But you
know, I just set a puzzle less than a week ago. I think it makes sense to be
a little optimistic right now, even if I have no clue how I'm going to set
the next one.

> But what do puzzles have to do with writing stories---

*Everything*. I've written stories before, though looking back I'd like to beat
the life out of each one of them with a fireplace poker. And I write puzzles,
of course, that's at least an eighth of my personality. And even though one
seems like a purely logical endeavor, while the other is emotional, I believe
they speak to the same part of the soul. The part that hopes the ideas you want
to tell fit together, the part that screams in relief when it all just *works*.
The part that reaches for an easy conclusion, and the part that says Wait. You
can do better. There is a good way to finish this puzzle, to finish this story,
and it's somewhere in you, so you better start looking goddamnit.

So maybe I'm not completely screwed. I believe that the part of my soul that
writes puzzles can be rekindled to help me write stories again, and to nurture
that part of my soul I will proceed to spend the next 3 hours doing nothing but
puzzles---

## Takeaways

There are none.

I dunno, life is really complicated. If I knew what I was doing, if I could
coherently tell you what I wanted to do in the future, it wouldn't have taken
me three thousand words to do it.

There is a story I want to write that is still fresh in my head. Only time will
tell if it will be good, and above all, whether I have the courage and stamina
to go through with it. I want to end this on a hopeful note, so I'll say this:
please look forward to it sometime in the future.
