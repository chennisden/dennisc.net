---
title: Our Fall 2023 program has rejected 0 applicants
date: 2023-09-02
---

As you read the title, you might be thinking, "then what's the point of the application? If it isn't to weed people out, then what is it?"

Well, that is *kind of* the point. There's probably a selection bias happening here: because there's an application process and I'm very vocal about only accepting very self-motivated students who take things into their own hands, the only kind of people who would apply are the ones who would be accepted anyway.

But this selection bias, while a nice side effect of the application process, is not the main point. The point is to get people used to "shooting their shot", so to say. Instead of having your parents fill out a class registration form for you, and getting driven there, students now have to reach out to me, put in their own effort to get in the class, etc. In fact, it's *because* enough people have taken this initiative over the last year that I decided to teach a class again to begin with.
