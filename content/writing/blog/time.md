---
title: Time
date: 2021-12-06
---

_"Work expands to fill the time allotted to it." --- Parkinson's Law_

When you were a little kid your teachers and parents probably emphasized the value of hard work. So much so that the act of putting in effort itself was seen as virtuous, rather than working as a means to an end. Certainly motivation is a problem for most people, but chances are if you're reading the blog the opposite is true. I would be incredibly shocekd if someone unmotivated decided to spend their time reading _essays_ instead of, I don't know, playing BTD Battles 2 instead. Which would be fair enough, since I would do the same.

A lot of people do a lot of their work on a computer. If you're anything like me, you probably get distracted easily when you're working on a computer and encounter resistance. After all, the friction of checking your phone only needs to be less than the friction of the problem you're working on for you to check it. Checking your phone is easy and worthwhile work is hard, so it's really easy to waste a lot of time.

## Quotas

We think of work quotas as being "spend at least X hours on Y task." This turns out to be a very wasteful approach. It might be more helpful instead of set a maximum time limit instead; say, only spending up to two hours of free time a day on your work. Instead of thinking "I want to do vague X, how much time do I need?" think "How much can I squeeze out of T time?"

There's a reason the people best at what they do are minimalists. Novice writers think "How many pages can I stretch this out to?" The best think "How little space do I need to fit everything in?" Computer noobs think sexy is "Wow, look at how pretty my Windows screensaver is." Power users think "Wow, look at how much I can do with so few resources and such little clutter."

## Experiment

I'm going to go full circle and try something: a screentime limit. Here are the details:

- At home I can only use the computer two hours a day. Homework counts (incentive for me to do it faster).
- Using my computer at school doesn't count, since my butt is already in a chair, it would be too hard to count, and I actually spend my time at school programming instead of playing games (mostly).
- I'm recording time on little slips of paper. I think it'd be ironic if I wrote scripts on the computer to prevent myself from using it. Also if something critical happens (read: the English essay I don't give a shit about is due when I get to class and it's 5 AM in the morning), I want to be able to use the computer. (Or if I'm just transcribing an essay from paper to computer, I really don't care if I go over for that.)

In principle I think you could omit the general ban altogether and just limit your work. After all, the point of this restriction is to make work more efficient, not because the computer is a boogeyman that will kill you if you stare into it for too long. That being said, I still think a general computer limitation is better:

- It's harder to get distracted. Especially for power users like me, I can just press `$Mod+d` to open `dmenu`, then run `steam`, and enter into some game from there. It's not as easy to put down my pencil, walk over to my computer, deal with the guilt of committing not to work midway through my work, and also commit to wasting my two hours of computer time. It's also hard to switch back to working, so it's less likely I fool myself into thinking "I'll just finish it later, how hard could it be to switch to Workspace 1 where my Vim terminal is open?"
- Health. This really requires no explanation, it's just common sense.

Note this only works for academics --- limiting your exercise time is not likely to work the same way, even though in principle some people do need stop exercising as much (injured people. I learned that the hard way.) This is because when you're seriously studying there's only one pace --- a full-on sprint. Whereas when you're exercising there are long days, hard days, recovery days, etc. all with different paces.

## Sucks

There are some things that really suck about limitations like this though, at least for me. They are somewhat specific so someone else trying this probably won't face these.

- **Writing.** There are some times when I can think much faster than my hand to move. This problem is mostly solved when I'm typing because I can type really fast and also am an absolute Vim[^vim] lord. This is why I always do revisions by typing, because I can basically think on the fly and translate it to words. (Though forcing me to write on paper has gotten me to write more, even if it takes longer.)
  [^vim]: I actually kind of suck, I still don't use hjkl in Vim (though I do know how to use it in programs with vim-like keybindings, thank you very much).
- **EdPuzzles.** Imagine if your teacher forced you to watch an unskippable ad, except instead of being 30 seconds long it's 30 minutes long. And instead of being about Google's new phone it's about the Civil War. I guess I can use it to get some gaming time in?
- **Music.** Although I already know how I'm going to handle this: I'll just use the TV to play whatever song on YouTube while I'm working out. (Obviously it won't count as "computer time.")

These really won't be issues, I'm already thinking of them as features and not as bugs.

I would say on the whole that this experiment is probably a good idea. Part of me is slightly embarrassed that I'm publicly admitting I use the computer so much that I need to give myself a screentime restriction, grade-school style, but in the end I think it would be more embarrassing _not_ to acknowledge this problem.
