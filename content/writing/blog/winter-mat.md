---
title: The upcoming Winter MAT
date: 2022-07-29
---

Nothing I am about to say is official or even solidified internally, but Math Advance has plans to hold a Winter MAT sometime after winter break this upcoming school year. I want to explain what the Winter MAT is, what its goals are, and how it differs from the Summer MAT.

In summary, the Winter MAT is an easier version of the MAT compared to the Summer MAT. On average you could say it's similar to the Summer MAT shifted one problem down, although the introductory problems don't have that much room to get easier.

The Winter MAT is meant to be an introductory contest. It is intended to fill a similar niche as the Mandelbrot before it was discontinued. As thus, there will be a minimal focus on competitiveness, which means we will not need to focus on breaking ties. Prizes (if any) and recognition will be cutoff-based, not ranking based, though we will obviously use contest statistics to determine said cutoffs. This removes the need for tiebreakers entirely, so in the interest of logistics (shorter contests are easier to run), we'll be dropping the tiebreaker set from the Winter MAT. (It will still stay for the Summer.)

As part of our outreach efforts, some Math Advance members are thinking about holding the Winter MAT at school or other local testing centers. This is because taking contests online is a draining experience, even if it isn't as long as other contests. One of my favorite parts of math contests was discussing problems immediately after tests were collected after each round. COVID has robbed this experience from us for the last few years, and I'd like to try and get it back.

This year we made the Summer MAT significantly easier, particularly for the last problem in each set. While we don't ever intend to return to 2021 levels of difficulty, you can expect a spike next year in the final problems in each set, and a minor increase for 2/5/8 as well. This is for mostly pragmatic reasons: we have a lot of really good hard problems that we've been waiting to use. Part of the reason we're hosting the Winter MAT is to make hosting harder MATs in the summer more feasible: if we already have a dedicated beginner contest, then the Summer MAT does not need to be as accessible anymore and can cater more to stronger contestants.
