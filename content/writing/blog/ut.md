---
title: Visiting UT Austin
date: 2023-03-16
---

Yesterday I visited the University of Austin at Texas. I haven't actually written anything about college apps yet (I have a couple of off-the-cuff remarks from long ago written down in my notebook but not typed up), but for this story to make sense, I'll have to reveal a couple of college results. So here goes.

## College decisions

- REA Princeton => Defer
- EA UT Austin + Turing => Accept!
- RD MIT/CalTech => Reject

I think --- or perhaps, thought --- I have the kind of personality that aligns itself with MIT/CalTech best. Now I am rethinking this, and this is only partially me malding and seething over the rejections, because the reasoning against MIT/CalTech applies to some other schools (most notably CMU).

For people who don't know, UT Austin is a Texas state school that is really good at computer science for undergrad (top 10 on US News, or whatever other metric you want to use). Turing is an honors program that selects less than 100 of the incoming freshman CS class, and this is very biased, but I think it's really good.[^stats] There's a tight-knit community already forming within (some subset of) the admitted class of 2027, and apparently Dr. Lin (the director of the Turing program) has dropped bombshells like "The difference between an Ivy and Turing is a Lambo, pick the Lambo". I recommend that the class of '24 applies to this program!

[^stats]: UT Austin (not even Turing) is among the top 5 schools recruited by Jane Street, and JS pays Turing for the privilege of advertising itself to the students. Also 75% of freshmen get an internship in the summer (or some ridiculous number).

Anyway, the point of this post isn't about college decisions, it's just about a fun visit.

## Visit

We left WARP for the airport at about 12 PM. Looking back, the cohort of us who wanted to travel should have requested an earlier group, but until then I wasn't even aware there were people with ridiculously late flights (and were interested in coming to Austin). But anyway, I vaguely remembered (and fortunately, my memory was correct) that we had to catch the bus on route 20, and as luck would have it, we caught a bus right as it was about to leave.

I didn't have enough coins to pay for all 5 of us, so the bus driver just said "forget it" and let us on the bus after I inserted some number of dollar bills (probably because we were holding up the line). The bus took much longer to get to Deen-Keaton than I would have hoped (45 minutes, as opposed to ~25 on Uber), but since we didn't have to wait for an Uber at the airport it was probably okay.

After scrutinizing Google Maps for the entire bus ride to know when we were supposed to get off, we found ourselves at the perimeter of UT Austin, which means that it was the intersection of UT and downtown. None of us had lunch, so the first thing we did was get In-N-Out. Three of us were European and never had In-N-Out before, and suffice it to say, their entire worldview got shattered.

Then we walked around campus with the limited time we had left and took a couple of pictures near weird structures. We went past Moody, the biotech(?) and chemistry sections, and saw the sign for the Cockrell School of Engineering. Funnily enough, I was *not* able to find the Computer Science building, which is ostensibly what I visited for in the first place! (If I had more time, I definitely would have gone looking for it.)

Impressions of the campus:

- Seems lively! Integrated with downtown, lots of cool stores and stuff to do in Austin. Also there is an In-N-Out right outside of campus, which is very important and awesome.
- The architecture is on point. Unlike Northwestern, the campus actually looks *consistent*; every building has the same vibe, color palette, and materials. And unlike Harvard, it's actually really pretty.
- I'm concerned about the lack of trails that I saw (which maybe is a problem with how little we explored). Google/Reddit says that there's stuff nearby, but you actually have to run pretty far to get there or take the bus, neither of which is a great choice.

Everyone else had a flight at ~6 PM while my flight was at ~8 PM (or it was, until it got delayed a stupid amount), so we headed back pretty early and had to get to the airport fast. The bus was too slow, so I decided to Uber instead, but Uber was being stupid and kept reserving taxis at 17:45 when I clearly inputted 15:45, **twice**. When I realized, I went "fuckfuckfuck" and just reserved an Uber to come pick us up, NOW. Our driver was initially going pretty slowly but once we texted him that we had flights to catch, he put in the full stops to make sure we got to the airport as fast as possible.

Then when I passed through TSA Pre, I got mistaken for a SXSW attendee, then correctly identified as class of '23. Right as I was about to leave I said "Longhorns" and the agent was like "that's what we're talking about". Everyone in Austin (and even Woodcreek, where WARP was held --- which is quite far from Austin proper!) seems to be very proud of their flagship university, which is a far cry from what happens in California.

## Airport

FUCK SOUTHWEST.

So "because of weather" (which is somewhat reasonable, fine, I guess), the first leg of my flight got delayed by an hour. Then Southwest put my layover in Las Vegas instead of Dallas, which seemed reasonable enough, and initially it would've taken off at 8, as originally planned. Then it too got delayed until 9. Trouble is, the second leg of my flight decided to come on time, so now I had to catch a flight that left at 5:25 AM instead. Well, that's great!

It would've been OK if Southwest actually, y'know, provided accommodations given that I paid for a continuous-ish flight from AUS to SFO, but OK! Apparently they do not provide accommodations "because the delays were solely due to weather", which accounts for... pretty much every delay? Never mind what the customer is supposed to do about this situation --- either sleep on the floor of the airport or pull an all-nighter and die of sleep deprivation because Southwest ALSO decided to change my boarding group from A to C, which means that basically the only seat left was a middle seat next to a guy who... did not leave a lot of room left for me, let's just say. (My other neighbor decided to try and steal my armrest --- the only armrest I had because my other neighbor couldn't fit without putting the armrest separating us up --- until I just laid down the law and put my arm there.)

So now I am stuck in Las Vegas, after pulling two consecutive all-nighters, with no food in my stomach and shitty water fountain water. I'm not happy about Southwest's response to this situation, but as for the situation itself, it's pretty acceptable. This is the risk I took when I decided to book a late flight in order to visit UT Austin, and I think it was a risk worth taking --- the flight part just panned out really poorly for me this time. The only lesson for me to learn is to book flights a little earlier because they will probably get delayed and certainly will not leave early.

Overall it was a fun trip worth taking, and a nice way for me to close out WARP by hanging out with the boys even after we left camp premises. Scout mindset.
