---
title: All New Year's resolutions come to an end
date: 2023-04-11
desc: The end of my puzzle-writing goals.
---

At the start of the year I set some puzzle-writing goals. Well, I've technically met them all, and as usual for a New Year's resolution, now is about time to call it quits.

I've figured out the niches of sudoku I like to set: arithmetic, sets, and geometry. (Though I still don't want to touch anti-knight with a 10-foot long pole.) Since the role of these goals was to find my niche, in some sense, I've succeeded in my meta-goal, even if I'm technically giving up on my resolution.
