---
title: Dan Gabor 2022
date: 2022-03-07
---

Last Saturday I ran the 1600m and the 800m in our home meet, the [Dan Gabor Invitational](https://diablotiming.com/results/2022-03-05/), and summarily proceeded to run absolutely terrible (i.e. worse than median).

## 1600m

**5:44.80**, PRed over my Freshman year Dan Gabor time (by one second!)

The 1600m was the first event most of the distance runners were racing. My heat started at 5 in the afternoon, but we came too early --- we arrived at 3, when we only need 45 minutes for warmups. So we lounged around and did nothing until 3:50, when we began warmups.

Warmups also began too early, we had literally 30 minutes between the end of warmups and when our heat got _called_ to race. So that was 30 minutes we had to basically fill up with random strides. But I think I was in pretty good condition before the race.

In the 1600 (and the 800) we started in a double waterfall formation. The inner waterfall can merge immediately but the outer waterfall has to merge after the first bend. I think it's better to be in the inner waterfall so you can see everyone else, but then you might get passed/blocked early on. I was in the outer waterfall.

I started out pretty strong during the first hundred meters, but got flat tired as someone stepped on my shoe AND drove their spike into my heel. The hole in my foot didn't hurt as much during the mile (adrenaline!), but would really suck for cooldowns/warmup between the 1600m and 800m. The flat tire made it a lot harder to kick in the end since I couldn't sprint effectively. Plus, some poor tactical moves in the second half of the second lap and the third lap also contributed to a slowish time.

Goal next time: **5:35**.

## 800m

**2:29.38**, PR by a lot (did not get a good 800m race before this)

After the race the pain in my foot started to kick in, to the point where I had to hobble towards our team tent. I couldn't cool down properly since I could barely walk, so I decided to sit down and eat some food to replenish my energy.We did nothing for about an hour and in the interim I got a bandaid on my foot.

Afterwards we did some more warmups (not as long as the usual pre-race warmups) and this time we finished right before my heat got called to the bullpen. (So we started a bit late then, especially since we had to cut the warmup run short.) It was cold as hell when I was waiting in the bullpen, and I honestly regret not bringing my jacket there.

Again, I was in the outer waterfall, and I made sure to position myself right at the front of lane 5 this time around. I relied on adrenaline to make my pain "go away". Funnily enough this is the third race I've relied on adrenaline before a race. Some part of me would always hurt enough to make it hard to walk and right before the race I just think "it will be fine the pain will go away".

I started the 800m near the front and was doing quite well for the first 600m. A breakdown thereof: when I closed the first curve I was in 4th and right behind 3rd. The top 3 in each heat get a T-shirt and I was thinking, "T-shirt T-shirt gotta go fast". So I kept running fast without slowing down my pace from the beginning (I always start pretty fast, probably too fast) and passed him at around the 300m mark. Then it was open season for the next 200m, I passed 2nd place pretty quickly too and was pretty far behind 1st but also pretty far ahead of everyone else. Unfortunately between 500m-600m I could feel that I was slowing down and started to go a bit more conservatively. Here 3rd place passed me and I became 3rd place again, and in the final 200m I just ran out of gas. Then I got passed by 2 more people and audibly screamed (not that you can scream very loudly when you're running) "No!" because I knew I wasn't passing them after that/getting the T-shirt.

I don't even think I went out too fast really, just that I didn't have the endurance/mental strength to keep up for the last 200m. So I should start taking tempos & repeats more seriously and pushing myself to the point of exhaustion there (not that I shouldn't have before).

Goal next time: **2:27**
