---
title: More on WARP
date: 2023-03-15
desc: A couple more thoughts and details about WARP I forgot in my earlier post.
---

- On Day 0 my roommate used a sharpie to draw smiley faces on everyone's hand.
- WARP has a distinct lack of consequences for basically anything. People leave their trash strewn around the commons (a lot more on this later), nothing happens if you miss class, and generally no one gives a shit what you do.
- I'm more introverted than I expected to be. I skipped a lot of afternoon/some evening activities to recharge my batteries, preferring instead to interact with no one or do light interactions with a few people (generally just "chilling").
- I tend to find a group of people in the first few days and after that, I'm mostly just with them. Fortunately this time the group I chose was gym rats 💪
- On the break day (which already had a ton of physical activity), Max, Bryan, and I did two archery contests where the loser had to do 50 pushups (for each loss). Guess who ended up doing 100...

## Cleanliness

*This will be much angrier than my usual post --- even my usual angry post. If you don't want to deal with that, you've been warned.*

Jesus. Fucking. Christ.

How hard is it to just clean up after yourself?

![Trash at WARP. Yeah, it's really bad.](https://media.discordapp.net/attachments/1077038148112093304/1085101221964152832/3CCB1BC4-9FFA-4147-9BC8-8FA8F70740C4.jpg?width=513&height=684)

Yeah I get it, you're too rational to clean this shit up. You choose to defect rather than cooperate and let some other poor chap clean up in the mess, and in the same day, you manage to mess the entire commons up again. I'm honestly impressed.

If you're reading this, and you *know* that you're one of the people who regularly contributed to this, then know that I think you're generally a very cool person --- everyone at WARP is --- but please, make it a habit to clean up after yourself. Please make "leaving the place better than I found it" an actual *value* of yours, even if it's 2AM or whatever.[^2am] I know no one is doing this out of malice, but if you "just forgot" to clean up, it's because you just don't care enough. I know, for a fact, that you learned a ton of skills to train habits at this camp. Put the "Applied" in "Applied Rationality": please make this a habit you decide to train because it is really important.

[^2am]: And if you're too tired at 2AM to fix your mess, consider sleeping earlier.

Because how the fuck are you going to solve hard problems like AI alignment if you can't even solve the mess in the commons?
