---
title: so it ends
date: 2023-06-07
---

well, I've graduated.

## may 30: last day of school

May 30th was a Tuesday. Incidentally, it also was the last day I came to school.[^mvc] We have finals on Wednesday, Thursday, and Friday, but I don't have anything going on in any of my classes (I don't even have classes on Friday), and my *teachers* aren't even showing up (mostly), so I might as well not show up either.

[^mvc]: The only reason I came at all was because there were multivariable presentations (including my own on that day). The only reason I attended these presentations at all was because I didn't want to do 24 project reviews.

A lot of my senior friends are leaving for vacation right as the school year ends, which really sucks since I decided to leave July instead since I didn't want to leave immediately. So realistically the last time I'll see them until winter break is graduation.

But honestly, sitting in a classroom and doing nothing for two hours would be a bit sad, and I don't actually care enough to come to school just to talk to my classmates.

The noteworthy part is that no one really got hit by the fact it was all about to end. It's similar to college apps: you expect to be super nervous about it the entire time, but in the end you just can't be bothered to care anymore. Sure, there probably are people who really loved high school and don't want to leave, but for me it was just [a slightly enjoyable job](https://guya.cubari.moe/read/manga/Oshi-no-Ko/39/9/).

There still are a couple of things left to do, like graduation, grad night, and my friend's grad party. (As I actually type this up all three have happened. They were fun, but as I expected, there was no sense of finality.) But now that I've finished every assignment from high school, I can "start fresh". I don't have to worry about stupid assignments or checking my high school email or truancy letters (which I have 3 of) or whatever. There's a lot of things I plan to start doing using the momentum of the transition from high school to college, some of which I'll declare publicly even though I know I'll fail most of them:

- Set a strict sleep schedule at like 10:00 PM or something. It'll get really bad at college so I'm giving it room to get worse to like, 11:30 PM which is still not *horrible*.
- Set an instructionless puzzle by the end of the summer. (This'll be hard if the only ideas I get are Fillomino Chaos Constructions. They *could* work as instructionless, but if I set another puzzle with squares and circles and the example just *so happens to align with FCC*, then it'll be really obvious.)
- Finish my Git host (or at least have the MVP done)
- Pretend to take some MIT/Harvard courses at an accelerated rate. I want to get through the undergrad math curriculum (should've started a LOT earlier) and probably be a little ahead in CS too. I might be in SCS, but I'm really a fake CS major. Being good at math is much more important to me, though obviously I'll also be a tryhard when it comes to CS.
- Stop being a Rust one-trick (i.e. stop sucking at C and learn Haskell, everything else kinda sucks)
- Hit the gym every day and get that 6 pack (and non-stickman arms). This is unironically the most important

Summer represents a time with minimal obligations, which means maximal freedom for self-improvement. I already feel far behind of where I could've been and should've been if I was more aggressive in learning before, so I gotta play catchup now.

On that note, I should probably get the logistics ball for Summer MAT 2023 rolling, both sponsorship and web-hosting wise.

I'm not so concerned about looking back at the past right now. Yeah, I missed a lot of opportunities and experiences in high school (some of which is my fault, and some of which is due to external circumstances), but the future looks so promising that I don't really care anymore.

## may 31: so it all comes crashing down

Only on the Dennis Chen blog will you get a tech support story in my graduation post. Alas, when my server fries itself and my websites are all down, everything else kinda has to take a back seat for a bit.

Context: A week ago, I bought a Raspberry Pi 4 Model B to host a Minecraft server for the cross country guys, and when I was done with it I was going to bring it to CMU for it to act as our webserver.

So as I was finishing my last ever high school assignment and messing around, I noticed that puzz.dennisc.net was not connecting. Huh, weird. I was upstairs and didn't want to move downstairs to where the server was, so I just tried SSHing into the server to check it was up. No dice. How about pinging it?

> Route unreachable

Well FUCK. That means my server doesn't just have connectivity issues, it's literally powered off. So I quickly redirect my DNS servers to my cloud server for now and investigate in the meanwhile.

The red LED on the Pi (the one that denotes power) is on. The RPI doesn't have a way to force a graceful shutdown, so after confirming the device wasn't booted with an HDMI cable and a monitor, I just unplugged the power.

Then, I kept experimenting with reboots, using different SD cards with different operating systems just to rule out the SD card as a potential problem. No matter what, it wouldn't boot --- not even into a bootloader to tell you, "you fucked up man". This was really bad. I didn't want to spent the first day of vacation doing sysadmin stuff, but it looked like this problem wasn't going to resolve itself so easily.

Then I started looking stuff up. Maybe the bootloader killed itself for no reason. The forums say that if the Pi can't boot, the green LED *should* flash 4 times in a metronomic rhythm.  So I remove the SD card, connect the power, and got just one quick flash --- which meant the bootloader was dead. So the last thing I try is running the repair OS --- for a potential problem that is incredibly rare and has no reason to happen --- and again, no dice.

At this point the machine is probably totaled. Irredeemable. (Note from the future: it was. I returned it.) Fortunately, Best Buy has a 15 day return policy, and the Pi decided to kill itself for no reason much before the deadline. So I'm returning it tomorrow and hopefully I get a refund.

In the meantime, I learn that CMU gives a public address to every machine connected via Ethernet (how do they have so many public IPs, one wonders?) Also, when we're not running any time-sensitive events like the MAT, it'd probably be OK if something messed up and the server needed to be taken down for a couple of hours.

The original plan (after using the Pi as a Minecraft server with the cross country kids) was to have one machine at home and another at CMU. But I just discovered that A records don't allow for fallbacks, so there wouldn't be a ton of point in maintaining a far away machine. So, possibly contingent on getting Alpine Linux to run on my Orange Pi 3 LTS, I'm probably not going to buy another single-board chip. I just don't see the need for it.

Speaking of trying to get the Orange Pi to run Alpine: by god it is such an annoying, difficult, confusing, and under-documented process. The guides for getting *any* Allwinner[^allwinner] board to run Alpine are at least 5 years old, and none are specific to both Alpine and the 3 LTS. I've tried a few times before and no dice. The process I'm roughly trying is:

[^allwinner]: This is the Orange Pi 3 LTS's processor.

- compile u-boot and burn it to a micro SD card's first 1024 bytes
- create a boot partition of filetype ext4, toggle bootable, run mkfs.ext4
- extract the tarball of Alpine for Generic ARM into that partition (using mount)

The Pi refuses to boot when I insert such an SD Card, or sometimes in the past it would just boot the built-in Android ROM. I have fuckall idea what's happening, because I don't own a USB to TTL serial cable, but (note from the future) hopefully it'll come soon.

Fortunately there is an Orange Pi Discord, and three months ago someone tried to do the exact same thing I'm doing right now. So hopefully they succeeded and I can ask them what they did, install Alpine myself, and actually document the goddamn process.

Miscellaneous events today:

- Asian moms pulled up to our house and I sequestered myself in my room
- I solved KNT's ridiculous 25 letter ciphered Japanese Sums puzzles
- recently I felt kinda chubby and that's really bad (I can't *see* the difference in the mirror but I can *feel* it). **Note from the future:** I started running and swimming with the cross country guys so now this problem is fixed.

Well, hopefully I resolve my stupid self-hosting issues by tomorrow and actually start working on stuff I care about. (Note from the future: Hahahahahahahahahaha no. That did not happen.)

## june 1: the final ditch

This is the last day I ever get to ditch high school. And of course, that is exactly what I did.

I was planning on working on personal CS Projects, but this morning, CMU's Discrete Math Primer summer course came out. So instead, I spent much of the morning working through the DMP, and got through about 1/3 of it before grad rehearsal.

Grad rehearsal was so pointless. I don't know why I even came; it's not like anyone checked. Guys, please don't drag us into the sweltering heat just to ramble on about stuff you could've just sent in an email.

After I came back, I did some more DMP and went on a run. Then, I had an idea for my new puzzle site: I could add optional `hints` and `example` tags to the puzzle struct (note from the future: `example` became one of the potential links), so I could display hints and examples like in my old puzzle site.

Then I went to bed around 11:30 tonight. Not as early as I'd like, but tomorrow is grad night and it's going to get so much worse then. (Technically, I won't even go to bed tomorrow since I get back at 1:00 AM!)

## june 2: so it ends

This morning, the CS placement exams came out. So I finished DMP then did both of the exams. While I unfortunately failed to ace both the exams (note from the future: the one problem I missed probably has an incorrect answer since **no one** has aced this really easy 10 question exam yet), I did do good enough to not get thrown out of Imperative, which I have the AP credit for anyway.

Today, we also finally went to Best Buy to return the broken RPI. Unfortunately Best Buy doesn't carry USB to TTL serial cables, a niche debugging tool for stuff that has exposed GPIO pins (basically just single-board chips like the Pi) that I need to figure out how to run Alpine on the Orange Pi 3 LTS. Then I went to TJMaxx and bought some black shorts, because I desperately need more. (Yes, my fit is all black. No, you do not get to laugh.) Nothing of note happened after that until graduation.

Graduation, to put it bluntly, was a mess. We stood around doing nothing for two hours, and I hate crowds and enclosed spaces because you have to pretend the 3 things you can even do are interesting as you cycle between them.

During the grad speeches, I was thinking, "shut up about COVID please I've heard about it 5 other times", "wow you are a disaster child", and "Zzzz". The final one isn't entirely true: during the valedictorian speech my brain was working on overdrive. Not only did I find the ruleset I wanted to use for Fillomino Chaos Construction #2, but I even thought of a breakin. So as the speaker was talking, I was thinking "please let the wifi just connect so I can set the breakin with Penpa before I forget." (I got the wifi.)

Then after awards (for god-knows what) were presented, it was time to read the names of all. 700. seniors. It took way too long, but it honestly wasn't bad because I had a lot of friends to cheer on throughout the whole ceremony. Not that there's anything cheerworthy about walking on a stage and receiving a participation trophy --- well, not even that, because the diploma itself wasn't even there on time, just the diploma *cases* --- but for a brief moment, it was fun to pretend otherwise. Celebrating each other, even if it's for stupid stuff, is honestly not that bad. Especially because when someone does something genuinely impressive, most of the time no one else understands what they actually did. So the silver lining of the pointless and ridiculously long graduation ceremony was that at least we got to celebrate each other and fully understand *why*.

That still doesn't mean I think graduation isn't stupid, and I still wish I got to skip it. But since I had to be there anyway, I made sure to enjoy it a little.

It was only during the afterparty that it really hit home: I'm not seeing these people ever again. It's not like I'm crying about it or super depressed, but it did strike a reflective chord in me. The people I was really close to, I'll meet them one way or another in college or further in the future. Some people I straight up had no idea existed, so I honestly don't care that much. It's the people in between that leave me torn: it was nice knowing them, but we're not nearly close enough to meet up in the summer or in college.

Some part of me regrets not getting to know these people better. I genuinely like these people, and vice versa. But come on, a fake extrovert like me only has enough energy to be social every so often, and I'm not going to sacrifice my uninterrupted 8-hour puzzle writing streaks just to go *partying* or something.

I do care about these people. Just not enough to go out of my way to find their contact info and invite them to something, especially since I can already fill my schedule with the people I know and the things I want to get done this summer. And that's kind of a sobering thought.

But of course far more important than anything else today, I figured out what the next few puzzles I'm going to set are going to be like, and I can't wait to actually set them. I'm going to continue the Fillomino Chaos Construction series and I already have ideas for three total puzzles. (Note from the future: I finished writing FCC #2.) So if you liked FCC #1, boy I got some good news for you.

## aftermath

As I predicted, I didn't really care about graduation. Instead my priorities were:

- CMU (both summerwork and getting to know people)
- writing puzzles (I somehow managed to drag myself away from the Penpa tab when it was time to meet up with the guys)
- meeting up with the cross country guys

Also algebraic data types and Haskell. I really need to learn Haskell and FP.

Out of my priorities, I did a lot more hanging out than I expected, and less math/*strength* exercises than I should've.

I finished up Fillomino Chaos Construction #2 and sent it to KNT, and as he testsolved I discovered 2 errors, which is an entirely new record. I only fucked up twice when I was setting the puzzles, and even better, it was actually pretty easy to fix the logical leaps I made and make the steps actually rigorous. Man I am such a genius, maybe someday I'll even figure out how to not send broken puzzles in the first place.

I also went to a graduation party of a friend who goes to a different school. The host had a Kahoot about herself (only God knows why), and there were questions like "what is my favorite flower". And everyone else is getting these right, and I'm thinking "how the fuck am I supposed to know what *your* favorite color is, I don't even know what *mine* is". Needless to say, I placed dead last. Also I need more meat, but it was super fun despite there being basically no meat in the menu.

OK, now it's midnight and technically June 7, so I should really head to bed. Schedule for tomorrow is aggressively do math, make the build script for dennisc.net *much* faster, send some sponsorship emails, and maybe find a CS course to study.
