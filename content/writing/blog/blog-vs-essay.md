---
title: The difference between my blog posts and essays
date: 2022-07-31
---

I've already [written about this before](/writing/blog/separation), so forgive me for treading over old grounds. But this is more a reminder for myself than a public explanation.

Blog posts are timely --- they largely rely on context from events around that time. As of now, some of my posts reference [my anime taste in 2021](/writing/blog/2021-recs), [the 2021 MAT](/writing/blog/mat-remarks), [an English essay I was really upset about having to write](/writing/blogs/paragraphs), [a track meet](/writing/blog/gabor-2022), [USAMO 2022](/writing/blog/i-made-it), and [MAST's indefinite hiatus](/writing/blog/mast-hiatus).[^more] This doesn't necessarily apply for [really stupid stuff](/writing/blog/best-colors), but hey, a man's got to shitpost.

[^more]: I could have included more but chose not to for space.

Essays are more timeless and usually don't need the context of a specific event to be understood. That's why [To parents: stop emailing me](writing/essays/dont-email) ultimately became an essay even though it doesn't feel like one. In retrospect, maybe these posts about the distinction between my blog and essays should be in the essays section too.[^timely]

[^timely]: The reason I put them in blog posts is because I think my mind is prone to changing about this distinction. At the very least, I'm explicitly discovering criteria that I vaguely/subconsciously used before.

I don't make this distinction for tech posts because all tech posts become obsolete over time, though the rate of obsolescence differs (for instance, I along with the rest of the world stopped caring about [Gemini](/writing/tech/gemini), but [the morally correct way to make a static website](/writing/tech/static-website) will likely remain the same for the next decade at least).
