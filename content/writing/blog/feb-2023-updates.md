---
title: February 2023 Updates
date: 2023-02-16
---

It's been quite a hectic month and some pretty cool things have happened. I've also got a couple of projects planned.

## Puzzle Goals

It's midway through February and I think this challenge is going to be pretty hard because of what I've enjoyed setting lately (Killer Cages). As a refresher, the original goal was to use 4 new variants, whether classic or my own invention. I'm weakly considering changing it to 4 *common* variants that I haven't used before, like Quads or Sandwich. But either way you count it, I'm behind.

On January 31st I sniped myself by setting Genome: I used Renban Lines and Kropki, two easy constraints. (Hey, it's good to make the setting challenge a bit hard for myself!) And right now, I've mostly been setting Killer Cage puzzles along with a few Star Battles, so I'm not exactly treading a lot of new ground variant-wise here. But honestly, I'm not too concerned about failing, because I can just arbitrarily extend the challenge or move it to a different month or something. (Not like I've figured out a challenge for every month anyway --- not even close...)

Under the most generous interpretation, I've set puzzles with two new variants: Diagonals (classic) and [X-Lines](/puzzles/x-lines/sudokupad). Right now I'm trying to set a Thermo Quad[^story], and Fortress, Extra Regions, and Sandwich also seem promising.

[^story]: Funny story: I transferred my puzzle to paper and then thought my logic was wrong. Turns out, I shifted one quad up a cell and another quad down a cell...

Oh, and check out [Class of '23](https://logic-masters.de/Raetselportal/Raetsel/zeigen.php?id=000CZD).

### Puzzle Book

For the puzzle book, I will probably include some classic sudoku and killer cage sudoku even though the focus is on "homemade variants". Two reasons: one, a lot of my puzzles use cages or at least arithmetic (and obviously all sudoku puzzles use sudoku, so classic is always good), and two, it helps pad the length of the book :)

Also, I'd like to think that I'd include Highway, Stairs, Class of '23, etc, because otherwise that means I've made no progress this entire month. (Well, that wouldn't be true, I invented X-Lines two days ago so now I have more flexibility.)

## Math things

Objectively speaking, I don't know much math. Most of my friends that do math have finished at least abstract algebra and real anaysis, and I have not really done either. So I plan to read a book on one of these --- probably linear algebra --- and type up a few notes. On the bright side, I think linear algebra has finally clicked for me. Turns out, that all I had to do was keep repeating to myself that matrices are linear functions from $\mathbb{R}^m \to \mathbb{R}^n$, that's all.

Math Advance has been in a lull lately, especially since our leadership is concentrated in the class of '23. I also need to collate Winter MAT results, hopefully that will happen soon. The Summer MAT is taking longer to start writing than usual (mid-February and still only initial drafting stages, as opposed to sending out serious drafts in December the last two years), so we need to get that going fairly quickly.

## Code stuff

### mapm

Yesterday I adjusted mapm to support multiple choice tests and today I released mapm 6.1.0, which has MC support. I also fixed the bug where `mapm preview` and `mapm preview-all` don't care if the problem doesn't satisfy the preview template's variable requirements.

As for how many people (or rather how few) people are using mapm, I think most people are uninterested in setting up mapm on their own computers, especially the people who are not good with computers (and math contest kids, by and large, are bad with computers). Even asking people to install a GUI would be a pretty tough ask, so I think I'm going to write a web host to import/export problems from instead.

The benefits of writing a web host besides increased adoption is that LaTeX in non-HTML GUIs is hell, and I don't really want to use Tauri for a GUI because that would be pretty cursed. I'll do the research (and maybe some of the work for) LaTeX in GUI but this will probably be after Glee is stable and the mapm web client is done.

## Glee

(This is mostly hardware stuff.)

I'm participating in Hack Club's Winter Wonderland, a hardware event that lasts for 10 days. Right now I need to get my Orange Pi set up in some trivial way, so that I can move on to getting the temperature/humidity sensor, establishing a static IP address/routing through a static address that hopefully our ISP is providing us (more research needed here), and migrating some less mission-critical projects, like mathadvance.org, dennisc.net --- mostly static websites --- to the physical server.

Glee itself will obviously take more than 10 days, but I might try to get something rudimentary set up during this 10 day span. Of course, I can and do intend to continue working on it even after the event.

## Crosswords

Just something miscellaneous that I wanted to add: crossword construction seems pretty interesting. Unlike Sudoku, whereI think I have a handle on what makes a good puzzle, I don't have the slightest clue about crosswords.[^symmetry]

[^symmetry]: Well, besides symmetry, but that's obvious.

For comparison, I started solving Sudokus pretty intensely for six weeks or so before I tried setting my first puzzle, and even then I had some experience prior to those six weeks.[^ctc] Whereas my crossword experience mostly consists of failing to solve them on airplanes and trying a couple these past few weeks.

[^ctc]: I think I watched a little bit of CTC my freshman year.

Well, I think that's all for now. See you next time!
