---
title: Extension to Fall 2023 application deadline
date: 2023-09-04
---

[Because my website was down for two days](/writing/tech/domain-renewal), I'm extending the application deadline for our [Fall 2023 course](/apply-2023.pdf) until Wednesday (Sep 6), 11:59 PM EST.
