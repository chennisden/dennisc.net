---
title: How to remember your SCOTUS justices
date: 2023-07-16
---

The Supreme Court plays a large role in the government, yet most people (likely including you) don't know who the justices are by heart. Here's an easy way to group them up:

- **Trump appointees**: Neil Gorsuch, Brett Kavanaugh, Amy Coney Barrett
- **Original conservatives** (i.e. not Trump appointees): *Chief Justice* Roberts, Clarence Thomas, Samuel Alito
- **Liberals**: Elena Kagan, Sonya Sotomayor, Ketanji Brown Jackson

It's also worth knowing that Gorsuch, Kavanaugh, ACB, and Jackson replaced Justices Antonin Scalia, Anthony Kennedy, Ruth Bader Ginsburg, and Steven Breyer, respectively. Jackson was a Biden nominee.
