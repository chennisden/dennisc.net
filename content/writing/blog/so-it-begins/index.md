---
title: ...and so it begins
date: 2023-08-30
---

## Final Day of Sojourn

As the title suggests, I spent a good chunk of time in California watching Alice in Borderland (which, by the way, is much better than Squid Game). A brief rundown of my summer:

- Went to China
- Actually did stuff in very hot places
- Got my wisdom teeth extracted in a place with reasonable weather
- Did not want to live (and did nothing) for two weeks
- Got back and got (mildly) sick
- Did not want to live (and saw nobody) for 4 or 5 days
- Saw people

I don't think a listicle of things done makes for interesting reading, but I'm at college now and barely remember any of it.

Vaguely, the way I remembered *feeling* was "ok well I just wanna get this over with and like be on my own". I didn't really have much to do, although at least on the last few days I started seeing people (my boy Sword Looting went on a trail run with me). But yeah it is nice to just, maybe have problematic decisions like staying up till 1 or weird shit like showering twice a day (I will **never** do the opposite) or even technically irresponsible but more agentic things like skipping shitty O-week events with friends. (I hate the word "agentic" with a passion but it actually worked perfectly here.)

Also I just decided "fuck it I ain't seeing anyone" on Thursday, the day before my flight. Probably a good decision so my social battery doesn't go to 0 before I even go to college.

## Day 0+1 (Locked Out)

Friday 4:00 AM I woke up for a 7:00 AM flight from Oakland. I don't know why we left so early.

Anyway, as a result of being lazy last night and my mom being convinced we had no time for me to do some last minute packing (she was wrong!) I was left without

- my Switch
- Secret Hitler
- Ethernet cables
- my passport(!!!)

Anyway the point is I am an idiot. And apparently now I am coming back for Fall Break because I really need some of these things (my passport) and really want some others (my Switch and Secret Hitler if I don't just cave and order it on Amazon).

After arriving we did some shopping at Costco, and I was too lazy to actually go and find a restaurant so we just ate Costco pizza and went to the hotel. The hotel was literally in the middle of nowhere, and to make things worse the people next to us were blasting TV really loudly at 1 AM, which made it hard to sleep. My movein time was 9:45 AM and I should've woken up around 8:30, but I got woken up at 7:30 ish (I am guessing here) by the fucking TV and was like "ok I never even fell asleep I can go back to bed".

Then I wake up at 9:55. At first I think my dad is playing a prank on me; no way I slept that long. But no, it is 9:55 AM and I am definitely late for my move-in time. "Well, crap", was not the first thing --- or even one of the things --- that I thought, because I knew (read: assumed) move-in times didn't really matter. I was right.

After moving in I kick my dad out because I like having friends. Then we eventually osmosised into a large ass group and it was really funny when I ran into him again because I forgot to take my blazer out of some luggage (and so I would've accidentally shipped it back oops). And I won't lie I forgot the rest of what happened but I vaguely think it involved the gym a little? Also I met up with a lot of people that day, so I didn't do the two things I wanted to:

- Sleep before 11 PM
- Study linalg

The one thing I do remember, though, was getting locked out of our dorm for 2 hours and sitting outside because our door key swiper thing ran out of battery?? And then watching Axler videos with my roommate while my other roommate slept through the loud-ass knocking on the door. Eventually the RA contacted campus police who opened our door and contacted maintenance to fix the lock for us. So we just permanently left it open before then.

Also I still owe people money and should pay them back eventually instead of being a bum.

## Day 2+3 (Bonding over O-Week by hating O-week)

One of my friends asked (rhetorically) "how do you even make friends during O-Week this shit is so boring". (Because it is. Pro tip if you go to CMU next year, skip almost everything because it sucks, go explore Pitt with your friends instead!!) Or maybe I asked it. Anyway, someone else answered "by shitting on O-week events with the people next to us". Honestly, he's not wrong.

The actual O-week events are by far the worst part of O-week. The president's welcoming address was boring and made me play Clash Royale for the first time in literal months (and my roommates too). The SCS speech was like, "bro why are you flexing 50% yield and stop I don't care about the details let me leeeeave." The floor event was like "OK let us now memorize names" (I don't care). And the Day 3 (everything before was Day 2) house meeting was like, "ok and now the RAs will socialize while you sit here and do nothing for two hours".

The worst part isn't that these O-week events are nominally mandatory. It is that everyone else goes there so you can't actually plan to hang out then, although thank jesus it is beginning to change. At least I had a few friends who were sane and didn't go to more house meetings from 3 to 5 or some bullshit?? And when you get there it's literally kindergarten. The lunch tables are too reminiscent of high school, and not in a good way. (Although that's just the atmosphere; the people I eat lunch with are amazing.) And we literally had to "fill time" so we could get "properly dismissed for lunch". Like bro I'm not some high school freshman. I can vote so please just let me stand up and leave.

We also had a math placement test. I didn't ace but did pretty well (as far as I know, maybe the best?), so it was ok. Better than I expected and worse than I hoped for.

I complain a lot here, but by and large the positives outweigh the negatives by far, especially since I skipped a lot of events (mostly to recharge before placement test, and because the placement test overlapped). I like CMU and I like the people here. But the most novel thing these two days is the O-week events, and also the least confidential, so it is what I focus on here.

Also my gym ratting + two showers a day regime is going well. I will probably stay consistent because I still study a LOT. Especially now the placement test is over.

## Day 4

I dunno if it was post-test nerves or something, but I could not sleep the entire night. So around 6 AM, I just gave up and went outside (and now I am absolutely exhausted). When I got back I just started doing work and puzzles, which somehow requires the least brainpower compared to actually socializing and stuff. (Goes to show that I'm still secretly an introvert despite how hard I try to gaslight myself otherwise.)

Then we tried to register for whitewater rafting in the outdoors event:

> Try to register for event
>
> Reg is open from Aug 22, 8 AM - Aug 22, 1 AM
>
> ???

Number 1 CS school for real. Now we just have to camp the website??

The link then got replaced under our noses and we only realized when it was too late, so my roommates + our adjacent friend group didn't get our first choice (water rafting). Plus my second choice (paintballing) disappeared too, so I went with my third choice, caving. But at that point you can't really get everyone to compromise on a third choice (most peoples' second choice was paintballing too), so we all got split up.

Also I went to the floor meeting (for a few minutes anyway, right as it ended) this afternoon. That was pretty out of character for me, but I got a free shirt from it so hooray?

At some point I figured I needed to clean our dorm, do laundry, and buy eggs (for breakfast). I would've done that but got too busy socializing (I need to be more responsible) and was going to do it at midnight but things happened.

## Day 5+6

After two days of not sleeping and a bunch of things going on, I just crashed.

Every time I went to a live-in camp, I would be super extroverted for the first few days, but at some point I'd just go "OK I'm done" and basically hole myself in my room working on my own stuff. (It could be worse; I could hole myself in my room doing nothing.) And these two days are when I went, "I'm done".

Day 5 wasn't too bad --- at least, I didn't realize what I'd done until Day 6 (which is when I am typing this section up). I did sleep a lot, eat breakfast in my room (instead of with other people --- my roommates were either asleep or gone?), skip the pool event I signed up for, and cut my gym session short because I was so tired, but it wasn't all going downhill. Yesterday I took the initiative to actually go grocery shopping and went to the Google field trip (which wasn't quite what I expected or hoped for it to be).

I did nearly fall asleep on the Google trip because it was a lot of "sit down and listen to panelists" and I was super sleep deprived.

Now for Day 6... I slept at 10 PM last night and woke up pretty late (8 AM ish)? I decide to skip CMU breakfast because CMU breakfast is dogshit. Unfortunately, I am not particularly experienced at cooking stuff nor particularly intelligent, and by the time I got back from grocery shopping on Day 5, the aforementioned Google trip was happening very soon. So in my haste to pack everything into the mini-fridge, I accidentally put the eggs in the freezer instead of cooling.

The eggs were fucked.

So instead of having something nice for breakfast, I was basically just subsisting on the bread and fruits I bought a while back. I had to chuck the eggs out as well. It seems small --- and honestly, it really is --- but I became irrationally upset for a few hours. I say became, but right now I'm still holed in my room playing BTD Battles 2. Orientation events might be a bad use of my time, but the difference between me skipping earlier and skipping now is that in the former case, I actively had better things to do. I was hanging out with people or at least going shopping, but now I am just a bum playing video games instead of socializing with other people.

The worst thing is, right now I am waiting on external events --- whether it be lunch, a (hopefully good) MMS result, or other people --- rather than relying on my own initiative to change anything or improve my mood. I know what I *need* to do, but I just can't be assed to do it. This feeling of learned helplessness is pretty awful.

Ok but whatever who cares right? Apparently I did "quite well" on MMS and need to "discuss options", which is distinctly different from "oh OK you can just choose to take 242 or not". Which means WE GETTING OUT AND WE GETTING CHOICE WOOOO

My roommate also got the same email so there'll be someone I know in my classes maybe. I dunno though because our choice on what to do next might differ.

## Day 7

Yeah, basically everything went as perfectly as it could have. I'm taking 0 math classes and 2 CS classes this fall which is nice, because most people take 3 core classes. So I can just chill this semester.

Also Carnegie Cup happened. We did puzzle hunt, the counselors running it dipped, so we dipped back to my room for a while too. The logic puzzle round was nice and I solo-solved it in ~10 minutes, the rest was between difficult to impossible for me.

A thought I had near the end of the night: I think there's a point where it's better to meet new people, and then past that point it's better to get to know the people you just met. I think we crossed that point by Day 4. Orientation should not be nearly this long, and it really should've just been speeches (I guess, because they're kind of mandatory) and the fun things like Pittsburgh Connect. (More on that later!)

## Day 8

I figured out what my second gen-ed will be today: 80-180 (Nature of Language). I know a couple of people in that class, and technically I'm on the waitlist but my advisor told me there were 9 open spots, so I'll basically be guaranteed to get in the class.

More importantly, today was Pittsburgh Connect. The idea is you either touch grass (go outside) or see cultural stuff (eg museums) to create, well, a *connection* with Pittsburgh. I chose to go caving and honestly it was so fun. And if you're in disbelief, yes, I actually went inside a cave. Like, *a real one*. I was surprised when I saw the details too.

We went to the upper two levels of the Laurel Caverns, and the actual caving was about 90 minutes long. (The bus ride was 2 hours each way.) I got really dirty on the trip, and I also got to experience *genuine* pitch black and near-silence in a cave. The highlight was the spring water though. Holy shit it tasted so good. Like genuinely, that shit changes lives. I swear I might've almost gotten high on just how good that water was.

I also started reading up on a little bit of Haskell, since I got into 15-150 (the functional programming class). I already know some big-picture things about Haskell and FP because I've just been reading about these ideas on and off, but it didn't really register that I'd be actually learning about FP this soon until yesterday. I'd heard about people on LinkedIn who got an Amazon/Jane Street/etc internship who took 150 in the fall but until now I thought those people were a myth and me doing the same thing would be a pipe dream. Apparently not.

## Day 9 (last day)

Man. I was so tired that I slept at 11 and when I decided it was time to wake up, it was already 9:30. Today the goal is to actually become adjusted for classes. Not that I can do that in one day, but I'll be doing what I can.

Today I decided to figure out my schedule (i.e. what I'd be doing every day) and also dealt with a bit of MAT logistics. (Sorry it's taken so long, guys!) And, surprising even myself, I decided to attend the final floor meeting because it's the last one.

Then I got dinner with some people and we went grocery shopping but there were no eggs. And so I didn't get anything, because the apples in Scotty's Market are $2.59. Not per pound. **EACH.** That is pretty fucked up. Scotty, you are one disaster of a mascot dog. Get your act together.

## ...and so it begins.

So I've had a couple of days of classes already (literally a couple: exactly two).

First thing's first: CMU stopped serving their shitty orientation food and put us on the meal plan proper. Actually, this happened a few days before (Day 7 if I remember correctly?) but I never thought it was really worth mentioning. The food is pretty good, I really like Rev Noodles. Meal blocks are still a scam though and there's no way I finish them all.

Speaking of eating, I've basically only had time to eat two meals a day. Part of this is because of scheduling shittiness; sometimes I only have enough time to eat brunch and dinner. But another part (and honestly most of it) is self-inflicted. More on that later.

My actual classes were mostly OK. Imperative was a little slow but surely it'll catch up. Functional programming was pretty interesting and I liked the first lecture better, but I almost collapsed during the first lecture. (More on this later, and this is related to my two meals a day situation too.) I don't want to write anything mean about my writing class so the less said the better. Hopefully Nature of Language shapes up to be interesting.

This is what my schedule looks like for this quarter:

<img src="real.png" alt = "Real schedule">

This is very chill. As I've said before, testing out means I had no math classes. But also taking zero math classes is one surefire path to brain rot, so I decided to sit in on a couple of classes and asked a friend to join me. Well, something awakened in his mind, and now I am sitting on three classes. This is my fake schedule (that reflects what I am actually doing):

<img src="fake.png" alt = "Fake schedule (including sit-ins)">

If I have to drop something I'll drop Complex Analysis first. (I didn't go on Monday and I was already fucked up from four back-to-backs.) Because Complex is more standard than Formal Logic (e.g. online resources and friends who know it are more common), it's also something I can afford to put off for a semester.

My fake classes are by far the most interesting. They aren't large ass lecture halls and the professors can actually individually see people (wow).

In Linear Algebra (which is covered with abstract fields and has multilinear transformations hopefully) we just went over fields in the first lecture. Mostly just review, but it was funny when Prof. Schimmerling was making an effort to assume as little prereq knowledge as possible, got to $\mathbb{F}_p$, and just thought for a second and gave up (i.e. he said "the field of integers modulo $p$"). I also am not sure whether he knows we're not actually registered students or not.

In Basic Logic (remains to be seen whether it will turn out to be truly basic... my guess is no) we covered "the view from 3000 feet up" and got a general overview of the difference between syntax and semantics, Godel's (in)completeness theorem, why the Peano axioms cause incompleteness, and what the heck induction actually is. He also gave some supplemental textbook recs for the course, which we later learned are on the syllabus. (The course uses no text.) And now we are on the Canvas and Piazza, which is pretty nice.

And this is why I get two meals a day some days... (and probably why I nearly fell asleep in FP lecture). Oh well!

Also, if you think my fake schedule is fucked up...

<img src="sfake.png" alt = "Most fucked up fake schedule">

Yeah I think I really awakened something.

Anyway, I'm still hanging out with friends and having fun. Life is fine even if I technically have 80 units on my fake schedule.
