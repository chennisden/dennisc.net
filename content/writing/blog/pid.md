---
title: Two questions on free modules over a PID
desc: Some interesting questions concerning free modules of infinite rank over a principal ideal domain.
date: 2024-09-08
---

While reading about free modules over a PID I had two natural questions come up. To provide some context, here are two standard facts:

1. every submodule of a free module over a PID is free, and the proof involves the well-ordering principle,
2. given $N \leq M$ where $M$ has finite rank, there exists a basis $\{m_1, \dots, m_k\}$ of $M$ and ring elements $r_1 \mid \dots \mid r_j$ such that $\{r_1 m_1, \dots, r_j m_j\}$ is a basis of $N$. (Of course $N$ and $M$ are over a PID.) The standard proof is over the Smith Normal Form and requires us to rely on the fact that the required row/column operations we do to perform a change of basis are finite.

## Relationship to the Axiom of Choice

[Relevant StackExchange](https://math.stackexchange.com/questions/4966748/does-every-submodule-of-a-free-module-under-a-pid-is-free-imply-the-axiom-of-c)

What is the relationship of the first fact to the Axiom of Choice? For context we know "every vector space has a basis" is equivalent to Choice. So it does not seem implausible that the first fact is equivalent to Axiom of Choice too.

## Nice bases of a submodule of a free module of infinite rank

[Relevant StackExchange](https://math.stackexchange.com/questions/4967770/nice-bases-for-submodules-of-infinite-free-modules-over-a-pid)

Is the second fact true if $M$ has infinite rank? If not, which $M$ disprove it? Alternatively what extra conditions do we need to put on the underlying ring of $M$? (For instance we know making it a field is sufficient.) So on and so forth.
