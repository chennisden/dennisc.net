This is the problem presented in the Sat Feb 15 2025 review session for Midterm 1. The presentation has been heavily streamlined, so you may notice some differences from what was presented on the board. *In particular the name of the language has been changed*, and the presentation of the definition is different, but it is still the same language.

= Problem

#let filter = "filter"

Fix some alphabet $Sigma$. Given a word $w in Sigma$, we let $filter(w)$ denote $w$ with its odd-indexed characters removed. For example, $filter(a) = epsilon$, $filter(a b) = b$, $filter(a b b a) = b a$, and $filter(epsilon) = epsilon$. For a language $L subset.eq Sigma^*$, define

$
	filter(L) := {filter(x) | x in L}.
$

If $L$ is regular, show that $filter(L)$ is also regular.

= Initial approaches

_This is just some motivation to come up with the correct solution. If you just want to review the solution, you can skip to "The full solution"._

My advice for solving a DFA problem is always the following:

- First, find _any_ algorithm that solves the problem, so you know what you want your DFA to do.
- Then, try to make your algorithm more "DFA-like" by adding specificity.

Also, if we know that $L$ is regular, we should actually give the DFA solving $L$ a name. So suppose that $M = (Q, delta, q_0, F)$ solves $L$.#footnote[I am omitting $Sigma$ because we already know what the alphabet is: we fixed $Sigma$ earlier! And the DFA solving $filter(L)$ will have the same $Sigma$, so there really is no need to specify the language.]

Here is a basic algorithm to check if $y in filter(L)$:

- Find all the $x in Sigma^*$ such that $filter(x) = y$.
- For all said $x$, check if $x in L$ (by running $M(x)$). If any such $x$ is in $L$, then accept.

To get some concrete ideas, we should actually look at what the $x$ look like. For the purposes of clarity, we will examine just the case $y = a b c$:

- We could have $x = a ? b ? c$, where $?$ represents any character in $Sigma$.
- We could also have $x = a ? b ? c ?$.

== An easier problem

The main difficulty is that there are two cases to check. For example, if we want to check whether $a b c in filter(L)$, we need to check all the strings $x$ of the form

- $a ? b ? c$
- $a ? b ? c ?$

which makes the problem inherently more complicated. So we will first solve a simpler problem: if $L$ is regular *and every string in $L$ has even length*, then show that $filter(L)$ is regular.

Here we only need to check the second case, because the first case consists of strings of odd length. This will allow the rest of our discussion to be a little simpler.

Here is the key observation: we can generate all of the possible strings $x$ as we read in the characters of $y$.

```python
def generate_x(y):
	S = {ε} # This is the set of all possible x
	for σ ∈ y:
		S := {x + σ + char | x ∈ S, char ∈ Σ}
```

So when we encounter each $sigma in y$, for every $"char" in Sigma$, we create a new thread $x + sigma + "char"$. And now we are very close to the solution, at least for this easier problem!

== The point of the powerset

But before we try and formalize this idea, there is one last hiccup to take care of: the number of threads balloons, which means that a priori there is no reason to believe the number of states would be finite (which we need for a DFA).

But there is one important observation to make _for this problem_:#footnote[Meaning it does not always hold for _every_ threading problem!] we want to check if _any_ of the threads accepts, which means

+ We only care about the _states_ of the threads,
+ and in particular, we do not care if there is more than one thread at the same state.

Armed with this knowledge, the intuition is as follows: whenever we see two threads at the same state, we should merge them. And what mathematical object automatically merges duplicates? Sets!

== The solution to the easier problem

Now we actually formalize our ideas and write out a full solution _to the easier problem_.

Define a DFA $M' = (Q', delta^*, q_0 ', F')$ where

- $Q' := cal(P) (Q)$
- $delta' (S, sigma) = {delta (q, sigma c) | q in S, c in Sigma}$
- $q_0^' = {q_0}$
- $F' = {S | S sect F != emptyset}$.

What is this DFA really doing? Let's explain it step by step:

- $Q'$: The state $S$ is a subset $Q$, and it is the collection of threads that we are running. Each thread $q$ is an individual state inside of $Q$.
- $delta'$: Whenever we encounter a character $sigma$ in $y$, _for every current state $q in S$_, we would like to create new threads by feeding $sigma c$ into the DFA at state $q$ for every $c in Sigma$. The collection of these threads is just ${delta (q, sigma c) | q in S, c in Sigma}$, exactly as we defined $delta'(S, sigma)$.
- $q_0 '$: Our initial thread is just $q_0$, since we initially start with $y = epsilon$ and the only *even length* string $x$ such that $filter(x) = epsilon$ is $x = epsilon$.
- $F'$: A set of threads $S$ accepts precisely when it contains at least one accepting state. And this occurs precisely when $S sect F != emptyset$.

== How to upgrade

To solve the full problem, where we do not assume that every $x in L$ has even length, we have to handle the odd-length case.

Whenever we read in a character $sigma$, just as we compute all the possible threads after appending $sigma c$ (for every $c in Sigma$), we can compute all the possible threads after just appending $sigma$. So suppose the threads we compute after just appending $sigma$ form a set $T$. Then at any step, $S union T$ consists of all the possible states that we could reach with _all_ of the $x$.

The main mental barrier to defining this $T$ is the following: not knowing how to use $T$ for future updates, or fear that storing $T$ will somehow interfere with future updates. After all, our old solution worked because we were solely using $S$ for updates, and as long as we preserve $S$, hopefully you believe that $T$ is easily computable. But very roughly, the strategy is just to ignore $T$ in $delta$. To do this, we need to store $T$ _separately_ from $S$.

All this will hopefully motivate the following.

= The full solution

Suppose $L$ is solved by $(Q, delta, q_0, F)$. Then define a DFA $(Q', delta', q_0^', F')$ where

- $Q' := cal(P)(Q)$
- $delta' ((S, T), sigma) = ({delta(q, sigma c) | q in S, c in Sigma}, {delta(q, sigma) | q in S})$
- $q_0^' := ({q_0}, {delta(q_0, sigma) | sigma in Sigma})$
- $F' = {(S, T) | (S union T) sect F != emptyset}$.

Then $(Q', delta', q_0^', F')$ solves $filter(L)$.

== What the states represent

_The goal of this section is to clarify why the $delta$ function is defined as it is and especially why $q_0^'$ --- particularly the second component --- is defined this way. It will also become the bedrock for the proof of correctness._

If the state after reading $y$ is $(S, T)$, $S$ is the set of states achieved by the even-length $x$ where $filter(x) = y$ and $T$ is the set of states achieved by the odd-length $x$ where $filter(x) = y$. In symbols,

	$
		S &= { delta(q_0, x) | filter(x) = y, abs(x) "is even" } \
		T &= { delta(q_0, x) | filter(x) = y, abs(x) "is odd" }.
	$

== Outline of proof of correctness

- The proof of correctness of this DFA essentially reduces to proving the claim above via induction on the length of $y$.
- Then we may conclude that

	$
		S union T = { delta(q_0, x) | filter(x) = y }.
	$
- To conclude, note that $(S union T) sect F != emptyset$ precisely when there is some $x in Sigma^*$ such that $delta(q_0, x) in F$ --- which implies $x in L$ --- and $filter(x) = y$.
